<?php
require_once("config.php");
require_once("logs.php");
require_once("email-template.php");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->formAction == "add"){

        $password = $data->username;

        $insertData = Array (
            "complete_name" => $data->complete_name,
            "username" => $data->username,
            "password" => password_hash($data->username, PASSWORD_DEFAULT),
            "role" => $data->role,
            "email" => $data->email,
            "is_deleted" => 0
        );

        $id = $database->insert ('accounts', $insertData);
        if($id){

            $password = $data->username;

            sendEmail($data->email, "LHCS - Back Office Account Registration", account_activation_template(
                $data->complete_name, $password, $data->username, $password, URL
            ));

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Account added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "complete_name" => $data->complete_name,
            "username" => $data->username,
            "role" => $data->role,
            "email" => $data->email,
            "is_deleted" => 0
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update ('accounts', $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Account details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update ('accounts', $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Account deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} ACCOUNT: {$data->complete_name}");
    }else{
        saveLog($database,"{$data->formAction} ACCOUNT ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $database->where ('s.is_deleted', 0);
    $database->join ("access_levels a", "s.role=a.id", "LEFT");
    $userDB = $database->get("accounts s", null, "s.*, a.description");
    echo json_encode($userDB);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $cols = Array ("id", "complete_name", "username", "role", "email");
    $database->where ("id", $id);
    $userDB = $database->getOne("accounts", $cols);
    echo json_encode($userDB);
}

?>