<?php

require("db.php");

if (isset($_GET['_getStudentGrade'])) {

    $studentInfo = getLoggedStudent();
    $schoolYear = getOpenSchoolYear();
    $student = getStudentByStudentId($studentInfo['student_id']);

    $values = $database->rawQuery("SELECT 
            ss.id,
            ss.grade_level,
            ss.subject_id,
            ss.faculty_id,
            s.code,
            s.description,
            CONCAT(f.fname, ' ',f.mname, ' ',f.lname) as faculty_name,
            sg.student_id,
            sg.first,
            sg.second,
            sg.third,
            sg.fourth,
            sg.final,
            sg.remarks,
            (SELECT description FROM semester where is_opened = 1) as school_year,
            CONCAT(stud.fname, ' ',stud.mname, ' ',stud.lname) as student_name,
            stud.grade_level,
            stud.section
        FROM subjects_schedule ss
        LEFT JOIN student_grades sg
            ON sg.subject_id = ss.subject_id
        LEFT JOIN subjects s 
            ON s.id = ss.subject_id
        LEFT JOIN faculties f
            ON f.id = ss.faculty_id
        LEFT JOIN students stud
            ON stud.id = sg.student_id
        WHERE
            ss.grade_level = '{$studentInfo['grade_level']}'
            and sg.student_id = '{$studentInfo['student_id']}'
            and sg.semester_id = {$schoolYear['id']}
        GROUP BY ss.subject_id");

    $response['values'] = $values;
    $response['detail'] = $studentInfo;
    echo json_encode($response);
}
