<?php
require_once("config.php");
require_once("auth.php");

function saveLog($database,$description){

    $loggedUser = json_decode(getLoggedUserDetails($database));

    $insertData = Array (
        "account_id" => $loggedUser->complete_name,
        "description" => ucfirst(strtolower($description)),
        "ip" => $_SERVER['REMOTE_ADDR'],
        "hostname" => gethostbyaddr($_SERVER['REMOTE_ADDR']),
    );

    $id = $database->insert (LOGS, $insertData);
    if($id){
        return true;
    }else{
        return false;
    }
}

//GET METHODS
if(isset($_GET["getLogs"])){

    $primaryKey = 'id';
    $columns = array(
        array( 'db' => 'id', 'dt' => 0 ),
        array( 'db' => 'description', 'dt' => 1 ),
        array(  'db' => 'account_id',
                'dt' => 2,
            ),
        array( 'db' => 'hostname',   'dt' => 3 ),
        array(
            'db'        => 'timestamp',
            'dt'        => 4,
            'formatter' => function( $d, $row ) {
                return date( 'F j, Y, g:i a', strtotime($d));
            }
        ),
    );
    
    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, LOGS , $primaryKey, $columns, "is_deleted = 0" ) // is_deleted = 0 //all logs
    );
}

?>