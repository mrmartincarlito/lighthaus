<?php

require("db.php");

if (isset($_POST['_setEditAccount'])) {
    $data = json_decode($_POST['_setEditAccount']);

    $database->where("username", $_COOKIE['student-logged-in']);
    $student = $database->getOne(ACCOUNTS);

    if (!PASSWORD_VERIFY($data->oldPassword, $student['password'])) {
        $response = json_encode(array(
            "type" => "error",
            "title" => "Error!",
            "text" => "Please enter your password correctly to proceed with the changes",
        ));
        echo $response;
        return;
    }

    $password = $student['password'];

    if (!empty($data->newPassword)) {
        if ($data->newPassword !== $data->confirmPassword) {
            $response = json_encode(array(
                "type" => "error",
                "title" => "Error!",
                "text" => "New Password should match with confirm password to continue",
            ));
            echo $response;
            return;
        } else {
            $password = PASSWORD_HASH($data->newPassword, PASSWORD_DEFAULT);
        }
    }

    $updateData = array(
        "password" => $password
    );

    $database->where("id", $student['id']);
    $update = $database->update(ACCOUNTS, $updateData);

    if ($update) {
        $response = json_encode(array(
            "type" => "success",
            "title" => "Successful!",
            "text" => "Successfully updated Profile Details",
        ));
        echo $response;
    } else {
        $response = json_encode(array(
            "type" => "error",
            "title" => "Error!",
            "text" => "Error while updating settings " . $database->getLastError(),
        ));
        echo $response;
    }


}