<?php
require_once("config.php");
require_once("logs.php");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if (empty($data->faculty)) {
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "Please choose faculty"
        ));
        return;
    }

    $faculty = getFacultyById($data->faculty);
    $openedSchoolYear = getOpenSchoolYear();

    $startDate = strtotime($data->from_date);
    $startDay = date('l', $startDate);

    $endDate = strtotime($openedSchoolYear['end_date']);
    $isConflict = false;

    do {

        $insertSchedule = array(
            "subject_id" => $data->subjects,
            "grade_level" => $data->grade_level,
            "school_year" => $openedSchoolYear['id'],
            "title" => getSubjectById($data->subjects)['description'] . " (" . $faculty['fname'] . " " . $faculty['lname'] . ")",
            "faculty_id" => $data->faculty,
            "start" => date('Y-m-d', $startDate) . " " . $data->from_time,
            "end" => date('Y-m-d', $startDate) . " " . $data->to_time,
            "className" => "bg-primary",
            "added_by" => $_SESSION["username"],
            "created_at" => $database->now()
        );

        $database->where("subject_id", $insertSchedule['subject_id']);
        $database->where("grade_level", $insertSchedule['grade_level']);
        $database->where("start", $insertSchedule['start']);
        $database->where("end", $insertSchedule['end']);
        $records = $database->getOne(SUBJECT_SCHEDULE);

        if (empty($records)) {

            $schedules = $database->rawQuery("SELECT * from " . SUBJECT_SCHEDULE . " where grade_level = '{$insertSchedule['grade_level']}' and
             (start BETWEEN CAST('{$insertSchedule['start']}' as datetime) and CAST('{$insertSchedule['start']}' as datetime)
            AND end BETWEEN CAST('{$insertSchedule['end']}' as datetime) and CAST('{$insertSchedule['end']}' as datetime))
            ");

            if (!empty($schedules)) {
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error!",
                    "text" => "Schedule Conflict"
                ));
                $isConflict = true;
                break;
            }
            $id = $database->insert(SUBJECT_SCHEDULE, $insertSchedule);
        } else {
            $database->where("id", $records['id']);
            $id = $database->update(SUBJECT_SCHEDULE, $insertSchedule);
        }
    
        $startDate = strtotime("next $startDay", $startDate);

    } while($startDate < $endDate);

    if (!$isConflict) {
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Successfully insert new schedule"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

}

//GET METHODS
if(isset($_GET["get"])){

    if (isset($_GET['filter']) && !empty($_GET['filter'])) {
        $database->where("grade_level", $_GET['filter']);
    }

    $database->where("is_deleted", 0);
    $userDB = $database->get(SUBJECT_SCHEDULE);
    echo json_encode($userDB);
}

if (isset($_GET['deleteOccurence'])) {
    $id = $_GET['deleteOccurence'];

    $selectedSchedule = $database->rawQuery("SELECT * , TIME(start) as time_start, DAYNAME(start) as day_start FROM ". SUBJECT_SCHEDULE ." WHERE id = $id");

    $database->rawQuery("
        DELETE from " . SUBJECT_SCHEDULE . " where subject_id = {$selectedSchedule[0]['subject_id']}
        AND grade_level = '{$selectedSchedule[0]['grade_level']}'
        AND school_year = '{$selectedSchedule[0]['school_year']}'
        AND faculty_id = {$selectedSchedule[0]['faculty_id']}
        AND TIME(start) = '{$selectedSchedule[0]['time_start']}'
        AND DAYNAME(start) = '{$selectedSchedule[0]['day_start']}'
    ");

    echo json_encode(Array (
        "type" => "success",
        "title" => "Successful!",
        "text" => "Successfully deleted subject schedule"
    ));
}