<?php
require_once("config.php");
require_once("logs.php");

$LOGGEDPROFILE = json_decode(getLoggedUserDetails($database));

if (isset($_GET['getSubjectHandledByGrade'])) {

    $gradeLevel = $_GET['getSubjectHandledByGrade'];

    $database->where("emp_id", $LOGGEDPROFILE->username);
    $faculty = $database->getOne(FACULTY);

    $subjectHandled = array();

    if (!empty($faculty)) {
        $subjectHandled = $database->rawQuery("
        SELECT * FROM " .SUBJECT_SCHEDULE. " where grade_level = '$gradeLevel' AND faculty_id = {$faculty['id']} GROUP BY subject_id
        ");
    }


    echo json_encode($subjectHandled);
}

if (isset($_GET['filterStudentGrades'])) {
    $request = json_decode($_GET['filterStudentGrades']);

    $students = $database->rawQuery("SELECT * FROM `students` WHERE 
    status = 'APPROVED' 
    AND id 
    IN(
        SELECT student_id FROM enrolled_students where 
        status = 'ENROLLED' 
        and grade_level = '{$request->grade_level}'
        and school_year = (
            SELECT id FROM semester where is_opened = 1)
    )
    ORDER BY lname ASC
    ");

    $responses = array();

    foreach ($students as $student) {
        $response = array();

        $schoolYear = getOpenSchoolYear();


        $database->where("id", $request->subject_handled);
        $response["subject"] = $database->getOne(SUBJECTS);
        
        $database->where("student_id", $student["id"]);
        $enrolledStudentInfo = $database->getOne(ENROLLED_STUDENTS);

        $response["enrolledInfo"] = $enrolledStudentInfo;
        $response["studentInfo"] = $student;

        $database->where("student_id", $student["id"]);
        $database->where("faculty_id", $LOGGEDPROFILE->id);
        $database->where("subject_id", $request->subject_handled);
        $database->where("semester_id", $schoolYear['id']);
        $studentGrade = $database->getOne(STUDENT_GRADES);
        $response["grade"] = $studentGrade;

        if (empty($studentGrade)) {
            $database->insert(STUDENT_GRADES , array (
                "student_id" => $student["id"],
                "faculty_id" => $LOGGEDPROFILE->id,
                "subject_id" => $request->subject_handled,
                "semester_id" => $schoolYear['id']
            ));

            $database->where("student_id", $student["id"]);
            $database->where("faculty_id", $LOGGEDPROFILE->id);
            $database->where("subject_id", $request->subject_handled);
            $database->where("semester_id", $schoolYear['id']);
            $studentGrade = $database->getOne(STUDENT_GRADES);

            $response["grade"] = $studentGrade;
        }

        array_push($responses, $response);
    }

    echo json_encode($responses);
}

if (isset($_POST['saveGrade'])) {
    $values = json_decode($_POST['saveGrade']);

    foreach ($values as $value) {
        $database->where("id", $value->gradeId);
        $database->update(STUDENT_GRADES, array(
            "first" => $value->first ?? 0,
            "second" => $value->second ?? 0,
            "third" => $value->third ?? 0,
            "fourth" => $value->fourth ?? 0,
            "final" => ($value->first ?? 0 
                        + $value->second ?? 0
                        + $value->third ?? 0
                        + $value->fourth ?? 0) / 4,
            "remarks" => $value->remarks
        ));
    }

    echo json_encode(Array (
        "type" => "success",
        "title" => "Successful!",
        "text" => "Successfully saved grade"
    ));
}   
  
if (isset($_GET['viewStudentGrade'])) {

    $response = array();
    $schoolYearRecord = getOpenSchoolYear();

    if (isset($_GET['grade']) && !empty($_GET['grade']) && empty($_GET['student_number'])) {
        $database->where("grade_level", $_GET['grade']);
    }

    if (isset($_GET['student_number']) && !empty($_GET['student_number'])) {
        $database->where("student_number", $_GET['student_number']);
        $enrolledStudent = $database->getOne(ENROLLED_STUDENTS);

        $database->where("id", $enrolledStudent["student_id"]);
    }

    //Get all student
    $database->where("status", "APPROVED");
    $students = $database->get(STUDENTS);

    foreach ($students as $student) {
        $studentRecord['student'] = $student;

        $database->where("student_id", $student['id']);
        $enrolled = $database->getOne(ENROLLED_STUDENTS);
        $studentRecord['enroll_info'] = $enrolled;

        //Get Kinder 1 record on enrollment
        $studentRecord["Kinder_1"]["detail"] = getEnrolledDetailsPerGrade("Kinder 1",  $student['id']);

        //Get Grades during Kinder 1
        $schoolYear = isset($studentRecord["Kinder_1"]["detail"]["school_year"]) ? $studentRecord["Kinder_1"]["detail"]["school_year"] : 0;
        $studentRecord["Kinder_1"]["grade"] = getGradesAll("Kinder 1", $student['id'], $schoolYear);

        //Get Kinder 2 record on enrollment
        $studentRecord["Kinder_2"]["detail"] = getEnrolledDetailsPerGrade("Kinder 2",  $student['id']);

        //Get Grades during Kinder 2
        $schoolYear = isset($studentRecord["Kinder_2"]["detail"]["school_year"]) ? $studentRecord["Kinder_2"]["detail"]["school_year"] : 0;
        $studentRecord["Kinder_2"]["grade"] = getGradesAll("Kinder 2", $student['id'], $schoolYear);

        //Get Grade 1 record on enrollment
        $studentRecord["Grade_1"]["detail"] = getEnrolledDetailsPerGrade("Grade 1",  $student['id']);

        //Get Grades during Grade 1
        $schoolYear = isset($studentRecord["Grade_1"]["detail"]["school_year"]) ? $studentRecord["Grade_1"]["detail"]["school_year"] : 0;
        $studentRecord["Grade_1"]["grade"] = getGradesAll("Grade 1", $student['id'], $schoolYear);

        //Get Grade 2 record on enrollment
        $studentRecord["Grade_2"]["detail"] = getEnrolledDetailsPerGrade("Grade 2",  $student['id']);

        //Get Grades during Grade 2
        $schoolYear = isset($studentRecord["Grade_2"]["detail"]["school_year"]) ? $studentRecord["Grade_2"]["detail"]["school_year"] : 0;
        $studentRecord["Grade_2"]["grade"] = getGradesAll("Grade 2", $student['id'], $schoolYear);

        //Get Grade 3 record on enrollment
        $studentRecord["Grade_3"]["detail"] = getEnrolledDetailsPerGrade("Grade 3",  $student['id']);

        //Get Grades during Grade 3
        $schoolYear = isset($studentRecord["Grade_3"]["detail"]["school_year"]) ? $studentRecord["Grade_3"]["detail"]["school_year"] : 0;
        $studentRecord["Grade_3"]["grade"] = getGradesAll("Grade 3", $student['id'], $schoolYear);

        //Get Grade 4 record on enrollment
        $studentRecord["Grade_4"]["detail"] = getEnrolledDetailsPerGrade("Grade 4",  $student['id']);

        //Get Grades during Grade 4
        $schoolYear = isset($studentRecord["Grade_4"]["detail"]["school_year"]) ? $studentRecord["Grade_4"]["detail"]["school_year"] : 0;
        $studentRecord["Grade_4"]["grade"] = getGradesAll("Grade 4", $student['id'], $schoolYear);

        //Get Grade 5 record on enrollment
        $studentRecord["Grade_5"]["detail"] = getEnrolledDetailsPerGrade("Grade 5",  $student['id']);

        //Get Grades during Grade 5
        $schoolYear = isset($studentRecord["Grade_5"]["detail"]["school_year"]) ? $studentRecord["Grade_5"]["detail"]["school_year"] : 0;
        $studentRecord["Grade_5"]["grade"] = getGradesAll("Grade 5", $student['id'], $schoolYear);

        //Get Grade 6 record on enrollment
        $studentRecord["Grade_6"]["detail"] = getEnrolledDetailsPerGrade("Grade 6",  $student['id']);

        //Get Grades during Grade 6
        $schoolYear = isset($studentRecord["Grade_6"]["detail"]["school_year"]) ? $studentRecord["Grade_6"]["detail"]["school_year"] : 0;
        $studentRecord["Grade_6"]["grade"] = getGradesAll("Grade 6", $student['id'], $schoolYear);


        //Get Existing balance
        $balanceInfo = $database->rawQuery("
            SELECT SUM(billed_amount) as total_balance
            FROM student_payments
            WHERE payment_history_id = 0 AND student_number = 
                (
                    SELECT student_number
                    FROM enrolled_students
                    where student_id = {$student['id']}
                    ORDER BY school_year DESC
                    LIMIT 1
                )
        ");

        $studentRecord["balance"] = $balanceInfo;
        
        $database->where("student_id", $student['id']);
        $database->where("school_year", $schoolYearRecord['id']);
        $database->where("status", "ENROLLED");
        $studentRecord["enrolledInfo"] = $database->getOne(ENROLLED_STUDENTS);

        array_push($response, $studentRecord);
        
    }

    echo json_encode($response);
}

function getEnrolledDetailsPerGrade($gradeLevel, $studentId) {
    global $database;

    $database->where("grade_level", $gradeLevel);
    $database->where("student_id", $studentId);
    return $database->getOne(ENROLLED_STUDENTS);
}


function getGradesAll($gradeLevel, $studentId, $semesterId) {
    global $database;

    $values = $database->rawQuery("SELECT 
        ss.id,
        ss.grade_level,
        ss.subject_id,
        ss.faculty_id,
        s.code,
        s.description,
        CONCAT(f.fname, ' ',f.mname, ' ',f.lname) as faculty_name,
        sg.student_id,
        sg.first,
        sg.second,
        sg.third,
        sg.fourth,
        sg.final,
        sg.remarks,
        (SELECT description FROM semester where id = $semesterId) as school_year,
        CONCAT(stud.fname, ' ',stud.mname, ' ',stud.lname) as student_name,
        stud.grade_level,
        stud.section
    FROM subjects_schedule ss
    LEFT JOIN student_grades sg
        ON sg.subject_id = ss.subject_id
    LEFT JOIN subjects s 
        ON s.id = ss.subject_id
    LEFT JOIN faculties f
        ON f.id = ss.faculty_id
    LEFT JOIN students stud
        ON stud.id = sg.student_id
    WHERE
        ss.grade_level = '$gradeLevel'
        and sg.student_id = '$studentId'
        and sg.semester_id = $semesterId
    GROUP BY ss.subject_id");

    return $values;
}