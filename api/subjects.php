<?php
require_once("config.php");
require_once("logs.php");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->formAction == "add"){

        $subject = $database->rawQuery("SELECT * from " . SUBJECTS . " where grade_level = '{$data->grade_level}' and lower(description) = '" . trim(strtolower($data->description)) . "'");

        if(!empty($subject)) {
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => "You cannot insert same subject to same grade level"
            ));
            return;
        }

        $insertData = Array (
            "grade_level" => $data->grade_level,
            "code" => generateRefno($database, "1", SUBJECTS, "code", "S-"),
            "created_at" => $database->now(),
            "description" => $data->description,
            "added_by" => $_SESSION["username"],
            "is_deleted" => 0
        );

        $id = $database->insert (SUBJECTS, $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Subject added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "grade_level" => $data->grade_level,
            "description" => $data->description,
            "added_by" => $_SESSION["username"],
            "is_deleted" => 0
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (SUBJECTS, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Subject details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (SUBJECTS, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Subjects deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} SUBJECT: {$data->description}");
    }else{
        saveLog($database,"{$data->formAction} SUBJECT ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $database->where("is_deleted", 0);
    $userDB = $database->get(SUBJECTS);
    echo json_encode($userDB);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $userDB = $database->getOne(SUBJECTS);
    echo json_encode($userDB);
}

if (isset($_GET['getSubjectsByGradeLevel'])) {
    $grade_level = $_GET['getSubjectsByGradeLevel'];

    $database->where("grade_level", $grade_level);
    $subjects = $database->get(SUBJECTS);
    echo json_encode($subjects);
}


?>