<?php
require_once("config.php");
require_once("auth.php");
require_once("email-template.php");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    $database->where("id", $data->modifyId);
    $current = $database->getOne(STUDENTS);

    if ($current["status"] != "PENDING") {
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "This is already " . $current["status"]
        ));
        return;
    }

    if($data->formAction == "add"){ 

        $updateData = Array (
            "status" => "APPROVED",
            "approved_by" => $_SESSION["username"]
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (STUDENTS, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "You just approved a student registration"
            ));
            
            //TODO Add email here with student password
            //Add student account
            $studentNumber = generateRefno($database, 1, ENROLLED_STUDENTS, "student_number", "E-");
            $password = uniqid("STUD-");

            $database->insert(ACCOUNTS,
                array(
                    "complete_name" => $data->fname . " " . $data->lname,
                    "username" => $studentNumber,
                    "password" => password_hash($password, PASSWORD_DEFAULT), //TODO to be change to random
                    "email" => $data->email,
                    "role" => "47", //must be student role,
                )
            );

            //insert to enrolled students
            $database->insert(ENROLLED_STUDENTS,
                array(
                    "student_id" => $data->modifyId,
                    "student_number" => $studentNumber,
                    "status" => "",
                    "school_year" => getOpenSchoolYear()["id"],
                    "grade_level" => $data->grade_level
                )
            );

            sendEmail($data->email, "LHCS - Account Registration", account_activation_template(
                $data->fname, $password, $studentNumber, $password, URL
            ));

        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "status" => "REJECTED",
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (STUDENTS, $updateData);
        if($id){
            sendEmail($data->email, "LHCS - Account Registration", accountRejected(
                $data->fname
            ));

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "You just rejected a student registration"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} STUDENT: {$data->fname} {$data->lname}");
    }else{
        saveLog($database,"{$data->formAction} STUDENT ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $primaryKey = 'id';
    $columns = array(
        //array( 'db' => 'id', 'dt' => 0 ),
        array( 'db' => 'grade_level', 'dt' => 0 ),
        array( 'db' => 'fname', 'dt' => 1 ),
        array( 'db' => 'mname', 'dt' => 2 ),
        array( 'db' => 'lname', 'dt' => 3 ),
        array( 'db' => 'address', 'dt' => 4 ),
        array( 'db' => 'contact_number', 'dt' => 5 ),
        array( 'db' => 'created_at', 'dt' => 6 ),
        array(  'db' => 'id', 
                'dt' => 7,
                'formatter' => function ($data, $row) {
                    return '<button type="button" title="View Student Info" onclick="viewStudent(' . $data . ')" class="btn btn-primary btn-circle"><i class="fa fa-eye"></i> </button>';
                }
            ),
    );

    $condition = "is_deleted = 0";
    $status = "PENDING";

    if(isset($_GET['filter'])){
        $filter = json_decode($_GET["filter"]);

        if (!empty($filter->status)) {
            $status = $filter->status;
        }

        if(!empty($filter->grade_level)){
            $condition = $condition . " and grade_level = '".$filter->grade_level."'";
        }

        if(!empty($filter->gender)){
            $condition = $condition . " and gender = '".$filter->gender."'";
        }
    }
    
    $condition = $condition . " and status = '".$status."'";

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, STUDENTS , $primaryKey, $columns, $condition )
    );
}

if(isset($_GET['getDetails'])){
    $studId = $_GET['getDetails'];

    $database->where("id", $studId);
    $response = $database->getOne(STUDENTS);

    echo json_encode($response);
}

