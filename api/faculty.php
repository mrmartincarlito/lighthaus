<?php
require_once("config.php");
require_once("logs.php");
require_once("email-template.php");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->formAction == "add"){

        $insertData = Array (
            "emp_id" => $data->emp_id,
            "fname" => $data->fname,
            "mname" => $data->mname,
            "lname" => $data->lname,
            "created_at" => $database->now(),
            "position" => $data->position,
            "email" => $data->email,
            "added_by" => $_SESSION["username"],
            "is_deleted" => 0
        );

        $id = $database->insert (FACULTY, $insertData);
        if($id){

            $password = uniqid("EMP-");
            $completeName = $data->fname . " " . $data->lname;

            $database->insert(ACCOUNTS,
                array(
                    "complete_name" => $completeName,
                    "username" =>$data->emp_id,
                    "password" => password_hash($password, PASSWORD_DEFAULT),
                    "email" => $data->email,
                    "role" => "46", 
                )
            );

            sendEmail($data->email, "LHCS - Faculty Registration", account_activation_template(
                $completeName, $password, $data->emp_id, $password, URL
            ));

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Faculty added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "emp_id" => $data->emp_id,
            "fname" => $data->fname,
            "mname" => $data->mname,
            "lname" => $data->lname,
            "position" => $data->position,
            "added_by" => $_SESSION["username"],
            "email" => $data->email,
            "is_deleted" => 0
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (FACULTY, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Subject details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (FACULTY, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "FACULTY deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} FACULTY: {$data->emp_id}");
    }else{
        saveLog($database,"{$data->formAction} FACULTY ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $database->where("is_deleted", 0);
    $userDB = $database->get(FACULTY);
    echo json_encode($userDB);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $userDB = $database->getOne(FACULTY);
    echo json_encode($userDB);
}


?>