<?php

require("db.php");

if (isset($_POST['data'])) {
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    $insertData = Array (
        "fname" => $data->fname,
        "mname" => $data->mname,
        "lname" => $data->lname,
        "gender" => $data->gender,
        "birthday" => $data->birthday,
        "grade_level" => $data->grade_level,
        "section" => $data->section ?? "",
        "address" => $data->address,
        "place_of_birth" => $data->place_of_birth ?? "",
        "lrn" => $data->lrn ?? "",
        "prev_school" => $data->prev_school,
        "fathers_name" => $data->fathers_name,
        "mothers_name" => $data->mothers_name,
        "name_of_guardian" => $data->name_of_guardian,
        "contact_number" => $data->contact_number,
        "relationship_student" => $data->relationship_student,
        "email" => $data->email,
        "created_at" => $database->now(),
        "registered_at" => $_SERVER['REMOTE_ADDR'],
    );

    if($data->formAction == "add"){
        
        $id = $database->insert (STUDENTS, $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Student added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){
        
        $database->where("id", $data->modifyId);
        $id = $database->update (STUDENTS, $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Student updated successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    
}

if (isset($_GET['getDetails'])) {
    $id = $_GET['getDetails'];

    $database->where("id", $id);
    $student = $database->getOne(STUDENTS);

    echo json_encode($student);
}