<?php

define ("NAME", "Light in the Haus Christian School Online Portal");
define ("FOOTER" , "LHCS ADMIN TEAM");
define ("LOGO", "http://lhcs.online/back-office/api/lchs.png");

function forgot_password ($name, $username, $password) {
  return '
    
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Forgot Password</title>
</head>
<body style="margin:0px; background: #f8f8f8; ">
<div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
<div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
    <tbody>
      <tr>
      <td style="vertical-align: top; padding-bottom:30px;" align="center"><a href="javascript:void(0)" target="_blank"><img src="'.LOGO.'" width = "200" height = "200" alt="LOGO" style="border:none"></a> </td>
      </tr>
    </tbody>
  </table>
  <div style="padding: 40px; background: #fff;">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
      <tbody>
        <tr>
          <td><b>Welcome to LHCS Portal '.$name.',</b>
            <p>This is your temporary password</p>
            <p>
              Username : '.$username.' <br/>
              Password : '.$password.'
            </p>
            <b>' .FOOTER. '</b> </td>
        </tr>
      </tbody>
    </table>
  </div>
  <div style="text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px">
    <p> '.NAME.' </p>
  </div>
</div>
</div>
</body>
</html>

  ';
}

function account_activation_template($name, $password, $student_no, $activationHash, $url){
    return '
    
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Account Activation</title>
</head>
<body style="margin:0px; background: #f8f8f8; ">
<div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
  <div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
      <tbody>
        <tr>
        <td style="vertical-align: top; padding-bottom:30px;" align="center"><a href="javascript:void(0)" target="_blank"><img src="'.LOGO.'" width = "200" height = "200" alt="LOGO" style="border:none"></a> </td>
        </tr>
      </tbody>
    </table>
    <div style="padding: 40px; background: #fff;">
      <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tbody>
          <tr>
            <td><b>Welcome to LHCS Portal '.$name.',</b>
              <p>This is to inform you that, Your account has been created successfully. Below are the details of your login credentials. Please change it immedieatley</p>
              <!--<a href="'. $url. '/api/register.php?q='.$activationHash.'&z='.$student_no.'" style="display: inline-block; padding: 11px 30px; margin: 20px 0px 30px; font-size: 15px; color: #fff; background: #1e88e5; border-radius: 60px; text-decoration:none;"> Verify your account </a>-->
              <p>
                Username : '.$student_no.' <br/>
                Password : '.$password.'
              </p>
              <b>' .FOOTER. '</b> </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style="text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px">
      <p> '.NAME.' </p>
    </div>
  </div>
</div>
</body>
</html>

    ';
}

function accountRejected($name){
  return '
  
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Account Activation</title>
</head>
<body style="margin:0px; background: #f8f8f8; ">
<div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
<div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
    <tbody>
      <tr>
      <td style="vertical-align: top; padding-bottom:30px;" align="center"><a href="javascript:void(0)" target="_blank"><img src="'.LOGO.'" width = "200" height = "200" alt="Admin Responsive web app kit" style="border:none"></a> </td>
      </tr>
    </tbody>
  </table>
  <div style="padding: 40px; background: #fff;">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
      <tbody>
        <tr>
          <td><b>Welcome to LHCS Portal '.$name.',</b>
            <p>This is to inform you that, Your account has been Rejected. 
              Please comply to the requirements or call our admin office for assistance
            </p>
            
            <b>' .FOOTER. '</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div style="text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px">
    <p>'.NAME.'</p>
  </div>
</div>
</div>
</body>
</html>

  ';
}


function evaluationRejected($name){
  return '
  
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Account Activation</title>
</head>
<body style="margin:0px; background: #f8f8f8; ">
<div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
<div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
    <tbody>
      <tr>
      <td style="vertical-align: top; padding-bottom:30px;" align="center"><a href="javascript:void(0)" target="_blank"><img src="'.LOGO.'" width = "200" height = "200" alt="Admin Responsive web app kit" style="border:none"></a> </td>
      </tr>
    </tbody>
  </table>
  <div style="padding: 40px; background: #fff;">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
      <tbody>
        <tr>
          <td><b>Welcome to LHCS Portal '.$name.',</b>
            <p>This is to inform you that, your evaluation has been Rejected. 
              Please comply to the requirements or call our admin office for assistance
            </p>
            
            <b>' .FOOTER. '</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div style="text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px">
    <p>'.NAME.'</p>
  </div>
</div>
</div>
</body>
</html>

  ';
}

function evaluationApproved($name){
  return '
  
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Account Activation</title>
</head>
<body style="margin:0px; background: #f8f8f8; ">
<div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
<div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
    <tbody>
      <tr>
      <td style="vertical-align: top; padding-bottom:30px;" align="center"><a href="javascript:void(0)" target="_blank"><img src="'.LOGO.'" width = "200" height = "200" alt="Admin Responsive web app kit" style="border:none"></a> </td>
      </tr>
    </tbody>
  </table>
  <div style="padding: 40px; background: #fff;">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
      <tbody>
        <tr>
          <td><b>Welcome to LHCS Portal '.$name.',</b>
            <p>This is to inform you that, your evaluation has been Approved. 
              Please log in to your student portal using your account and pay the necessary payment to be fully enrolled.

              <br/> <br/>
              <b>TRANSFER FUNDS OR DEPOSIT TO :</b><br/>
              <b>EASTWEST BANK</b><br/>
              Account Name   : LIGHT IN THE HAUS CHRISTIAN SCHOOL, INC.<br/>
              Account Number : 200004658247<br/>
              <br/><br/>
              <b>BANK OF THE PHILIPPINE ISLANDS (BPI)</b><br/>
              Account Name   : LIGHT IN THE HAUS CHRISTIAN SCHOOL, INC.<br/>
              Account Number : 4723085425<br/>
              <br/><br/>
              <b>GCASH</b><br/>
              09651645126
              <br/><br/>
              <b>PAYMAYA</b><br/>
              09237137143<br/>

              <br/><br/>
              Hope to see you soon '.$name.' !
            </p>
            
            <b>' .FOOTER. '</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div style="text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px">
    <p>'.NAME.'</p>
  </div>
</div>
</div>
</body>
</html>

  ';
}

function emailPaymentDue($enrolledStudent, $student, $payment){
  return '
  
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Account Activation</title>
</head>
<body style="margin:0px; background: #f8f8f8; ">
<div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
<div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
  <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
    <tbody>
      <tr>
      <td style="vertical-align: top; padding-bottom:30px;" align="center"><a href="javascript:void(0)" target="_blank"><img src="'.LOGO.'" width = "200" height = "200" alt="Admin Responsive web app kit" style="border:none"></a> </td>
      </tr>
    </tbody>
  </table>
  <div style="padding: 40px; background: #fff;">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
      <tbody>
        <tr>
          <td><b>Welcome to LHCS Portal '.$student['fname'].',</b>
            <p>This is to inform you that your '.$enrolledStudent['transaction_type'].' transaction worth Php '.$payment['billed_amount'].'
             is now on DUE. Please settle this payment before the next billing.
            </p>
            
            <b>' .FOOTER. '</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div style="text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px">
    <p>'.NAME.'</p>
  </div>
</div>
</div>
</body>
</html>

  ';
}

