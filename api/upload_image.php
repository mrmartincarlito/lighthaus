<?php
require_once("db.php");

function upload_image_file($id = "", $table = "", $FILES, $dir = "uploads/", $column = "image_file"){
    global $database;

    if(isset($FILES["image_file"]["name"])) {
        $path = $dir.$FILES['image_file']['name'];
        
        if(move_uploaded_file($FILES['image_file']['tmp_name'], $path)){
        
            $database->where("id", $id);
            $isUpdated = $database->update($table, $updateArray = Array(
                $column => $path
            ));
        
            if($isUpdated){
                $response = json_encode(Array (
                    "type" => "success",
                    "title" => "Success",
                    "text" => "Successfully inserted image"
                ));
            }else{
                $response = json_encode(Array (
                    "type" => "error",
                    "title" => "Error",
                    "text" => "Error while inserting ".$database->getLastError()
                ));
            }
        
        }else{
            $response = json_encode(Array (
                "type" => "error",
                "title" => "Error",
                "text" => "Uploading error!"
            ));
        }
        
    }
    else {
        $response = json_encode(Array (
            "type" => "error",
            "title" => "Error",
            "text" => "No picture!"
        ));
    }
    echo $response;
}
?>