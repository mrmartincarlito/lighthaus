<?php

include "env.php";
include "env.".ENV.".php";
include "email-template.php";

// TABLES
define ("ACCOUNTS", "accounts");
define ("SUBJECTS", "subjects");
define ("FACULTY" , "faculties");
define ("PAYMENTS", "payments");
define ("STUDENTS", "students");
define ("ENROLLED_STUDENTS", "enrolled_students");
define ("SEMESTER" , "semester");
define ("LOGS", "logs");
define ("SUBJECT_SCHEDULE" , "subjects_schedule");
define ("STUDENT_PAYMENTS", "student_payments");
define ("STUDENT_PAYMENT_HISTORY", "student_payment_history");
define ("STUDENT_GRADES", "student_grades");
define ("ANNOUNCEMENTS", "announcements");

//GLOBAL FUNCTIONS
function generateRefno($database, $initialCount, $table, $columnRef, $pattern){
    $data = $database->get($table);

    if(!empty($data)){
        $database->orderBy("id","DESC");
        $fetchedData = $database->getOne($table);

        $columnValue = $fetchedData[$columnRef];
        $toIncrement = substr($columnValue, 2);
        $toIncrement = $toIncrement + 1;

        return $pattern.str_pad($toIncrement, 4, '0', STR_PAD_LEFT);
    }else{
        return $pattern.str_pad($initialCount, 4, '0', STR_PAD_LEFT);
    }
}

function getOpenSchoolYear() {
    global $database;

    $database->where("is_opened", 1);
    return $database->getOne(SEMESTER);
}

function getSubjectById($id) {
    global $database;

    $database->where("id", $id);
    return $database->getOne(SUBJECTS);
}

function getFacultyById($id) {
    global $database;

    $database->where("id", $id);
    return $database->getOne(FACULTY);
}

function getLoggedStudent() {
    global $database;
    
    $username = $_COOKIE['student-logged-in'];
    
    $database->where("school_year", getOpenSchoolYear()['id']);
    $database->where("student_number", $username);
    return $database->getOne(ENROLLED_STUDENTS);
}

function getStudentInfo() {
    global $database;
    
    $username = $_COOKIE['student-logged-in'];
    
    $database->where("student_number", $username);
    return $database->getOne(ENROLLED_STUDENTS);
}

function getStudentByStudentId ($studentId) {
    global $database;

    $database->where("id", $studentId);
    return $database->getOne(STUDENTS);
}

function getEnrollmentPaymentBreakDown() {
    global $database;

    $database->where("is_included", "YES");
    $database->where("is_deleted", 0);
    $payments = $database->get(PAYMENTS);

    $regularTuitionFee = 0;

    foreach($payments as $payment) {
        if ($payment["description"] == "Tuition Fee"){
            $regularTuitionFee = $payment['cash'];
        }
    }

    $discounts = array(
        "cash" => -5,
        "semestral" => 5,
        "quarterly" => 8,
        "monthly" => 10,
        "espay" => 10
    );

    $tuitionWithDisc = array();

    foreach($discounts as $key => $value) {
        $tuitionWithDisc[$key] = (( $value / 100 ) * $regularTuitionFee) + $regularTuitionFee;
    }

    $nonTuition = $database->rawQuery("
        SELECT 
            SUM(cash) as sum
        FROM " . PAYMENTS . "
        WHERE is_deleted = 0 and NOT(description = 'Tuition Fee')  AND is_included = 'YES'
    ");

    $overAllPayment = array();
    foreach($tuitionWithDisc as $key => $value) {
        $overAllPayment[$key] = $value + $nonTuition[0]['sum'];
    }


    $initialPaymentUponEnrollment =  $database->rawQuery("
        SELECT  SUM(cash) as cash,
                SUM(semestral) as semestral,
                SUM(quarterly) as quarterly,
                SUM(monthly) as monthly,
                SUM(easy_payment_scheme) as espay
            FROM " . PAYMENTS . "
            WHERE is_included = 'YES' and is_deleted = 0
    ");

    $balanceUponDP = array();
    foreach ($initialPaymentUponEnrollment[0] as $key => $value) {
        $total = $overAllPayment[$key] - $value;

        if ($total < 0){
            $total = 0;
        }

        $balanceUponDP[$key] = $total;
    }


    $breakDown = array();

    foreach ($balanceUponDP as $key => $value) {
        if ($key != "cash") {
            $year = date("Y");
            $nextYear = $year + 1;
            
            $schedDates = array(
                "05-09-$year" => 0,
                "05-10-$year" => 0,
                "05-11-$year" => 0,
                "05-12-$year" => 0,
                "05-01-$nextYear" => 0,
                "05-02-$nextYear" => 0,
                "05-03-$nextYear" => 0,
                "05-04-$nextYear" => 0,
                "05-05-$nextYear" => 0
            );

            $payments = array();

            if ($key == "semestral") {
                $installment = $value / 2;

                $schedDates["05-12-$year"] = $installment;
                $schedDates["05-03-$nextYear"] = $installment;
                
                $payments["semestral"] = $schedDates;
            }

            if ($key == "quarterly") {
                $installment = $value / 3;

                $schedDates["05-11-$year"] = $installment;
                $schedDates["05-02-$nextYear"] = $installment;
                $schedDates["05-05-$nextYear"] = $installment;
                
                $payments["quarterly"] = $schedDates;
            }

            if ($key == "monthly" || $key == "espay") {
                $installment = $value / 9;

                $schedDates["05-09-$year"] = $installment;
                $schedDates["05-10-$year"] = $installment;
                $schedDates["05-11-$year"] = $installment;
                $schedDates["05-12-$year"] = $installment;
                $schedDates["05-01-$nextYear"] = $installment;
                $schedDates["05-02-$nextYear"] = $installment;
                $schedDates["05-03-$nextYear"] = $installment;
                $schedDates["05-04-$nextYear"] = $installment;
                $schedDates["05-05-$nextYear"] = $installment;
                
                $payments[$key] = $schedDates;
            }
            

            array_push($breakDown, $payments);
        }
    }

    $response = array(
        "regularTuition" => $regularTuitionFee,
        "initialDownPayment" => $initialPaymentUponEnrollment,
        "tuitionFee" => $tuitionWithDisc,
        "overAllPayment" => $overAllPayment,
        "balanceUponDP" => $balanceUponDP,
        "breakDown" => $breakDown
    );


    return $response;
}

function checkBillingBalancesDue() {
    global $database;

    $database->rawQuery("

    UPDATE " . STUDENT_PAYMENTS . "
    SET status = 'DUE'
    WHERE STR_TO_DATE(billed_date,'%d-%m-%Y') <= DATE(NOW()) and payment_history_id = 0 and is_deleted = 0
    
    ");

    $database->where("status", "DUE");
    $payments = $database->get(STUDENT_PAYMENTS);

    foreach($payments as $payment) {

        $database->where("student_number", $payment["student_number"]);
        $enrolledStudent = $database->getOne(ENROLLED_STUDENTS); 

        $student = getStudentByStudentId($enrolledStudent["student_id"]);

        if ($payment["is_sms"] == 0) {
            //send sms
            $message = "
Good Day {$student["fname"]}!

Please be informed that your " . $enrolledStudent["transaction_type"] . " transaction worth " . $payment["billed_amount"] . " is now on DUE.
You can view the billing details on your email or you may logged in to your student portal
http://lhcs.online/portal/

Please settle your balance before the next billing.

Thanks, 
LHCS Online Portal Team";
            sendSMS($student["contact_number"], $message);

            $database->where("id", $payment["id"]);
            $database->update(STUDENT_PAYMENTS, array("is_sms" => 1));
        }

        if ($payment["is_emailed"] == 0) {
            //send email
            sendEmail($student["email"], "Payment Due Notice", emailPaymentDue($enrolledStudent, $student, $payment));

            $database->where("id", $payment["id"]);
            $database->update(STUDENT_PAYMENTS, array("is_emailed" => 1));
        }
    }
    
}

function sendSMS($number = "", $message = ""){
    $ch = curl_init();
    $parameters = array(
        'apikey' => APIKEY, //Your API KEY
        'number' => $number,
        'message' => $message,
        'sendername' => SENDER_NAME
    );
    curl_setopt( $ch, CURLOPT_URL, SMS_GATEWAY );
    curl_setopt( $ch, CURLOPT_POST, 1 );

    //Send the parameters set above with the request
    curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $parameters ) );

    // Receive response from server
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    $output = curl_exec( $ch );
    curl_close ($ch);
}


function sendEmail($email, $subject, $message, $from = EMAIL_ACCOUNT){

	date_default_timezone_set('Asia/Manila');

    $header = "From: $from \r\n";
    $header .= "MIME-Version: 1.0\r\n";
    $header .= "Content-type: text/html\r\n";

    return mail($email, $subject, $message, $header);
	
// 	require 'mailing/PHPMailerAutoload.php';

// 	$mail = new PHPMailer;

// 	$mail->isSMTP(); 
// 	$mail->SMTPDebug = 1;
	
// 	/* $mail->SMTPOptions = array(
// 		'ssl' => array(
// 		'verify_peer' => false,
// 		'verify_peer_name' => false,
// 		'allow_self_signed' => true
// 		)
// 	); */

// 	$mail->Debugoutput = 'html';

//   $mail->Host = SMTP_HOST;
//   $mail->Port = SMTP_PORT;
//   $mail->SMTPSecure = 'tls';

//   $mail->SMTPAuth = true;

//   $mail->Username = EMAIL_ACCOUNT;
//   $mail->Password = EMAIL_PASSWORD;

//   $mail->setFrom($from, EMAIL_TITLE);

//   $mail->addReplyTo($from, 'REPLY EMAIL');
//   $mail->addAddress($email, EMAIL_ADDRESS);
// 	$mail->Subject = $subject;

// 	$mail->msgHTML($message);
	
// 	$mail->altBody = $message;


// 	//send the message, check for errors
// 	if (!$mail->send()) {
// 		return false;
// 	} else {
// 		return true;
// 	}

}
