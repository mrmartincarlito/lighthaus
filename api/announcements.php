<?php
require_once("config.php");
require_once("logs.php");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->formAction == "add"){
        $insertData = Array (
            "description" => $data->description,
            "is_deleted" => 0,
            "added_by" => $_SESSION["username"]
        );

        $id = $database->insert (ANNOUNCEMENTS, $insertData);
        if($id){
            $_SESSION["banner_id"] = $id;
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Announcements added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "description" => $data->description
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (ANNOUNCEMENTS, $updateData);
        if($id){
            $_SESSION["banner_id"] = $data->modifyId;
            
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Announcement details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (ANNOUNCEMENTS, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Announcement deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    saveLog($database,"{$data->formAction} ANNOUNCEMENT: {$data->description}");
}

//GET METHODS
if(isset($_GET["get"])){
    $database->where ('is_deleted', 0);
    $userDB = $database->get(ANNOUNCEMENTS);
    echo json_encode($userDB);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $userDB = $database->getOne(ANNOUNCEMENTS);
    echo json_encode($userDB);
}

?>