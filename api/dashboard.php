<?php
require_once("config.php");
require_once("logs.php");

if (isset($_GET['getCards'])) {

    $database->where("status", "APPROVED");
    $response["no_registered_students"] = count($database->get(STUDENTS));

    $database->where("status", "PENDING");
    $response["no_of_pending_registration"] = count($database->get(STUDENTS));

    $database->where("status", "ENROLLED");
    $response["no_of_enrolled_students"] = count($database->get(ENROLLED_STUDENTS));

    $database->where("status", "PENDING");
    $response["no_of_pending_evaluation"] = count($database->get(ENROLLED_STUDENTS));

    $database->where("status", "PENDING");
    $response["no_pending_payments"] = count($database->get(STUDENT_PAYMENTS));

    $database->where("is_deleted", 0);
    $response["no_of_faculties"] = count($database->get(FACULTY));

    echo json_encode($response);
}

if (isset($_GET['getStudentStats'])) {

    $gradelevels = array(
        "Kinder 1",
        "Kinder 2",
        "Grade 1",
        "Grade 2",
        "Grade 3",
        "Grade 4",
        "Grade 5",
        "Grade 6",
    );

    $response = array();

    foreach ($gradelevels as $grade) {
        $records = $database->rawQuery("SELECT count(*) as total,
            grade_level FROM `students` 
            WHERE status = 'APPROVED' 
            and id IN 
                (select student_id from enrolled_students s 
                    where s.status = 'ENROLLED' 
                    and s.school_year = ( select id from semester where is_opened = 1 ) 
                ) 
            and grade_level = '$grade'
            group by grade_level");

        array_push($response, array(
            "label" => $grade,
            "value" => empty($records) ? 0 : $records[0]['total']
        ));
    }

    echo json_encode($response);
}


if(isset($_GET['_getLineChartForTransactions'])){

    $months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');

    $groupByDefault = "MONTH(date_time)";

    $year = "YEAR(NOW())";
    if(!empty($_GET['year'])){
        $year = $_GET['year'];
    }

    $response = array();
    $days = 0;
   
    $whereMonthClause = "";

    if(!empty($_GET['month'])){
        $groupByDefault = "DAY(date_time)";
        $whereMonthClause = " and MONTH(date_time) = ".$_GET['month'];
        $days = cal_days_in_month(CAL_GREGORIAN, $_GET['month'], $_GET['year']);

        for($i=1; $i<=$days; $i++){
            $values["m"] = "$i";
            $values["a"] = "0";
            $values["b"] = "0";

            array_push($response, $values);
        }
    }

    for($i=1; $i<=12; $i++){
        if($days == 0){
            $values["m"] = $months[$i - 1] ;
            $values["a"] = "0";
            $values["b"] = "0";

            array_push($response, $values);
        }
    }

    $data = $database->rawQuery("
        select  count(id) as b, 
        sum(billed_amount) as a, 
        $groupByDefault as m 
        from student_payments 
        where status = 'APPROVED' and 
        YEAR(date_time) = $year
        $whereMonthClause 
        group by $groupByDefault
    ");
   
    foreach($data as $x){
        $response[$x["m"] - 1]["a"] = "{$x["a"]}";
        $response[$x["m"] - 1]["b"] = "{$x["b"]}";

    }

    echo json_encode($response);
}