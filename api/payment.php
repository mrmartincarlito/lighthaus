<?php
require_once("config.php");
require_once("logs.php");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->formAction == "add"){

        $insertData = Array (
            "description" => $data->description,
            "payment_code" => generateRefno($database, "1", PAYMENTS, "payment_code", "P-"),
            "cash" => $data->cash,
            "semestral" => $data->semestral,
            "quarterly" => $data->quarterly,
            "monthly" => $data->monthly,
            "easy_payment_scheme" => $data->easy_payment_scheme,
            "is_included" => $data->is_included,
            "created_at" => $database->now(),
            "added_by" => $_SESSION["username"],
        );

        $id = $database->insert (PAYMENTS, $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Subject added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "description" => $data->description,
            "cash" => $data->cash,
            "semestral" => $data->semestral,
            "quarterly" => $data->quarterly,
            "monthly" => $data->monthly,
            "easy_payment_scheme" => $data->easy_payment_scheme,
            "is_included" => $data->is_included
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (PAYMENTS, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Payment details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (PAYMENTS, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "PAYMENTS deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} PAYMENT: {$data->description}");
    }else{
        saveLog($database,"{$data->formAction} PAYMENT ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $database->where("is_deleted", 0);
    $userDB = $database->get(PAYMENTS);
    echo json_encode($userDB);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $userDB = $database->getOne(PAYMENTS);
    echo json_encode($userDB);
}

if (isset($_GET['sumOfPayments'])) {
    $sum = $database->rawQuery("
        SELECT 
            SUM(cash) as dp_1,
            SUM(semestral) as dp_2,
            SUM(quarterly) as dp_3,
            SUM(monthly) as dp_4,
            SUM(easy_payment_scheme) as dp_5
        FROM " . PAYMENTS . "
        WHERE is_deleted = 0
        AND is_included = 'YES'
    ");

    echo json_encode($sum);
}

if (isset($_GET['getSumOfNonTuitionFee'])) {
    $sum = $database->rawQuery("
        SELECT 
            SUM(cash) as sum
        FROM " . PAYMENTS . "
        WHERE is_deleted = 0 and NOT(description = 'Tuition Fee')  AND is_included = 'YES'
    ");

    echo json_encode($sum);
}


?>