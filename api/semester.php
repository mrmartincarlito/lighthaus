<?php
require_once("config.php");
require_once("logs.php");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->formAction == "add"){

        $insertSem = Array(
            "description" => $data->description,
            "start_date" => $data->start_date,
            "end_date" => $data->end_date,
            "created_by" => $_SESSION["username"],
            //"is_opened" => 1
        );

        $insert = $database->insert("semester", $insertSem);

        if($insert){
           
            //$database->rawQuery("Update semester set is_opened = 0 where description != '".$data->description."'");

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Successfully changed school year!"
            ));

        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $userDB = $database->get("semester");
    echo json_encode($userDB);
}

if (isset($_GET['changeStatus'])) {
    $id = $_GET['changeStatus'];

    $database->where("is_opened", 1);
    $schoolYear = $database->get("semester");

    if (empty($schoolYear)) {
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "You cannot close all school year"
        ));

        return;
    }

    $schoolYear = getOpenSchoolYear();

    $database->where("school_year", $schoolYear['id']);
    $database->where("status", "ENROLLED");
    $database->update(ENROLLED_STUDENTS, array("school_year_status" => "COMPLETED"));

    $database->rawQuery("Update semester set is_opened = 0 where 1");
    $database->rawQuery("Update semester set is_opened = 1 where id = $id");

    echo json_encode(Array (
        "type" => "success",
        "title" => "Successful!",
        "text" => "Successfully changed school year!"
    ));
}
