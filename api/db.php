<?php
session_start();
require_once("autoload.php");
require_once ('dbhelper/MysqliDb.php');

if(@(new mysqli(SQL_SERVERADDRESS, SQL_USERNAME, SQL_PASSWORD, SQL_DATABASE))->connect_errno){
    echo "Cannot connect to database, please contact server administrator.";
    exit;
}

$database = new MysqliDb (
    Array (
        'host' => SQL_SERVERADDRESS,
        'username' => SQL_USERNAME, 
        'password' => SQL_PASSWORD,
        'db'=> SQL_DATABASE,
        'port' => SQL_PORT,
        'prefix' => SQL_PREFIX,
        'charset' => 'utf8'
    )
);

$sqlSSPDetails = Array(
    'user' => SQL_USERNAME,
    'pass' => SQL_PASSWORD,
    'db' => SQL_DATABASE,
    'host' => SQL_SERVERADDRESS
);
