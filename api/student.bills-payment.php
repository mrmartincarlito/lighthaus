<?php

require("db.php");

if (isset($_GET['_getBillsPayments'])) {

    $studentInfo = getLoggedStudent();
    $schoolYear = getOpenSchoolYear();
    $studentNumber = $_COOKIE['student-logged-in'];
    
    $database->where("student_number", $studentNumber);
    //$database->where("school_year", $schoolYear['id']);
    $payments = $database->get(STUDENT_PAYMENTS);

    $values['all_payments'] = $payments;

    $database->where("student_number", $studentNumber);
    //$database->where("school_year", $schoolYear['id']);
    //$database->where("status", "PAID");
    $database->orderBy("date_paid", "DESC");
    $history = $database->get(STUDENT_PAYMENT_HISTORY);

    $values['history'] = $history;

    echo json_encode($values);
}