<?php
require_once("config.php");
require_once("auth.php");

if(isset($_GET["get"])){

    $primaryKey = 'id';
    $columns = array(
        //array( 'db' => 'id', 'dt' => 0 ),
        array( 'db' => 'id', 
                'dt' => 0 ,
                'formatter' => function ($data, $row) {
                    global $database;

                    $database->where("student_id", $data);
                    $stud = $database->getOne(ENROLLED_STUDENTS);

                    return $stud["student_number"];
                }
            ),
        array( 'db' => 'id', 'dt' => 1 ,
            'formatter' => function ($data, $row) {
                global $database;

                $database->where("student_id", $data);
                $stud = $database->getOne(ENROLLED_STUDENTS);

                return $stud["grade_level"];
            }
        ),
        array( 'db' => 'fname', 'dt' => 2 ),
        array( 'db' => 'mname', 'dt' => 3 ),
        array( 'db' => 'lname', 'dt' => 4 ),
    );
    
    $condition = "status = 'APPROVED' and 
        id IN (
            select student_id from enrolled_students
            where status = 'ENROLLED' and school_year = ( select id from semester where is_opened = 1 ) and grade_level = '{$_GET['grade']}'
        )";

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, STUDENTS , $primaryKey, $columns, $condition )
    );
}

if (isset($_GET['getAllGradeHandled'])) {
    $user = json_decode(getLoggedUserDetails($database));
    $schoolYear = getOpenSchoolYear();

    $database->where("emp_id", $user->username);
    $faculty = $database->getOne(FACULTY);

    $grades = $database->rawQuery("
        SELECT * FROM subjects_schedule
        where school_year = {$schoolYear['id']}
        and faculty_id = {$faculty['id']}
        group by grade_level
        order by grade_level desc
    ");

    $sched = $database->rawQuery("
        SELECT 
            title,
            DATE_FORMAT(start, '%M %d %Y %W') as start,
            DATE_FORMAT(start, '%h : %i %p') as startTime,
            DATE_FORMAT(end, '%h : %i %p') as endTime
        FROM subjects_schedule
        where school_year = {$schoolYear['id']}
        and faculty_id = {$faculty['id']}
        and DATE(start) = DATE(now())
        group by subject_id
    ");

    $response['grade_level'] = $grades;
    $response['schedules'] = $sched;

    echo json_encode($response);
}