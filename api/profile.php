<?php
require_once "config.php";
require_once "logs.php";

$LOGGEDPROFILE = json_decode(getLoggedUserDetails($database));

if (isset($_POST['viewProfile'])) {

    $database->where("id", $LOGGEDPROFILE->acc_id);
    $profile = $database->getOne(ACCOUNTS);

    $profile['role'] = $LOGGEDPROFILE->description;

    echo json_encode($profile);
}

if (isset($_POST['createSession'])) {
    $_SESSION['document-description'] = $_POST['createSession'];
}

if (isset($_POST['settingChanges'])) {

    $postData = json_decode($_POST["settingChanges"]);
    $data = json_decode($postData->data);

    if (!PASSWORD_VERIFY($data->oldPassword, $LOGGEDPROFILE->password)) {
        $response = json_encode(array(
            "type" => "error",
            "title" => "Error!",
            "text" => "Please enter your password correctly to proceed with the changes",
        ));
        echo $response;
        return;
    }

    $password = $LOGGEDPROFILE->password;

    if (!empty($data->newPassword)) {
        if ($data->newPassword !== $data->confirmPassword) {
            $response = json_encode(array(
                "type" => "error",
                "title" => "Error!",
                "text" => "New Password should match with confirm password to continue",
            ));
            echo $response;
            return;
        } else {
            $password = PASSWORD_HASH($data->newPassword, PASSWORD_DEFAULT);
        }
    }

    $updateData = array(
        "email" => $data->email,
        "username" => $data->username,
        "password" => $password,
    );

    $database->where("id", $LOGGEDPROFILE->acc_id);
    $update = $database->update(ACCOUNTS, $updateData);

    if ($update) {
        $response = json_encode(array(
            "type" => "success",
            "title" => "Successful!",
            "text" => "Successfully updated Profile Details",
        ));
        echo $response;
    } else {
        $response = json_encode(array(
            "type" => "error",
            "title" => "Error!",
            "text" => "Error while updating settings " . $database->getLastError(),
        ));
        echo $response;
    }
}
