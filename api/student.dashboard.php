<?php

require("db.php");

if (isset($_GET['_getDashboard'])) {
    $response = array();  

    //start of dashboard card
    $cards = array();

    $studentInfo = getLoggedStudent();
    $schoolYear = getOpenSchoolYear();
    $studentNumber = $_COOKIE['student-logged-in'];

    $cards['school_year'] = $schoolYear['description'];
    $cards['status'] = empty($studentInfo) ? array() : $studentInfo['status'];

    //balance due
    $database->where("student_number", $studentNumber);
    //$database->where("school_year", $schoolYear['id']);
    $database->where("status", "DUE");
    $balanceDues = $database->get(STUDENT_PAYMENTS);

    $totalBalanceDue = 0;
    foreach($balanceDues as $balanceDue) {
        $totalBalanceDue = $totalBalanceDue + $balanceDue['billed_amount'];
    }

    $cards['balance_due'] = number_format($totalBalanceDue);

    //total balance
    //$database->where("student_number", $studentInfo['student_number']);
    //$database->where("school_year", $schoolYear['id']);
    //$database->where("payment_history_id", 0);
    //$balances = $database->get(STUDENT_PAYMENTS);

    $balances = $database->rawQuery("SELECT * FROM " . STUDENT_PAYMENTS . " where student_number = '{$studentNumber}' 
    and payment_history_id = 0 and status = ''");
    
    $totalBalance = 0;
    foreach($balances as $balanceDue) {
        $totalBalance = $totalBalance + $balanceDue['billed_amount'];
    }

    $cards['total_balance'] = number_format($totalBalance);

    //end of dashboard card
    
    //start of subject schedule
    $database->where("is_deleted", 0);
    $database->where("grade_level", $studentInfo['grade_level'] ?? "");
    $database->where("school_year", $schoolYear['id']);
    //$database->where("DATE(start) = DATE(NOW())");
    $subjects = $database->get(SUBJECT_SCHEDULE);

    //announcements
    $database->where("is_deleted", 0);
    $database->orderBy("date_time", "DESC");
    $announcemetns = $database->get(ANNOUNCEMENTS);
    
    $values = array(
        "cards" => $cards,
        "balance_due_table" => $balanceDues,
        "subjects" => $subjects,
        "announcements" => $announcemetns
    );

    array_push($response, $values);

    echo json_encode($response);
}

if (isset($_POST['_payBalanceDue'])) {
    $payments = json_decode($_POST['_payBalanceDue']);

    $studentInfo = getLoggedStudent();
    $studentNumber = $_COOKIE['student-logged-in'];

    $paymentIdString = implode( ",", $payments->payments);
    $sumTotalAmount = $database->rawQuery("SELECT SUM(billed_amount) as total FROM ". STUDENT_PAYMENTS ." where id IN ($paymentIdString)");
    
    $totalAmount = empty($sumTotalAmount) ? 0 : $sumTotalAmount[0]['total'];

    $id = $database->insert(STUDENT_PAYMENT_HISTORY , array(
        "amount" => $totalAmount,
        "date_paid" => $database->now(),
        "student_number" => $studentNumber,
    ));

    foreach ($payments->payments as $payment) {
        $database->where("id", $payment);
        $database->update(STUDENT_PAYMENTS, array(
            "payment_history_id" => $id,
            "status" => "PENDING"
        ));

        $_SESSION['student-payment-history'] = $id;
    }

    if($id){
        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Successfully submitted payment"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
    }

}