<?php

require("db.php");

if (isset($_POST['data'])) {
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);
    $transactionType = $_POST['type'];

    $studentAccount = getStudentInfo();
    $enrolledStudent = getLoggedStudent();

    if (!empty($enrolledStudent)) {
        if ($enrolledStudent['status'] == "PENDING") {
            echo json_encode(Array (
                "type" => "error",
                "title" => "You already submitted a form please contact the administrator",
                "text" => $database->getLastError()
            ));
            return;
        }
    }

    $database->where("id", $studentAccount['student_id']);
    $studentInfo = $database->getOne(STUDENTS);

    $updateStudentInfo = array(
        "address" => $data->address,
        "birthday" => $data->birthday,
        "grade_level" => $data->grade_level,
        "lrn" => $data->lrn,
        "address" => $data->address,
        "place_of_birth" => $data->place_of_birth,
        "prev_school" => $data->prev_school,
        "fathers_name" => $data->fathers_name,
        "mothers_name" => $data->mothers_name,
        "name_of_guardian" => $data->name_of_guardian,
        "relationship_student" => $data->relationship_student,
        "contact_number" => $data->contact_number
    );

    $database->where("id", $studentAccount['student_id']);
    $isUpdated = $database->update(STUDENTS, $updateStudentInfo);

    if ($isUpdated) {

        $totalExistingBalance = empty($balance) ? 0 : $balance[0]['balance'];

        $getCurrentEnrollmentPayment = getEnrollmentPaymentBreakDown();
        $newBalance = 0;

        if ($transactionType == "CASH") {
            $database->insert(STUDENT_PAYMENTS,
                    array(
                        "payment_description" => "Full Payment",
                        "student_number" => $studentAccount['student_number'],
                        "billed_amount" => $getCurrentEnrollmentPayment['overAllPayment']['cash'],
                        "status" => "",
                    )
                );
            
            $newBalance = $getCurrentEnrollmentPayment['overAllPayment']['cash'];
        }

        if ($transactionType == "SEMESTRAL") {
            $database->insert(STUDENT_PAYMENTS,
                    array(
                        "payment_description" => "Down Payment",
                        "student_number" => $studentAccount['student_number'],
                        "billed_amount" => $getCurrentEnrollmentPayment['initialDownPayment'][0]['semestral'],
                        "status" => "",
                    )
                );
            
            $newBalance = $getCurrentEnrollmentPayment['overAllPayment']['semestral'];
            insertBreakDown(0, strtolower($transactionType), $getCurrentEnrollmentPayment, $studentAccount['student_number']);
        }

        if ($transactionType == "QUARTERLY") {
            $database->insert(STUDENT_PAYMENTS,
                    array(
                        "payment_description" => "Down Payment",
                        "student_number" => $studentAccount['student_number'],
                        "billed_amount" => $getCurrentEnrollmentPayment['initialDownPayment'][0]['quarterly'],
                        "status" => "",
                    )
                );
            $newBalance = $getCurrentEnrollmentPayment['overAllPayment']['quarterly'];
            insertBreakDown(1, strtolower($transactionType), $getCurrentEnrollmentPayment, $studentAccount['student_number']);
        }

        if ($transactionType == "MONTHLY") {
            $database->insert(STUDENT_PAYMENTS,
                    array(
                        "payment_description" => "Down Payment",
                        "student_number" => $studentAccount['student_number'],
                        "billed_amount" => $getCurrentEnrollmentPayment['initialDownPayment'][0]['monthly'],
                        "status" => "",
                    )
                );
            
            $newBalance = $getCurrentEnrollmentPayment['overAllPayment']['monthly'];
            insertBreakDown(2, strtolower($transactionType), $getCurrentEnrollmentPayment, $studentAccount['student_number']);
        }

        if ($transactionType == "ESPAY") {
            $database->insert(STUDENT_PAYMENTS,
                    array(
                        "payment_description" => "Down Payment",
                        "student_number" => $studentAccount['student_number'],
                        "billed_amount" => $getCurrentEnrollmentPayment['initialDownPayment'][0]['semestral'],
                        "status" => "",
                    )
                );

            $newBalance = $getCurrentEnrollmentPayment['overAllPayment']['espay'];
            insertBreakDown(3, strtolower($transactionType), $getCurrentEnrollmentPayment, $studentAccount['student_number']);
        }

        //$database->where("student_id", $studentAccount['student_id']);
        $id = $database->insert(ENROLLED_STUDENTS, array(
            "status" => "PENDING",
            "student_id" => $studentAccount['student_id'],
            "student_number" => $_COOKIE['student-logged-in'],
            "transaction_type" => $transactionType,
            "school_year" => getOpenSchoolYear()['id'],
            "grade_level" => $data->grade_level
            //"balance" => $newBalance + $totalExistingBalance
        ));

        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Successfully insert new schedule"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

}

function insertBreakDown($index, $transactionType, $payment, $studentNumber) {
    global $database;

    $breakDowns = $payment['breakDown'][$index][$transactionType];

    foreach ($breakDowns as $key => $value) {
        if ($value != 0) {
            $database->insert(STUDENT_PAYMENTS,
                array(
                    "payment_description" => "INSTALLMENT",
                    "student_number" => $studentNumber,
                    "billed_date" => $key,
                    "billed_amount" => $value,
                    "status" => "",
                )
            );
        }
        
    }

}

if (isset($_GET['_getStudentDetails'])) {

    $studentNumber = $_COOKIE['student-logged-in'];
    $schoolYear = getOpenSchoolYear();
    $response['open_school_year'] = $schoolYear;  

    $studentAccount["Kinder 1"] = getEnrollmentDetailsPerGrade("Kinder 1", $studentNumber);
    $studentAccount["Kinder 2"] = getEnrollmentDetailsPerGrade("Kinder 2", $studentNumber);
    $studentAccount["Grade 1"] = getEnrollmentDetailsPerGrade("Grade 1", $studentNumber);
    $studentAccount["Grade 2"] = getEnrollmentDetailsPerGrade("Grade 2", $studentNumber);
    $studentAccount["Grade 3"] = getEnrollmentDetailsPerGrade("Grade 3", $studentNumber);
    $studentAccount["Grade 4"] = getEnrollmentDetailsPerGrade("Grade 4", $studentNumber);
    $studentAccount["Grade 5"] = getEnrollmentDetailsPerGrade("Grade 5", $studentNumber);
    $studentAccount["Grade 6"] = getEnrollmentDetailsPerGrade("Grade 6", $studentNumber);


    $response['student'] = $database->rawQuery("SELECT * FROM " . STUDENTS . " where id = 
    (SELECT student_id FROM " . ENROLLED_STUDENTS . " where student_number = '$studentNumber' limit 1) ");

    $response['enrollment_details'] = $studentAccount;

    $database->where("school_year", $schoolYear['id']);
    $database->where("student_number", $studentNumber);
    $response['current_school_year'] = $database->getOne(ENROLLED_STUDENTS);

    echo json_encode($response);
}

if (isset($_GET['_getEnrollmentPayment'])) {
    echo json_encode(getEnrollmentPaymentBreakDown());
}

function getEnrollmentDetailsPerGrade($gradeLevel, $studentNumber) {
    global $database;

    $database->where("student_number", $studentNumber);
    $database->where("grade_level", $gradeLevel);
    return $database->getOne(ENROLLED_STUDENTS);
}