<?php

require("db.php");
require_once("email-template.php");

if (isset($_POST['forgot'])) {
    $username = $_POST['username'];

    $database->where("username", $username);
    $account = $database->getOne(ACCOUNTS);

    $password = substr(md5(mt_rand()), 0, 5);

    $database->where("username", $username);
    $id = $database->update(ACCOUNTS, array(
        "password" => password_hash($password, PASSWORD_DEFAULT)
    ));

    sendEmail($account['email'], "LHCS - Forgot Password", forgot_password(
        $account["complete_name"], $account["username"], $password
    ));

    echo "Sent";
}