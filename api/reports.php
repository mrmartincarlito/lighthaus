<?php
require_once("config.php");
require_once("logs.php");

$loggedUser = json_decode(getLoggedUserDetails($database));
//$checkAccess = checkAccessReport($loggedUser->role);

if (isset($_GET['report_of_grades'])) {
    $data = json_decode($_GET['report_of_grades']);

    $schoolYear = getOpenSchoolYear();
    $response = array();

    $database->where("student_number", $data->data->student_number);
    $enrolledStudent = $database->getOne(ENROLLED_STUDENTS);

    if (empty($enrolledStudent)) {
        $response["caption"] = "";
        $response["columns"] = array();
        $response["rows"] = array();
        $response["others"] = array();

        echo json_encode($response);
        return;
    }

    $database->where("id", $enrolledStudent["student_id"]);
    $student = $database->getOne(STUDENTS);

    $response["caption"] = "<h4>{$data->data->student_number} Report of Grades {$schoolYear['description']}</h4>
                            <h5>Student Name : ".strtoupper($student['lname'])." , ".strtoupper($student['fname'])." ".strtoupper($student['mname']).". </h5>
                            <h6>Grade/Section : ".$student['grade_level']."/".$student['section']."</h6>";
    //th columns
    $columns = array(
        "Faculty", "Subject", "1st", "2nd", "3rd", "4th", "Final", "Remarks"
    );

    $response["columns"] = $columns;

    $rows = $database->rawQuery("SELECT 
            ss.id,
            ss.grade_level,
            ss.subject_id,
            ss.faculty_id,
            s.code,
            s.description,
            CONCAT(f.fname, ' ',f.mname, ' ',f.lname) as faculty_name,
            sg.student_id,
            sg.first,
            sg.second,
            sg.third,
            sg.fourth,
            sg.final,
            sg.remarks,
            (SELECT description FROM semester where is_opened = 1) as school_year,
            CONCAT(stud.fname, ' ',stud.mname, ' ',stud.lname) as student_name,
            stud.grade_level
        FROM subjects_schedule ss
        LEFT JOIN student_grades sg
            ON sg.subject_id = ss.subject_id
        LEFT JOIN subjects s 
            ON s.id = ss.subject_id
        LEFT JOIN faculties f
            ON f.id = ss.faculty_id
        LEFT JOIN students stud
            ON stud.id = sg.student_id
        WHERE
            ss.grade_level = '{$student['grade_level']}'
            and sg.student_id = '{$student['id']}'
            and sg.semester_id = {$schoolYear['id']}
        GROUP BY ss.subject_id");

    $response["rows"] = $rows;
    $response["others"] = array();

    echo json_encode($response);
}

if (isset($_GET['report_of_grades_grade_level'])) {
    $data = json_decode($_GET['report_of_grades_grade_level']);

    $schoolYear = getOpenSchoolYear();
    $response = array();


    $response["caption"] = "<h4>Report of Grades {$schoolYear['description']}</h4>
                            <h5>Grade Level / Section : {$data->data->grade_level_filter} {$data->data->section}</h5>";
    //th columns
    $columns = array(
        "Stud Name", "Faculty", "Subject", "1st", "2nd", "3rd", "4th", "Final", "Remarks"
    );

    $response["columns"] = $columns;

    $rows = $database->rawQuery("SELECT 
            ss.id,
            ss.grade_level,
            ss.subject_id,
            ss.faculty_id,
            s.code,
            s.description,
            CONCAT(f.fname, ' ',f.mname, ' ',f.lname) as faculty_name,
            sg.student_id,
            sg.first,
            sg.second,
            sg.third,
            sg.fourth,
            sg.final,
            sg.remarks,
            (SELECT description FROM semester where is_opened = 1) as school_year,
            CONCAT(stud.fname, ' ',stud.mname, ' ',stud.lname) as student_name,
            stud.grade_level
        FROM subjects_schedule ss
        LEFT JOIN student_grades sg
            ON sg.subject_id = ss.subject_id
        LEFT JOIN subjects s 
            ON s.id = ss.subject_id
        LEFT JOIN faculties f
            ON f.id = ss.faculty_id
        LEFT JOIN students stud
            ON stud.id = sg.student_id
        WHERE
            ss.grade_level = '{$data->data->grade_level_filter}'
            and sg.semester_id = {$schoolYear['id']}
        GROUP BY ss.subject_id
        ORDER BY stud.lname ASC");

    $response["rows"] = $rows;
    $response["others"] = array();

    echo json_encode($response);
}

if (isset($_GET['enrolled_students'])) {

    // if ($checkAccess["access_report_payroll"] == "0") {
    //     $response["caption"] = "<h1 class='text-danger'>You do not have permission to access Payroll Report</h1>";
    //     $response["columns"] = array();
    //     $response["rows"] = array();
    //     $response["others"] = array();

    //     echo json_encode($response);
    //     return;
    // }

    $data = json_decode($_GET['enrolled_students']);
    $schoolYear = getOpenSchoolYear();
    $response = array();
    $response["caption"] = "<h4>Enrolled Student {$schoolYear['description']}</h4><h5>Grade / Section : {$data->data->grade_level_filter} {$data->data->section}</h5>";
    //th columns
    $columns = array(
        "STUD #", "LAST NAME", "FIRST NAME", "MIDDLE NAME", "GR/SEC", "AGE", "ADDRESS"
    );

    $response["columns"] = $columns;

    $gradeWhere = "";
    if (!empty($data->data->grade_level_filter)) {
        $gradeWhere = " and grade_level = '{$data->data->grade_level_filter}'";
    }

    $sectionWhere = "";
    if (!empty($data->data->section)) {
        $sectionWhere = " and section like '%{$data->data->section}%'";
    }

    $rows = $database->rawQuery("
        SELECT *, TIMESTAMPDIFF(YEAR, birthday, CURDATE()) AS age FROM students where id IN (
            select student_id from enrolled_students
            where status = 'ENROLLED' and school_year = ( select id from semester where is_opened = 1 )
        ) $gradeWhere $sectionWhere order by lname ASC
    ");

    $response["rows"] = $rows;

    $overAll = count($rows);
    $male = 0;
    $female = 0;
    foreach ($rows as $row) {
        if ($row['gender'] == "Male")
            $male ++;
        else
            $female ++;
    }

    $response["others"] = array(
        array(
            "name" => "Male",
            "value" => $male,
            "is_numeric" => "true"
        ),
        array(
            "name" => "Female",
            "value" => $female,
            "is_numeric" => "true"
        ),
        array(
            "name" => "Overall Total",
            "value" => $overAll,
            "is_numeric" => "true"
        )
    );

    echo json_encode($response);
}

if (isset($_GET['income_report'])) {

    // if ($checkAccess["access_report_payroll"] == "0") {
    //     $response["caption"] = "<h1 class='text-danger'>You do not have permission to access Payroll Report</h1>";
    //     $response["columns"] = array();
    //     $response["rows"] = array();
    //     $response["others"] = array();

    //     echo json_encode($response);
    //     return;
    // }

    $data = json_decode($_GET['income_report']);
    $schoolYear = getOpenSchoolYear();
    $response = array();
    $from = date("F j, Y", strtotime($data->data->dateFrom));
    $to = date("F j, Y", strtotime($data->data->dateTo));
    $response["caption"] = "<h4>Income Report</h4>
                            <h5>Date From : {$from} - {$to}</h5>";
    //th columns
    $columns = array(
        "STUD #", "OR #", "APPROVED BY", "DATE PAID", "REMARKS", "AMOUNT"
    );

    $response["columns"] = $columns;

    $database->where("date_paid", array($data->data->dateFrom, $data->data->dateTo), "BETWEEN");
    $database->where("status", "APPROVED");
    $payments = $database->get(STUDENT_PAYMENT_HISTORY);

    $response["rows"] = $payments;

    $overAll = 0;
    foreach ($payments as $p) {
        $overAll = $overAll + $p["amount"];
    }

    $response["others"] = array(
        array(
            "name" => "OVERALL TOTAL PAYMENTS",
            "value" => number_format($overAll, 2),
            "is_numeric" => "true"
        ),
    );

    echo json_encode($response);
}