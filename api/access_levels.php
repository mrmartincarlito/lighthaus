<?php
require_once("config.php");
require_once("logs.php");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    $access_levels = Array ();
    $access_levels_keys = array_keys($database->getOne ('access_levels'));
    foreach ($access_levels_keys as $key){
        if(preg_match('/access/', $key)){
            $access_levels = array_merge($access_levels, Array(
                $key => (@$data->$key) ? "1" : "0",
            ));
        }
    }

    if($data->formAction == "add"){
        $insertData = Array (
            "description" => $data->description
        );
        $insertData = array_merge($insertData,$access_levels);

        $id = $database->insert ('access_levels', $insertData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Access Level added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){
        $updateData = Array (
            "description" => $data->description,
            "is_deleted" => 0,
        );
        $updateData = array_merge($updateData,$access_levels);


        $database->where ('id', $data->modifyId);
        $id = $database->update ('access_levels', $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Access Level details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update ('access_levels', $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Access Level deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} Access Level: {$data->description}");
    }else{
        saveLog($database,"{$data->formAction} Access Level ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $database->where ('is_deleted', 0);
    $userDB = $database->get("access_levels");
    echo json_encode($userDB);
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("ID", $id);
    $userDB = $database->getOne("access_levels");
    echo json_encode($userDB);
}

?>