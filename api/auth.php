<?php
require_once("config.php");
require_once("logs.php");

function getLoggedUserDetails($database){
    $username = @$_SESSION["username"];

    $cols = Array ("s.id as acc_id", "s.complete_name", "s.username", "s.role", "s.email", "s.password", "a.*");
    $database->where ("username", $username);
    $database->join ("access_levels a", "s.role=a.ID", "LEFT");
    $userDB = $database->getOne("accounts s", $cols);
    return json_encode($userDB);
}

function setLoggedUser($username){
    $_SESSION["username"] = $username;
}

function setLoggedStudent($student) {
    setcookie('student-logged-in', $student , time() + 86400, "/");
}

if(isset($_GET["getLoggedUserDetails"])){
    echo getLoggedUserDetails($database);
}

if(isset($_GET["getLoggedEnrolledStudent"])) {
    $student = getLoggedStudent();
    echo json_encode($student);
}

if(isset($_POST["data"]) && preg_match('/auth.php/', $actual_link)) {
    $postData = json_decode($_POST["data"]);
    $action = $postData->action;
    $data = json_decode($postData->data);

    if($action == "login"){
        $database->where ("username", $data->username);
        $userDB = $database->getOne ("accounts");
        if($userDB['username'] == $data->username && password_verify($data->password,$userDB['password'])){
            $student = array();
            if ($userDB["role"] == "47") { //for student
                setLoggedStudent($userDB['username']);
                $loggedStudent = getStudentInfo();

                $database->where("id", $loggedStudent["student_id"]);
                $student = $database->getOne(STUDENTS);
            } else {
                setLoggedUser($userDB['username']);
                saveLog($database,"Logged in");
            }

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Logged in succesfully!",
                "info" => json_encode($userDB),
                "student" => json_encode($student)
            ));

        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => "Wrong username or password!"
            ));
        }
    }
}



?>