<?php
require_once("config.php");
require_once("logs.php");

if(isset($_GET["get"])){
    $primaryKey = 'id';
    $columns = array(
        array( 'db' => 'id', 'dt' => 0 ),
        array( 'db' => 'student_number', 'dt' => 1 ),
        array( 'db' => 'amount', 'dt' => 2 ),
        array( 'db' => 'remarks', 'dt' => 3 ),
        array( 'db' => 'approved_by',   'dt' => 4 ),
        array( 'db' => 'date_approved',   'dt' => 5 ),
        array(  'db' => 'status',   
                'dt' => 6,
                'formatter' => function ($data, $row){
                    $label = "alert alert-info";

                    if ($data == "APPROVED") {
                        $label = "alert alert-success";
                    }

                    if ($data == "REJECTED") {
                        $label = "alert alert-danger";
                    }

                    return '<span class="label '.$label.' label-rouded">'.$data.'</span>';
                }
            ),
        array(  'db' => 'id',   
                'dt' => 7 ,
                'formatter' => function($data ,$row) {

                    return '<button type="button" class="btn btn-primary btn-circle" onclick="modify('.$data.')" title="View Payment"><i class="fa fa-eye"></i> </button>
                    <button type="button" class="btn btn-primary btn-circle" onclick="viewImage('.$data.')" title="Receipt"><i class="fa fa-download"></i> </button>';
                    
                }
            ),
    );
    
    $condition = "is_deleted = 0";

    if(isset($_GET['status'])){
        $condition .= " and status = '{$_GET['status']}'";
    }

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, STUDENT_PAYMENT_HISTORY , $primaryKey, $columns, $condition )
    );
}

if (isset($_GET['getDetails'])) {
    $id = $_GET['getDetails'];

    $database->where("id", $id);
    $payment = $database->getOne(STUDENT_PAYMENT_HISTORY, array("id", "amount", "date_paid", "student_number", "approved_by", "date_approved"
    , "is_approved", "or_number", "status", "remarks", "image_file"));
    echo json_encode($payment);
}


if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    $approvedBy = "";
    $dateApproved = NULL;
    $isApproved = 0;

    if ($data->status == "APPROVED") {
        $approvedBy = $_SESSION["username"];
        $dateApproved = $database->now();
        $isApproved = 1;
    }


    if($data->formAction == "add"){

        $insertData = Array (
            "amount" => $data->amount,
            "date_paid" => $data->date_paid,
            "status" => $data->status,
            "approved_by" => $approvedBy,
            "date_approved" => $dateApproved,
            "is_approved" => $isApproved,
            "or_number" => $data->or_number,
            "student_number" => $data->student_number,
            "remarks" => $data->remarks
        );

        $id = $database->insert (STUDENT_PAYMENT_HISTORY, $insertData);
        if($id){
            $_SESSION['payment-history-id'] = $id;
            $selectedPayment = json_decode($_POST['selectedPayment']);

            foreach ($selectedPayment as $paymentId) {
                $database->where("id", $paymentId);
                $database->update(STUDENT_PAYMENTS, array(
                    "payment_history_id" => $id,
                    "status" => $data->status,
                    "remarks" => $data->remarks,
                    "added_by" => $_SESSION["username"]
                ));
            }

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Student Payment added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "date_paid" => $data->date_paid,
            "approved_by" => $approvedBy,
            "date_approved" => $dateApproved,
            "is_approved" => $isApproved,
            "status" => $data->status,
            "remarks" => $data->remarks,
            "or_number" => $data->or_number,
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (STUDENT_PAYMENT_HISTORY, $updateData);
        if($id){

            if ($isApproved) {
                //set student enrolled
                $database->where("payment_history_id", $data->modifyId);
                $database->update(STUDENT_PAYMENTS, array(
                    "status" => $data->status,
                    "remarks" => $data->remarks
                ));

                $database->where("student_number", $data->student_number);
                $database->update(ENROLLED_STUDENTS, array(
                    "status" => "ENROLLED",
                    "last_payment_amount" => $data->amount,
                    "last_payment_date" => $data->date_paid
                ));
            } else {
                $database->where("payment_history_id", $data->modifyId);
                $database->update(STUDENT_PAYMENTS, array(
                    "status" => "DUE",
                    "remarks" => $data->remarks
                ));
            }

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Payment details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (STUDENT_PAYMENT_HISTORY, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "PAYMENTS deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} PAYMENT: {$data->student_number}");
    }else{
        saveLog($database,"{$data->formAction} PAYMENT ID {$data->modifyId}");
    }
}

if (isset($_GET['getStudent'])) {
    $studentNumber = $_GET['getStudent'];

    $database->where("student_number", $studentNumber);
    $enrolled = $database->getOne(ENROLLED_STUDENTS);

    if (empty($enrolled)) {
        $response["student"] = array(
            "fullName" => "",
            "grade_level" => null
        );
    
        $response["payments"] = null;
        echo json_encode($response);
        return;
    }

    $database->where("id", $enrolled["student_id"]);
    $student = $database->getOne(STUDENTS);

    $database->where("student_number", $studentNumber);
    $database->where("payment_history_id", 0);
    $database->where("status" , "");
    $database->where("is_deleted", 0);
    $payments = $database->get(STUDENT_PAYMENTS);

    $response["student"] = array(
        "fullName" => $student["fname"] . " " .$student["mname"] . " ". $student["lname"],
        "grade_level" => $student["grade_level"]
    );

    $response["payments"] = $payments;

    echo json_encode($response);

}

if (isset($_GET['setPaymentStatus'])) {
    $isDue = $_GET['isDue'];
    $paymentId = $_GET['paymentId'];

    $database->where("id", $paymentId);
    $id = $database->update(STUDENT_PAYMENTS, array(
        "status" => $isDue == "1" ? "DUE" : "",
    ));
    if ($id) {

        if ($isDue == "0") {
            $database->where("id", $paymentId);
            $id = $database->update(STUDENT_PAYMENTS, array(
                "is_sms" => 0,
                "is_emailed" => 0
            ));
        }

        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Student Payment updated successfully!"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
    }

    saveLog($database,"UPDATE STUDENT PAYMENT: $paymentId");
}