<?php
require_once("config.php");
require_once("auth.php");
require_once("email-template.php");

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    $schoolYear = getOpenSchoolYear();
    
    // $database->where("student_id", $data->modifyId);
    // $database->where("school_year", $schoolYear['id']);
    // $current = $database->getOne(ENROLLED_STUDENTS);

    // if (!empty($current)) {
    //     if ($current["status"] != "PENDING" && $current["status"] != "") {
    //         echo json_encode(Array (
    //             "type" => "error",
    //             "title" => "Error!",
    //             "text" => "This is already " . $current["status"]
    //         ));
    //         return;
    //     }
    // }
    
    if($data->formAction == "add"){ 

        $getCurrentEnrollmentPayment = getEnrollmentPaymentBreakDown();

        //get student exisiting balance
        $balance = $database->rawQuery(
            "SELECT sum(billed_amount) as balance
            FROM ".STUDENT_PAYMENTS." where

            student_number = '{$current['student_number']}'
            AND (status = '' OR status = 'DUE' OR status = 'PENDING')
            "
        );


        $database->where("school_year", getOpenSchoolYear()['id']);
        $database->where("student_id", $data->modifyId);
        $current = $database->getOne(ENROLLED_STUDENTS);
        $newBalance = $getCurrentEnrollmentPayment['overAllPayment'][strtolower($current['transaction_type'])];

        $updateData = Array (
            "status" => "PENDING FOR INTIAL DP",
            "approved_by" => $_SESSION["username"],
            "approved_date" => $database->now(),
            "balance" => $newBalance + $balance[0]['balance']
        );

        $database->where ("school_year", getOpenSchoolYear()['id']);
        $database->where ('student_id', $data->modifyId);
        $id = $database->update (ENROLLED_STUDENTS, $updateData);
        if($id){

            //update downpayment to be due
            $database->where("student_number", $current['student_number']);
            $database->where("payment_history_id", 0);
            if ($current['transaction_type'] == "CASH") {
                $database->where("payment_description", "Full Payment");
            } else {
                $database->where("payment_description", "Down Payment");
            }
            $database->update(STUDENT_PAYMENTS, array(
                "billed_date" => date("d-m-Y"),
                "status" => "DUE"
            ));

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "You just approved a student evaluation. Need to complete payment to be able to complete the enrollment"
            ));

            $student = getStudentByStudentId($data->modifyId);
            sendEmail($student['email'], "LHCS - Evaluation Status", evaluationApproved(
                $student['fname']
            ));
            
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "status" => "REJECTED",
            "rejected_by" => $_SESSION["username"],
            "rejected_date" => $database->now()
        );

        $database->where ("school_year", getOpenSchoolYear()['id']);
        $database->where ('student_id', $data->modifyId);
        $id = $database->update (ENROLLED_STUDENTS, $updateData);
        if($id){
            //Remove initial payment record

            $database->where("student_id", $data->modifyId);
            $enrolledStudent = $database->getOne(ENROLLED_STUDENTS);

            $database->where("student_number", $enrolledStudent['student_number']);
            $database->delete(STUDENT_PAYMENTS);

            //end removal payment record

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "You just rejected a student evaluation"
            ));

            $student = getStudentByStudentId($data->modifyId);
            sendEmail($student['email'], "LHCS - Evaluation Status", evaluationRejected(
                $student['fname']
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} STUDENT ID: {$data->modifyId}");
    }else{
        saveLog($database,"{$data->formAction} STUDENT ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){

    $condition = "is_deleted = 0";
    $status = "APPROVED";
    $statusFilter = "PENDING";

    if(isset($_GET['filter'])){
        $filter = json_decode($_GET["filter"]);

        if (!empty($filter->status)) {
            $statusFilter = $filter->status;
        }

        if(!empty($filter->grade_level)){
            $condition = $condition . " and grade_level = '".$filter->grade_level."'";
        }

        if(!empty($filter->gender)){
            $condition = $condition . " and gender = '".$filter->gender."'";
        }
    }

    $primaryKey = 'id';
    $columns = array(
        //array( 'db' => 'id', 'dt' => 0 ),
        array( 'db' => 'id', 
                'dt' => 0 ,
                'formatter' => function ($data, $row) {
                    global $database;

                    $database->where("student_id", $data);
                    $stud = $database->getOne(ENROLLED_STUDENTS);

                    return $stud["student_number"];
                }
            ),
        array( 'db' => 'grade_level', 'dt' => 1 ),
        array( 'db' => 'fname', 'dt' => 2 ),
        array( 'db' => 'mname', 'dt' => 3 ),
        array( 'db' => 'lname', 'dt' => 4 ),
        array( 'db' => 'approved_by', 'dt' => 5 ),
        array(  'db' => 'id', 
                'dt' => 6,
                'formatter' => function ($data, $row) {
                    global $statusFilter;
                    $editButton = "";

                    if ($statusFilter == "ENROLLED") 
                        $editButton = '<button type="button" title="Edit Student Info" onclick="editStudent(' . $data . ')" class="btn btn-info btn-circle"><i class="fa fa-pencil"></i> </button>';
                    
                    return '
                    <button type="button" title="View Student Info" onclick="viewStudent(' . $data . ')" class="btn btn-primary btn-circle"><i class="fa fa-eye"></i> </button>
                    ' .$editButton. '
                    ';
                }
            ),
    );
    
    $condition = $condition . " and status = '".$status."' and 
        id IN (
            select student_id from enrolled_students
            where status = '$statusFilter' and school_year = ( select id from semester where is_opened = 1 )
        )";

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, STUDENTS , $primaryKey, $columns, $condition )
    );
}

if(isset($_GET['getDetails'])){
    $studId = $_GET['getDetails'];

    $database->where("id", $studId);
    $student = $database->getOne(STUDENTS);

    $database->where("student_id", $studId);
    $enrolledStudent = $database->getOne(ENROLLED_STUDENTS);

    $response['students'] = $student;

    $enrolledInfo = $database->rawQuery("
        SELECT *, 
            (
                SELECT SUM(billed_amount) 
                FROM student_payments
                WHERE payment_history_id = 0 AND student_number = '{$enrolledStudent['student_number']}'
            ) as total_balance

        FROM enrolled_students
        WHERE student_number = '{$enrolledStudent['student_number']}'
        ORDER BY school_year DESC
        LIMIT 1
    ");

    $response['enrolledInfo'] = $enrolledInfo[0];


    $database->where("student_number", $enrolledInfo[0]['student_number']);
    //$database->where("school_year", $schoolYear['id']);
    $payments = $database->get(STUDENT_PAYMENTS);

    $response['all_payments'] = $payments;

    echo json_encode($response);
}

