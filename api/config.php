<?php
$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

require_once("auth.php");
require_once("db.php");
require_once("ssp.class.php");

date_default_timezone_set('Asia/Manila');

checkBillingBalancesDue();

if(!isset($_SESSION['username']) && !preg_match('/login.php/', $actual_link)&& !preg_match('/auth.php/', $actual_link)){
    header('Location: login.php');
}

if(isset($_SESSION['username']) && preg_match('/login.php/', $actual_link)){
    header('Location: index.php');
}

if(isset($_SESSION['username']) && (!preg_match('/login.php/', $actual_link) || !preg_match('/auth.php/', $actual_link))){
    $userDetailsJSON = json_decode(getLoggedUserDetails($database));

    foreach ($userDetailsJSON as $key => $val) {
        if(preg_match('/access/', $key)){
            $url = "access_".substr($actual_link,strpos($actual_link,"?")+1);

            if($val != 1 && preg_match("/{$key}/", $url)){
                header('Location: ?not_allowed');
            }
        }
    }
} 

?>