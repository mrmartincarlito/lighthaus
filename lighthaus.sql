-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 05, 2022 at 10:30 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lighthaus`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_levels`
--

CREATE TABLE `access_levels` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `access_user_accounts` int(11) NOT NULL DEFAULT 0,
  `access_access_levels` int(11) NOT NULL DEFAULT 0,
  `access_dashboard` int(11) NOT NULL DEFAULT 0,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_levels`
--

INSERT INTO `access_levels` (`id`, `description`, `access_user_accounts`, `access_access_levels`, `access_dashboard`, `is_deleted`) VALUES
(3, 'Administrator', 1, 1, 1, 0),
(4, 'User', 1, 0, 0, 0),
(45, 'Customer', 0, 0, 0, 1),
(46, 'Faculty', 0, 0, 0, 0),
(47, 'Student', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `complete_name` varchar(50) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `role` varchar(255) NOT NULL,
  `is_deleted` int(1) NOT NULL DEFAULT 0,
  `image_file` text DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `complete_name`, `username`, `password`, `email`, `role`, `is_deleted`, `image_file`, `date`) VALUES
(7, 'Rigor Abargos', 'EMP-0001', '$2y$10$1fHe9SEOj/cbrZqsX0lhWuRAfRO9Mm4powWhrdim53moagWZMAXSm', 'sample@email.com', '3', 0, 'uploads/dp.jpg', '2022-06-26 01:33:04'),
(16, 'test 2', 'student', '$2y$10$ZMGRyqdrlp0pZwjLmnTlgeA469nTp12Pw8qAtsvXQLREV0.NoVANG', 'tobechange@email.com', '47', 0, NULL, '2022-02-17 01:04:43'),
(17, 'Juan Ponce Enrile', 'E-0002', '$2y$10$mPrBct92DXIzDl.lAK/p.OI7M/Y1obZmx9L/2XThw5R9FA8COPQ8O', 'tobechange@email.com', '47', 0, NULL, '2022-06-26 01:25:50'),
(18, 'asd', 'E-0003', '$2y$10$h4pH2KNUEQ9orjHKHzyCK.IniBBtXutIIXncqax54OqA2sBS7sj8a', 'tobechange@email.com', '47', 0, NULL, '2022-02-06 05:21:54'),
(19, 'test 3', 'E-0004', '$2y$10$/yvp3j9Q054DvnmVQL7lv.aHgrYNyLO/vhLpscWlGFs6HnG9TbfbC', 'tobechange@email.com', '47', 0, NULL, '2022-02-10 02:47:47'),
(20, 'Test 4 Test 4', 'E-0005', '$2y$10$nKeoCwTdC/v3O2xI0LNTcOsQ9v5kGMaFKdaV6raaDCjS7HGAFcXf.', 'tobechange@email.com', '47', 0, NULL, '2022-02-13 00:33:11'),
(21, 'Test 4 Test 4', 'E-0006', '$2y$10$56l09mZUfsMDwJ3uVSLMJebcBs.slAqrzo0pJDLIDWm1OOQQUQ2Rq', 'tobechange@email.com', '47', 0, NULL, '2022-02-13 00:33:57'),
(22, 'Test 3 Test 3', 'E-0007', '$2y$10$G2t3RfQ21qdM5BM55v49PuqNhWIo43TuLe8OF4UYAzn3PUr4nDd1m', 'tobechange@email.com', '47', 0, NULL, '2022-02-13 00:34:42'),
(30, 'Alana Conway', 'E-0008', '$2y$10$GTi58r/CpwOB0A4cg1p53.GO35ahH7kWcRJ6XY//4TrGAJGGZ6d.u', 'tobechange@email.com', '47', 0, NULL, '2022-02-13 01:11:20'),
(31, 'Xander Hunt', 'E-0009', '$2y$10$6CEmK6NJSb31nhBz/U64juPRHWEjAcUuOTQ7.WTWQ/4INaiRxVM..', 'tobechange@email.com', '47', 0, NULL, '2022-02-13 01:11:25'),
(32, 'Rigel Jarvis', 'E-0010', '$2y$10$.vDDLfJmEB3ltpbUVD3ij.Sm4qKOlGsEcIhQ.wewwP8XHfzsVd61q', 'tobechange@email.com', '47', 0, NULL, '2022-06-26 01:29:47'),
(34, 'Cally Wilder', 'E-0011', '$2y$10$SvGNMdB1V8M5niKSwGg5xuQjBQ.GTEgXcMb9KuHX6HB9klqvXg0aO', 'mrmartincarlito@gmail.com', '47', 0, NULL, '2022-06-26 01:27:21'),
(37, 'Carlito', 'carlito123', '$2y$10$ipQ7BXVaDlukIcc5laLPvuqrjvXD5ZgTLLyN0oPyEgGs2pWztOrO2', '', '46', 0, NULL, '2022-04-04 23:11:09'),
(40, 'Carlito', 'carlito1234', '$2y$10$cGZHitVwsdxla.VrpmX54.jZrRRDmI3eFxf06VrsXDl28oCT1IqsG', '', '46', 0, NULL, '2022-04-04 23:11:58'),
(41, 'test', 'test123', '$2y$10$6CgUs4GJx1A4GS0C1lgBGO9I5TmDkhb3sa628RoFd65DGLeZpUu1C', '', '46', 0, NULL, '2022-04-04 23:12:41'),
(43, 'test', 'test1234', '$2y$10$vSsE4pt/4swNAFaYSwx3QecyFPA4.vf2Isl7JKQZgfB1qFduOhln2', 'mrmartincarlito@gmail.comasdasd', '46', 0, NULL, '2022-07-02 07:00:08'),
(44, 'Brianna Martinez Kalia Mcmahon', 'E-0012', '$2y$10$ZMGRyqdrlp0pZwjLmnTlgeA469nTp12Pw8qAtsvXQLREV0.NoVANG', 'mrmartincarlito@gmail.com', '47', 0, NULL, '2022-05-14 07:58:33');

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `image_file` text NOT NULL,
  `added_by` varchar(255) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`id`, `description`, `image_file`, `added_by`, `date_time`, `is_deleted`) VALUES
(1, 'Test description', '', '', '2022-05-15 01:26:25', 1),
(2, 'test description', 'announcements/532d0341186be12412c6a4362cc08710.jpg', 'EMP-0001', '2022-05-15 01:34:16', 0),
(3, 'announcement 2', 'announcements/doctor-strange-in-the-multiverse-of-madness-4k-2020-pw.jpg', 'EMP-0001', '2022-05-15 01:34:31', 0),
(4, 'test no image', 'announcements/eternals-k7-1920x1080.jpg', 'EMP-0001', '2022-05-15 02:26:52', 1),
(5, 'test no image\r\n', '', 'EMP-0001', '2022-05-15 02:26:58', 0);

-- --------------------------------------------------------

--
-- Table structure for table `enrolled_students`
--

CREATE TABLE `enrolled_students` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `student_number` varchar(50) NOT NULL,
  `grade_level` varchar(20) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'PENDING',
  `school_year` int(50) NOT NULL,
  `school_year_status` varchar(50) DEFAULT 'ON GOING',
  `balance` decimal(10,2) DEFAULT 0.00,
  `transaction_type` varchar(50) NOT NULL,
  `last_payment_amount` decimal(10,2) DEFAULT NULL,
  `last_payment_date` datetime DEFAULT NULL,
  `approved_by` varchar(50) NOT NULL,
  `approved_date` datetime DEFAULT NULL,
  `rejected_by` varchar(50) NOT NULL,
  `rejected_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `enrolled_students`
--

INSERT INTO `enrolled_students` (`id`, `student_id`, `student_number`, `grade_level`, `status`, `school_year`, `school_year_status`, `balance`, `transaction_type`, `last_payment_amount`, `last_payment_date`, `approved_by`, `approved_date`, `rejected_by`, `rejected_date`) VALUES
(19, 4, 'student', '', '', 5, 'ON GOING', '0.00', '', NULL, NULL, 'admin', '2022-02-13 08:39:52', '', NULL),
(20, 3, 'E-0002', '', 'PENDING FOR INTIAL DP', 5, 'ON GOING', '0.00', '', NULL, NULL, 'admin', '2022-02-13 08:39:58', '', NULL),
(21, 2, 'E-0003', '', 'PENDING FOR INTIAL DP', 5, 'ON GOING', '0.00', '', NULL, NULL, 'admin', '2022-02-13 08:40:05', '', NULL),
(22, 1, 'E-0004', '', 'PENDING FOR INTIAL DP', 5, 'ON GOING', '0.00', '', NULL, NULL, 'admin', '2022-02-13 08:40:11', '', NULL),
(23, 12, 'E-0005', '', 'PENDING FOR INTIAL DP', 5, 'ON GOING', '0.00', '', NULL, NULL, 'admin', '2022-02-13 09:12:48', '', NULL),
(24, 11, 'E-0006', '', 'PENDING FOR INTIAL DP', 5, 'ON GOING', '0.00', '', NULL, NULL, 'admin', '2022-02-13 09:12:55', '', NULL),
(25, 10, 'E-0007', '', 'ENROLLED', 5, 'COMPLETED', '0.00', '', NULL, NULL, 'EMP-0001', '2022-03-17 07:03:51', '', NULL),
(26, 9, 'E-0008', 'Kinder 1', 'ENROLLED', 5, 'COMPLETED', '0.00', 'CASH', NULL, NULL, 'EMP-0001', '2022-03-17 07:07:19', '', NULL),
(27, 8, 'E-0009', '', 'REJECTED', 5, 'ON GOING', '24908.64', 'QUARTERLY', NULL, NULL, 'EMP-0001', '2022-03-29 07:31:56', 'EMP-0001', '2022-05-14 09:09:49'),
(28, 13, 'E-0010', 'Kinder 1', 'ENROLLED', 5, 'COMPLETED', '24908.64', 'QUARTERLY', '22957.60', '2022-07-02 00:00:00', 'EMP-0001', '2022-03-21 07:22:17', '', NULL),
(30, 14, 'E-0011', '', '', 5, 'ON GOING', '0.00', '', NULL, NULL, '', NULL, '', NULL),
(31, 21, 'E-0012', 'Grade 3', 'ENROLLED', 5, 'COMPLETED', '50417.60', 'MONTHLY', '11286.13', '2022-06-26 00:00:00', 'EMP-0001', '2022-05-14 16:01:16', 'EMP-0001', '2022-05-14 16:00:12'),
(37, 21, 'E-0012', 'Grade 4', 'ENROLLED', 8, 'ON GOING', '185016.17', 'SEMESTRAL', '11286.13', '2022-06-26 00:00:00', 'EMP-0001', '2022-06-26 13:38:30', '', NULL),
(39, 21, 'E-0012', 'Grade 5', 'ENROLLED', 9, 'ON GOING', '69654.17', 'SEMESTRAL', '11286.13', '2022-06-26 00:00:00', 'EMP-0001', '2022-06-26 13:51:10', '', NULL),
(40, 13, 'E-0010', 'Kinder 2', 'ENROLLED', 7, 'COMPLETED', '56179.44', 'QUARTERLY', '22957.60', '2022-07-02 00:00:00', 'EMP-0001', '2022-06-29 07:26:51', '', NULL),
(41, 13, 'E-0010', 'Grade 1', 'ENROLLED', 12, 'COMPLETED', '22957.60', 'CASH', '22957.60', '2022-07-02 00:00:00', 'EMP-0001', '2022-07-02 15:17:26', 'EMP-0001', '2022-07-02 15:10:04'),
(42, 13, 'E-0010', 'Grade 1', 'ENROLLED', 12, 'COMPLETED', '22957.60', 'CASH', '22957.60', '2022-07-02 00:00:00', 'EMP-0001', '2022-07-02 15:17:26', 'EMP-0001', '2022-07-02 15:10:04'),
(43, 13, 'E-0010', 'Grade 1', 'ENROLLED', 12, 'COMPLETED', '22957.60', 'CASH', '22957.60', '2022-07-02 00:00:00', 'EMP-0001', '2022-07-02 15:17:26', 'EMP-0001', '2022-07-02 15:10:04'),
(44, 13, 'E-0010', 'Grade 1', 'ENROLLED', 12, 'COMPLETED', '22957.60', 'CASH', '22957.60', '2022-07-02 00:00:00', 'EMP-0001', '2022-07-02 15:17:26', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `faculties`
--

CREATE TABLE `faculties` (
  `id` int(11) NOT NULL,
  `emp_id` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `position` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `added_by` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculties`
--

INSERT INTO `faculties` (`id`, `emp_id`, `fname`, `mname`, `lname`, `position`, `email`, `created_at`, `updated_at`, `added_by`, `is_deleted`) VALUES
(1, 'EMP-0001', 'August', 'Aubrey Casey', 'Olsen', 'Sint in ut commodi ', '', '2022-02-02 07:17:02', '2022-03-15 22:53:28', 'admin', 0),
(2, 'In nisi doloribus ad', 'Shelby', 'Christian Travis', 'Morris', 'Cumque occaecat sunt', '', '2022-02-02 07:18:07', '2022-02-01 23:18:07', 'admin', 0),
(3, 'Dolore dolores ut de', 'Wang Rosario', 'Lareina Carlson', 'Hollee Horton', 'Vero impedit sed ex', 'asdasd@asdasda', '2022-02-02 07:19:04', '2022-07-02 07:02:15', 'EMP-0001', 0);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `account_id` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `account_id`, `description`, `ip`, `hostname`, `timestamp`, `is_deleted`) VALUES
(275, '3', 'Add access level: test', '::1', 'VivoBook-S14', '2018-10-09 03:38:44', 0),
(276, '3', 'Delete access level id 5', '::1', 'VivoBook-S14', '2018-10-09 03:39:13', 0),
(277, '3', 'Add access level: test', '::1', 'VivoBook-S14', '2018-10-09 03:39:16', 0),
(278, '3', 'Add access level: test', '::1', 'VivoBook-S14', '2018-10-09 03:39:50', 0),
(365, '3', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-01-29 03:10:56', 0),
(366, '3', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-01-29 03:18:35', 0),
(367, '3', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-01-29 03:19:14', 0),
(368, '3', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-01-29 03:21:20', 0),
(369, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-01 22:56:56', 0),
(370, '3', 'Add subject: filipino', '::1', 'DESKTOP-FO8SD5D', '2022-02-01 22:58:47', 0),
(371, '3', 'Add subject: science', '::1', 'DESKTOP-FO8SD5D', '2022-02-01 23:01:46', 0),
(372, '3', 'Add subject: filipino', '::1', 'DESKTOP-FO8SD5D', '2022-02-01 23:01:58', 0),
(373, '3', 'Add subject: asdas', '::1', 'DESKTOP-FO8SD5D', '2022-02-01 23:02:21', 0),
(374, '3', 'Add subject: dfgdfg', '::1', 'DESKTOP-FO8SD5D', '2022-02-01 23:04:09', 0),
(375, '3', 'Add subject: sdfsdf', '::1', 'DESKTOP-FO8SD5D', '2022-02-01 23:04:33', 0),
(376, '3', 'Add subject: asdasd', '::1', 'DESKTOP-FO8SD5D', '2022-02-01 23:05:24', 0),
(377, '3', 'Add subject: asdasd', '::1', 'DESKTOP-FO8SD5D', '2022-02-01 23:05:27', 0),
(378, '3', 'Add faculty: ', '::1', 'DESKTOP-FO8SD5D', '2022-02-01 23:17:02', 0),
(379, '3', 'Add faculty: in nisi doloribus ad', '::1', 'DESKTOP-FO8SD5D', '2022-02-01 23:18:07', 0),
(380, '3', 'Add faculty: dolore dolores ut de', '::1', 'DESKTOP-FO8SD5D', '2022-02-01 23:19:04', 0),
(381, '3', 'Add subject: nesciunt reprehende', '::1', 'DESKTOP-FO8SD5D', '2022-02-01 23:26:47', 0),
(382, '3', 'Add subject: sdfsdfs', '::1', 'DESKTOP-FO8SD5D', '2022-02-01 23:26:54', 0),
(383, '3', 'Add subject: sdfsdf', '::1', 'DESKTOP-FO8SD5D', '2022-02-01 23:27:07', 0),
(384, '3', 'Add payment: tenetur ut quo iste ', '::1', 'DESKTOP-FO8SD5D', '2022-02-02 00:10:23', 0),
(385, '3', 'Edit payment id 3', '::1', 'DESKTOP-FO8SD5D', '2022-02-02 00:10:36', 0),
(386, '3', 'Delete payment id 3', '::1', 'DESKTOP-FO8SD5D', '2022-02-02 00:21:45', 0),
(387, '3', 'Delete payment id 2', '::1', 'DESKTOP-FO8SD5D', '2022-02-02 00:22:26', 0),
(388, '3', 'Add payment: tuition fee', '::1', 'DESKTOP-FO8SD5D', '2022-02-02 00:23:23', 0),
(389, '3', 'Add payment: registration fee', '::1', 'DESKTOP-FO8SD5D', '2022-02-02 00:23:45', 0),
(390, '3', 'Add payment: miscellaneous fee', '::1', 'DESKTOP-FO8SD5D', '2022-02-02 00:24:21', 0),
(391, '3', 'Add payment: other fees', '::1', 'DESKTOP-FO8SD5D', '2022-02-02 00:24:46', 0),
(392, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-02 22:53:13', 0),
(393, '3', 'Add payment: quo dolorum rerum am', '::1', 'DESKTOP-FO8SD5D', '2022-02-02 23:17:25', 0),
(394, '3', 'Delete payment id 8', '::1', 'DESKTOP-FO8SD5D', '2022-02-02 23:17:37', 0),
(395, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-03 22:53:28', 0),
(396, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:00:59', 0),
(397, '3', 'Delete student id ', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:24:03', 0),
(398, '3', 'Delete student id ', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:24:21', 0),
(399, '3', 'Delete student id 2', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:26:21', 0),
(400, '3', 'Add student: test', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:37:54', 0),
(401, '3', 'Add student: test', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:38:41', 0),
(402, '3', 'Add student: test', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:39:28', 0),
(403, '3', 'Add student: test', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:40:11', 0),
(404, '3', 'Add student: test', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:43:49', 0),
(405, '3', 'Add access level: faculty', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:51:55', 0),
(406, '3', 'Add access level: student', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:52:00', 0),
(407, '3', 'Edit subject id 11', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:54:04', 0),
(408, '3', 'Delete subject id 10', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:54:08', 0),
(409, '3', 'Delete subject id 9', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:54:11', 0),
(410, '3', 'Delete subject id 8', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:54:14', 0),
(411, '3', 'Delete subject id 7', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:54:16', 0),
(412, '3', 'Add subject: filipino', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:54:28', 0),
(413, '3', 'Add subject: math', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:54:39', 0),
(414, '3', 'Add subject: christian living', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:55:03', 0),
(415, '3', 'Add subject: sibika at kultura\\', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:55:44', 0),
(416, '3', 'Add subject: msp', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:56:03', 0),
(417, '3', 'Add subject: math', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:56:12', 0),
(418, '3', 'Add subject: english', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:56:14', 0),
(419, '3', 'Add subject: filipino', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:56:16', 0),
(420, '3', 'Add subject: sibika at kultura', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:56:22', 0),
(421, '3', 'Add subject: math', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:56:26', 0),
(422, '3', 'Delete subject id 21', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:56:48', 0),
(423, '3', 'Delete subject id 20', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:56:51', 0),
(424, '3', 'Delete subject id 19', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:56:53', 0),
(425, '3', 'Delete subject id 18', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:56:56', 0),
(426, '3', 'Delete subject id 17', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:56:59', 0),
(427, '3', 'Add subject: math', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:57:58', 0),
(428, '3', 'Add subject: english', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:00', 0),
(429, '3', 'Add subject: filipino', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:02', 0),
(430, '3', 'Add subject: math', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:13', 0),
(431, '3', 'Add subject: english', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:14', 0),
(432, '3', 'Add subject: filipino', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:16', 0),
(433, '3', 'Add subject: math', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:20', 0),
(434, '3', 'Add subject: english', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:21', 0),
(435, '3', 'Add subject: filipino', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:23', 0),
(436, '3', 'Add subject: math', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:28', 0),
(437, '3', 'Add subject: english', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:30', 0),
(438, '3', 'Add subject: filipino', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:32', 0),
(439, '3', 'Add subject: math', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:35', 0),
(440, '3', 'Add subject: englihs', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:37', 0),
(441, '3', 'Add subject: filipino', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:40', 0),
(442, '3', 'Add subject: math', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:44', 0),
(443, '3', 'Add subject: english', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:47', 0),
(444, '3', 'Add subject: filipino', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:49', 0),
(445, '3', 'Add subject: math', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:53', 0),
(446, '3', 'Add subject: english', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:55', 0),
(447, '3', 'Add subject: filipino', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:58:57', 0),
(448, '3', 'Add subject: math', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:59:01', 0),
(449, '3', 'Add subject: englihs', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:59:02', 0),
(450, '3', 'Add subject: filipino', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 00:59:04', 0),
(451, '3', 'Add student: test 2', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 01:50:38', 0),
(452, '3', 'Add student: test 2', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 01:52:06', 0),
(453, '3', 'Add student: test', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 01:52:24', 0),
(454, '3', 'Add student: test 2', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 01:53:37', 0),
(455, '3', 'Add student: test 2', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 01:55:33', 0),
(456, '3', 'Add student: test', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 01:55:58', 0),
(457, '3', 'Add student: test 2', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 02:16:58', 0),
(458, '3', 'Add student: test', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 02:18:06', 0),
(459, '3', 'Delete student id 1', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:07:33', 0),
(460, '3', 'Delete student id 1', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:07:58', 0),
(461, '3', 'Delete student id 1', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:08:47', 0),
(462, '3', 'Delete student id 1', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:09:00', 0),
(463, '3', 'Delete student id 1', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:09:09', 0),
(464, '3', 'Delete student id 1', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:10:23', 0),
(465, '3', 'Add student: ', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:10:36', 0),
(466, '3', 'Add student: ', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:10:41', 0),
(467, '3', 'Add student id: 1', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:11:02', 0),
(468, '3', 'Add student: test 2', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:11:28', 0),
(469, '3', 'Add student: test 2', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:16:26', 0),
(470, '3', 'Add student: juan ponce enrile', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:16:30', 0),
(471, '3', 'Add student: test 2', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:17:37', 0),
(472, '3', 'Add student: juan ponce enrile', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:17:46', 0),
(473, '3', 'Add student id: 2', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:18:08', 0),
(474, '3', 'Delete student id 1', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:18:37', 0),
(475, '3', 'Add student: asd', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:21:54', 0),
(476, '3', 'Add student id: 3', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:22:41', 0),
(477, '3', 'Add student id: 2', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:22:52', 0),
(478, '3', 'Add payment: tuition fee', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:31:47', 0),
(479, '3', 'Add payment: registration fee', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:34:08', 0),
(480, '3', 'Add payment: miscellaneous fee', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:34:54', 0),
(481, '3', 'Add payment: other fees', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 05:35:14', 0),
(482, '3', 'Add payment: test', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 07:07:49', 0),
(483, '3', 'Delete payment id 5', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 07:08:12', 0),
(484, 'Rigor Abargos', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 07:21:11', 0),
(485, 'Rigor Abargos', 'Edit payment id 1', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 07:22:39', 0),
(486, 'Rigor Abargos', 'Edit payment id 1', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 07:22:52', 0),
(487, 'Rigor Abargos', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-06 22:56:06', 0),
(488, 'Rigor Abargos', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-07 22:51:47', 0),
(489, 'Rigor Abargos', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-08 22:51:02', 0),
(490, 'Rigor Abargos', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-09 22:51:42', 0),
(491, 'Rigor Abargos', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-09 22:52:41', 0),
(492, 'Rigor Abargos', 'Add student: test 3', '::1', 'DESKTOP-FO8SD5D', '2022-02-10 02:47:47', 0),
(493, 'Rigor Abargos', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-02-10 23:12:23', 0),
(494, 'Rigor Abargos', 'Add payment: books', '::1', 'DESKTOP-FO8SD5D', '2022-02-10 23:19:10', 0),
(495, 'Rigor Abargos', 'Edit payment id 6', '::1', 'DESKTOP-FO8SD5D', '2022-02-10 23:19:24', 0),
(496, 'Rigor Abargos', 'Edit payment id 6', '::1', 'DESKTOP-FO8SD5D', '2022-02-10 23:19:31', 0),
(497, 'Rigor Abargos', 'Delete payment id 6', '::1', 'DESKTOP-FO8SD5D', '2022-02-10 23:19:51', 0),
(498, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-02-13 00:24:28', 0),
(499, 'Rigor Abargos', 'Add student: ', '::1', 'LAPTOP-UI866FP5', '2022-02-13 00:33:11', 0),
(500, 'Rigor Abargos', 'Add student: ', '::1', 'LAPTOP-UI866FP5', '2022-02-13 00:33:57', 0),
(501, 'Rigor Abargos', 'Add student: test 3 test 3', '::1', 'LAPTOP-UI866FP5', '2022-02-13 00:34:42', 0),
(502, 'Rigor Abargos', 'Add student id: 4', '::1', 'LAPTOP-UI866FP5', '2022-02-13 00:37:55', 0),
(503, 'Rigor Abargos', 'Add student: test 4 test 4', '::1', 'LAPTOP-UI866FP5', '2022-02-13 00:38:47', 0),
(504, 'Rigor Abargos', 'Add student: test 3 test 3', '::1', 'LAPTOP-UI866FP5', '2022-02-13 00:38:57', 0),
(505, 'Rigor Abargos', 'Add student: test 2 test 2', '::1', 'LAPTOP-UI866FP5', '2022-02-13 00:39:05', 0),
(506, 'Rigor Abargos', 'Add student: test1 test1', '::1', 'LAPTOP-UI866FP5', '2022-02-13 00:39:13', 0),
(507, 'Rigor Abargos', 'Add student id: 4', '::1', 'LAPTOP-UI866FP5', '2022-02-13 00:39:52', 0),
(508, 'Rigor Abargos', 'Add student id: 3', '::1', 'LAPTOP-UI866FP5', '2022-02-13 00:39:58', 0),
(509, 'Rigor Abargos', 'Add student id: 2', '::1', 'LAPTOP-UI866FP5', '2022-02-13 00:40:05', 0),
(510, 'Rigor Abargos', 'Add student id: 1', '::1', 'LAPTOP-UI866FP5', '2022-02-13 00:40:11', 0),
(511, 'Rigor Abargos', 'Add student: martha christensen', '::1', 'LAPTOP-UI866FP5', '2022-02-13 01:10:59', 0),
(512, 'Rigor Abargos', 'Add student: drake simon', '::1', 'LAPTOP-UI866FP5', '2022-02-13 01:11:05', 0),
(513, 'Rigor Abargos', 'Add student: inez silva', '::1', 'LAPTOP-UI866FP5', '2022-02-13 01:11:10', 0),
(514, 'Rigor Abargos', 'Add student: alana conway', '::1', 'LAPTOP-UI866FP5', '2022-02-13 01:11:20', 0),
(515, 'Rigor Abargos', 'Add student: xander hunt', '::1', 'LAPTOP-UI866FP5', '2022-02-13 01:11:25', 0),
(516, 'Rigor Abargos', 'Add student id: 12', '::1', 'LAPTOP-UI866FP5', '2022-02-13 01:12:48', 0),
(517, 'Rigor Abargos', 'Add student id: 11', '::1', 'LAPTOP-UI866FP5', '2022-02-13 01:12:55', 0),
(518, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-02-13 01:18:43', 0),
(519, 'asd', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-02-13 01:20:05', 0),
(520, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-03-07 22:50:51', 0),
(521, '3', 'Add student: rigel jarvis', '::1', 'DESKTOP-FO8SD5D', '2022-03-07 22:51:10', 0),
(522, '3', 'Add student id: 13', '::1', 'DESKTOP-FO8SD5D', '2022-03-07 22:52:53', 0),
(523, '3', 'Add student id: 13', '::1', 'DESKTOP-FO8SD5D', '2022-03-07 22:55:40', 0),
(524, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-03-08 22:44:32', 0),
(525, '3', 'Edit payment id 3', '::1', 'DESKTOP-FO8SD5D', '2022-03-08 23:07:18', 0),
(526, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-03-09 00:08:42', 0),
(527, '3', 'Edit payment id 7', '::1', 'DESKTOP-FO8SD5D', '2022-03-09 00:13:51', 0),
(528, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-03-09 22:37:51', 0),
(529, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-03-10 22:50:08', 0),
(530, '3', 'Add payment: ', '::1', 'DESKTOP-FO8SD5D', '2022-03-10 23:18:50', 0),
(531, '3', 'Add payment: ', '::1', 'DESKTOP-FO8SD5D', '2022-03-10 23:19:15', 0),
(532, '3', 'Add payment: ', '::1', 'DESKTOP-FO8SD5D', '2022-03-10 23:19:35', 0),
(533, '3', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-03-12 07:07:29', 0),
(534, '3', 'Add student id: 13', '::1', 'LAPTOP-UI866FP5', '2022-03-12 07:07:56', 0),
(535, '3', 'Add payment: test', '::1', 'LAPTOP-UI866FP5', '2022-03-12 07:11:51', 0),
(536, '3', 'Delete payment id 7', '::1', 'LAPTOP-UI866FP5', '2022-03-12 07:12:04', 0),
(537, '3', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-03-14 23:11:08', 0),
(538, '3', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-03-15 22:46:04', 0),
(539, '3', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-03-15 22:58:59', 0),
(540, '3', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-03-16 22:59:19', 0),
(541, '3', 'Add student id: 10', '::1', 'LAPTOP-UI866FP5', '2022-03-16 23:03:17', 0),
(542, '3', 'Add student id: 10', '::1', 'LAPTOP-UI866FP5', '2022-03-16 23:03:24', 0),
(543, '3', 'Add student id: 10', '::1', 'LAPTOP-UI866FP5', '2022-03-16 23:03:30', 0),
(544, '3', 'Add student id: 10', '::1', 'LAPTOP-UI866FP5', '2022-03-16 23:03:41', 0),
(545, '3', 'Add student id: 10', '::1', 'LAPTOP-UI866FP5', '2022-03-16 23:03:51', 0),
(546, '3', 'Add student id: 9', '::1', 'LAPTOP-UI866FP5', '2022-03-16 23:04:42', 0),
(547, '3', 'Add student id: 9', '::1', 'LAPTOP-UI866FP5', '2022-03-16 23:07:19', 0),
(548, '3', 'Add student id: 8', '::1', 'LAPTOP-UI866FP5', '2022-03-16 23:07:46', 0),
(549, '3', 'Add student id: 8', '::1', 'LAPTOP-UI866FP5', '2022-03-16 23:08:34', 0),
(550, '3', 'Add student id: 8', '::1', 'LAPTOP-UI866FP5', '2022-03-16 23:09:20', 0),
(551, '3', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-03-20 22:46:07', 0),
(552, 'Rigor Abargos', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-03-20 23:10:59', 0),
(553, 'Rigor Abargos', 'Edit payment id 8', '::1', 'DESKTOP-FO8SD5D', '2022-03-20 23:16:56', 0),
(554, 'Rigor Abargos', 'Add student id: 13', '::1', 'DESKTOP-FO8SD5D', '2022-03-20 23:22:18', 0),
(555, 'Rigor Abargos', 'Edit payment id 9', '::1', 'DESKTOP-FO8SD5D', '2022-03-20 23:24:35', 0),
(556, 'Rigor Abargos', 'Logged in', '::1', 'DESKTOP-FO8SD5D', '2022-03-28 22:53:12', 0),
(557, 'Rigor Abargos', 'Add student: cally wilder', '::1', 'DESKTOP-FO8SD5D', '2022-03-28 23:03:03', 0),
(558, 'Rigor Abargos', 'Add student: cally wilder', '::1', 'DESKTOP-FO8SD5D', '2022-03-28 23:06:21', 0),
(559, 'Rigor Abargos', 'Add student id: 8', '::1', 'DESKTOP-FO8SD5D', '2022-03-28 23:31:56', 0),
(560, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-04-04 22:53:24', 0),
(561, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-04-04 23:00:36', 0),
(562, 'Rigor Abargos', 'Add account: carlito', '::1', 'LAPTOP-UI866FP5', '2022-04-04 23:09:41', 0),
(563, 'Rigor Abargos', 'Add account: carlito', '::1', 'LAPTOP-UI866FP5', '2022-04-04 23:10:30', 0),
(564, 'Rigor Abargos', 'Add account: carlito', '::1', 'LAPTOP-UI866FP5', '2022-04-04 23:11:09', 0),
(565, 'Rigor Abargos', 'Add account: carlito', '::1', 'LAPTOP-UI866FP5', '2022-04-04 23:11:40', 0),
(566, 'Rigor Abargos', 'Add account: carlito', '::1', 'LAPTOP-UI866FP5', '2022-04-04 23:11:53', 0),
(567, 'Rigor Abargos', 'Add account: carlito', '::1', 'LAPTOP-UI866FP5', '2022-04-04 23:11:58', 0),
(568, 'Rigor Abargos', 'Add account: test', '::1', 'LAPTOP-UI866FP5', '2022-04-04 23:12:41', 0),
(569, 'Rigor Abargos', 'Add account: test', '::1', 'LAPTOP-UI866FP5', '2022-04-04 23:13:17', 0),
(570, 'Rigor Abargos', 'Add account: test', '::1', 'LAPTOP-UI866FP5', '2022-04-04 23:13:25', 0),
(571, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-04-11 22:53:09', 0),
(572, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-04-25 06:00:36', 0),
(573, 'Rigor Abargos', 'Delete student id 7', '::1', 'LAPTOP-UI866FP5', '2022-04-25 06:09:20', 0),
(574, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-04-25 06:26:51', 0),
(575, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-04-25 06:57:08', 0),
(576, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-04-25 23:08:09', 0),
(577, 'Rigor Abargos', 'Add payment: ', '::1', 'LAPTOP-UI866FP5', '2022-04-25 23:11:01', 0),
(578, 'Rigor Abargos', 'Delete payment id 15', '::1', 'LAPTOP-UI866FP5', '2022-04-25 23:14:38', 0),
(579, 'Rigor Abargos', 'Add payment: e-0010', '::1', 'LAPTOP-UI866FP5', '2022-04-25 23:15:44', 0),
(580, 'Rigor Abargos', 'Add payment: e-0010', '::1', 'LAPTOP-UI866FP5', '2022-04-25 23:17:07', 0),
(581, 'Rigor Abargos', 'Add payment: e-0010', '::1', 'LAPTOP-UI866FP5', '2022-04-25 23:18:07', 0),
(582, 'Rigor Abargos', 'Add payment: e-0010', '::1', 'LAPTOP-UI866FP5', '2022-04-25 23:18:33', 0),
(583, 'Rigor Abargos', 'Add payment: e-0010', '::1', 'LAPTOP-UI866FP5', '2022-04-25 23:20:14', 0),
(584, 'Rigor Abargos', 'Delete payment id 20', '::1', 'LAPTOP-UI866FP5', '2022-04-25 23:24:06', 0),
(585, 'Rigor Abargos', 'Delete payment id 19', '::1', 'LAPTOP-UI866FP5', '2022-04-25 23:24:10', 0),
(586, 'Rigor Abargos', 'Add payment: e-0010', '::1', 'LAPTOP-UI866FP5', '2022-04-25 23:24:28', 0),
(587, 'Rigor Abargos', 'Add payment: e-0010', '::1', 'LAPTOP-UI866FP5', '2022-04-25 23:27:23', 0),
(588, 'Rigor Abargos', 'Add payment: e-0010', '::1', 'LAPTOP-UI866FP5', '2022-04-25 23:28:52', 0),
(589, 'Rigor Abargos', 'Add payment: e-0010', '::1', 'LAPTOP-UI866FP5', '2022-04-25 23:29:29', 0),
(590, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-04-28 23:55:56', 0),
(591, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-02 23:06:36', 0),
(592, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-09 00:54:20', 0),
(593, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 00:02:31', 0),
(594, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 00:58:16', 0),
(595, 'Rigor Abargos', 'Delete student id 8', '::1', 'LAPTOP-UI866FP5', '2022-05-14 01:09:51', 0),
(596, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 01:44:05', 0),
(597, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 01:45:10', 0),
(598, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 01:47:42', 0),
(599, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 01:48:21', 0),
(600, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 01:48:37', 0),
(601, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 01:49:04', 0),
(602, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 01:49:36', 0),
(603, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 01:50:40', 0),
(604, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 01:52:03', 0),
(605, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 01:53:45', 0),
(606, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 01:54:15', 0),
(607, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 01:54:47', 0),
(608, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 02:01:54', 0),
(609, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 02:11:43', 0),
(610, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 02:13:00', 0),
(611, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 03:09:44', 0),
(612, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 03:10:10', 0),
(613, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-14 06:51:00', 0),
(614, 'Rigor Abargos', 'Delete student id 16', '::1', 'LAPTOP-UI866FP5', '2022-05-14 07:55:03', 0),
(615, 'Rigor Abargos', 'Delete student id 15', '::1', 'LAPTOP-UI866FP5', '2022-05-14 07:55:20', 0),
(616, 'Rigor Abargos', 'Add student: brianna martinez kalia mcmahon', '::1', 'LAPTOP-UI866FP5', '2022-05-14 07:57:20', 0),
(617, 'Rigor Abargos', 'Delete student id 21', '::1', 'LAPTOP-UI866FP5', '2022-05-14 08:00:14', 0),
(618, 'Rigor Abargos', 'Add student id: 21', '::1', 'LAPTOP-UI866FP5', '2022-05-14 08:01:18', 0),
(619, 'Rigor Abargos', 'Edit payment id 25', '::1', 'LAPTOP-UI866FP5', '2022-05-14 08:02:40', 0),
(620, 'Rigor Abargos', 'Edit payment id 25', '::1', 'LAPTOP-UI866FP5', '2022-05-14 08:06:55', 0),
(621, 'Rigor Abargos', 'Edit payment id 25', '::1', 'LAPTOP-UI866FP5', '2022-05-14 08:07:36', 0),
(622, 'Rigor Abargos', 'Add payment: e-0012', '::1', 'LAPTOP-UI866FP5', '2022-05-14 08:48:11', 0),
(623, 'Rigor Abargos', 'Edit payment id 26', '::1', 'LAPTOP-UI866FP5', '2022-05-14 08:48:30', 0),
(624, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-15 01:07:51', 0),
(625, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-15 01:08:00', 0),
(626, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-15 01:09:11', 0),
(627, 'Rigor Abargos', 'Add announcement: test description', '::1', 'LAPTOP-UI866FP5', '2022-05-15 01:24:07', 0),
(628, 'Rigor Abargos', 'Delete announcement: test description', '::1', 'LAPTOP-UI866FP5', '2022-05-15 01:26:25', 0),
(629, 'Rigor Abargos', 'Add announcement: test description', '::1', 'LAPTOP-UI866FP5', '2022-05-15 01:26:34', 0),
(630, 'Rigor Abargos', 'Edit announcement: test description', '::1', 'LAPTOP-UI866FP5', '2022-05-15 01:31:52', 0),
(631, 'Rigor Abargos', 'Edit announcement: test description', '::1', 'LAPTOP-UI866FP5', '2022-05-15 01:32:29', 0),
(632, 'Rigor Abargos', 'Edit announcement: test description', '::1', 'LAPTOP-UI866FP5', '2022-05-15 01:34:15', 0),
(633, 'Rigor Abargos', 'Add announcement: announcement 2', '::1', 'LAPTOP-UI866FP5', '2022-05-15 01:34:30', 0),
(634, 'Rigor Abargos', 'Edit payment id 18', '::1', 'LAPTOP-UI866FP5', '2022-05-15 01:47:44', 0),
(635, 'Rigor Abargos', 'Edit payment id 27', '::1', 'LAPTOP-UI866FP5', '2022-05-15 02:07:55', 0),
(636, 'Rigor Abargos', 'Add announcement: test no image', '::1', 'LAPTOP-UI866FP5', '2022-05-15 02:26:28', 0),
(637, 'Rigor Abargos', 'Delete announcement: test no image', '::1', 'LAPTOP-UI866FP5', '2022-05-15 02:26:52', 0),
(638, 'Rigor Abargos', 'Add announcement: test no image\r\n', '::1', 'LAPTOP-UI866FP5', '2022-05-15 02:26:58', 0),
(639, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-15 02:34:12', 0),
(640, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-05-18 08:16:30', 0),
(641, 'Rigor Abargos', 'Edit account id 43', '::1', 'LAPTOP-UI866FP5', '2022-05-18 08:20:18', 0),
(642, 'Rigor Abargos', 'Update student payment: 77', '::1', 'LAPTOP-UI866FP5', '2022-05-18 08:50:10', 0),
(643, 'Rigor Abargos', 'Update student payment: 77', '::1', 'LAPTOP-UI866FP5', '2022-05-18 08:51:52', 0),
(644, 'Rigor Abargos', 'Update student payment: 77', '::1', 'LAPTOP-UI866FP5', '2022-05-18 08:52:39', 0),
(645, 'Rigor Abargos', 'Update student payment: 77', '::1', 'LAPTOP-UI866FP5', '2022-05-18 08:54:44', 0),
(646, 'Rigor Abargos', 'Update student payment: 77', '::1', 'LAPTOP-UI866FP5', '2022-05-18 08:55:18', 0),
(647, 'Rigor Abargos', 'Update student payment: 77', '::1', 'LAPTOP-UI866FP5', '2022-05-18 08:55:44', 0),
(648, 'Rigor Abargos', 'Update student payment: 77', '::1', 'LAPTOP-UI866FP5', '2022-05-18 08:57:11', 0),
(649, 'Rigor Abargos', 'Update student payment: 77', '::1', 'LAPTOP-UI866FP5', '2022-05-18 08:57:25', 0),
(650, 'Rigor Abargos', 'Update student payment: 77', '::1', 'LAPTOP-UI866FP5', '2022-05-18 08:59:53', 0),
(651, 'Rigor Abargos', 'Update student payment: 77', '::1', 'LAPTOP-UI866FP5', '2022-05-18 09:00:46', 0),
(652, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-06-26 01:33:09', 0),
(653, 'Rigor Abargos', 'Add subject: test', '::1', 'LAPTOP-UI866FP5', '2022-06-26 02:24:40', 0),
(654, 'Rigor Abargos', 'Add subject: test 22', '::1', 'LAPTOP-UI866FP5', '2022-06-26 02:25:39', 0),
(655, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-06-26 04:23:46', 0),
(656, 'Rigor Abargos', 'Edit payment id 28', '::1', 'LAPTOP-UI866FP5', '2022-06-26 04:59:19', 0),
(657, 'Rigor Abargos', 'Add student id: 21', '::1', 'LAPTOP-UI866FP5', '2022-06-26 05:38:32', 0),
(658, 'Rigor Abargos', 'Edit payment id 29', '::1', 'LAPTOP-UI866FP5', '2022-06-26 05:43:57', 0),
(659, 'Rigor Abargos', 'Add student id: 21', '::1', 'LAPTOP-UI866FP5', '2022-06-26 05:46:34', 0),
(660, 'Rigor Abargos', 'Edit payment id 30', '::1', 'LAPTOP-UI866FP5', '2022-06-26 05:48:51', 0),
(661, 'Rigor Abargos', 'Add student id: 21', '::1', 'LAPTOP-UI866FP5', '2022-06-26 05:51:12', 0),
(662, 'Rigor Abargos', 'Edit payment id 31', '::1', 'LAPTOP-UI866FP5', '2022-06-26 05:52:10', 0),
(663, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-06-26 05:52:59', 0),
(664, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-06-27 09:12:14', 0),
(665, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-06-27 12:05:08', 0),
(666, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-06-27 23:12:12', 0),
(667, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-06-28 08:51:38', 0),
(668, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-06-28 23:15:15', 0),
(669, 'Rigor Abargos', 'Add student id: 13', '::1', 'LAPTOP-UI866FP5', '2022-06-28 23:26:53', 0),
(670, 'Rigor Abargos', 'Edit payment id 32', '::1', 'LAPTOP-UI866FP5', '2022-06-28 23:27:37', 0),
(671, 'Rigor Abargos', 'Edit payment id 32', '::1', 'LAPTOP-UI866FP5', '2022-06-28 23:27:43', 0),
(672, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-06-29 07:17:00', 0),
(673, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-06-29 13:12:32', 0),
(674, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-07-02 06:56:22', 0),
(675, 'Rigor Abargos', 'Edit account id 43', '::1', 'LAPTOP-UI866FP5', '2022-07-02 07:00:08', 0),
(676, 'Rigor Abargos', 'Edit faculty id 3', '::1', 'LAPTOP-UI866FP5', '2022-07-02 07:01:09', 0),
(677, 'Rigor Abargos', 'Edit faculty id 3', '::1', 'LAPTOP-UI866FP5', '2022-07-02 07:01:38', 0),
(678, 'Rigor Abargos', 'Edit faculty id 3', '::1', 'LAPTOP-UI866FP5', '2022-07-02 07:02:15', 0),
(679, 'Rigor Abargos', 'Delete student id 13', '::1', 'LAPTOP-UI866FP5', '2022-07-02 07:04:32', 0),
(680, 'Rigor Abargos', 'Delete student id 13', '::1', 'LAPTOP-UI866FP5', '2022-07-02 07:10:06', 0),
(681, 'Rigor Abargos', 'Add student id: 13', '::1', 'LAPTOP-UI866FP5', '2022-07-02 07:17:28', 0),
(682, 'Rigor Abargos', 'Edit payment id 33', '::1', 'LAPTOP-UI866FP5', '2022-07-02 07:20:18', 0),
(683, 'Rigor Abargos', 'Logged in', '::1', 'LAPTOP-UI866FP5', '2022-07-03 00:31:59', 0);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `payment_code` varchar(100) NOT NULL,
  `cash` decimal(10,2) NOT NULL,
  `semestral` decimal(10,2) NOT NULL,
  `quarterly` decimal(10,2) NOT NULL,
  `monthly` decimal(10,2) NOT NULL,
  `easy_payment_scheme` decimal(10,2) NOT NULL,
  `is_included` varchar(50) NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `added_by` varchar(100) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `description`, `payment_code`, `cash`, `semestral`, `quarterly`, `monthly`, `easy_payment_scheme`, `is_included`, `created_at`, `updated_at`, `added_by`, `is_deleted`) VALUES
(1, 'Tuition Fee', 'P-0001', '15008.00', '5252.80', '4052.16', '1650.88', '1650.88', 'YES', '2022-02-06 13:31:47', '2022-02-10 23:15:53', 'admin', 0),
(2, 'Registration Fee', 'P-0002', '1000.00', '1000.00', '1000.00', '1000.00', '1000.00', 'YES', '2022-02-06 13:34:08', '2022-02-10 23:15:57', 'admin', 0),
(3, 'Miscellaneous Fee', 'P-0003', '3700.00', '3700.00', '370.00', '3700.00', '370.00', 'YES', '2022-02-06 13:34:54', '2022-02-10 23:15:59', 'admin', 0),
(4, 'Other Fees', 'P-0004', '4000.00', '1333.33', '400.00', '4000.00', '400.00', 'YES', '2022-02-06 13:35:14', '2022-02-10 23:16:00', 'admin', 0),
(5, 'Test', 'P-0005', '2500.00', '1000.00', '1000.00', '1000.00', '1000.00', 'YES', '2022-02-06 15:07:49', '2022-02-10 23:16:01', 'admin', 1),
(6, 'Books', 'P-0006', '450.00', '0.00', '0.00', '0.00', '0.00', 'NO', '2022-02-11 07:19:10', '2022-02-10 23:19:51', 'admin', 1),
(7, 'test', 'P-0007', '2000.00', '100.00', '100.00', '100.00', '100.00', 'YES', '2022-03-12 15:11:51', '2022-03-12 07:12:04', 'admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE `semester` (
  `id` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `is_opened` int(11) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'ON GOING',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`id`, `description`, `start_date`, `end_date`, `created_by`, `is_opened`, `status`, `created_at`) VALUES
(5, 'SY 2022 - 2023', '2022-01-01', '2022-06-30', 'admin', 1, 'DONE', '2022-07-03 01:27:55'),
(7, 'Test school year', '2022-06-01', '2022-09-30', 'EMP-0001', 0, 'DONE', '2022-07-03 01:27:55'),
(8, 'test', '2022-06-01', '2022-06-30', 'EMP-0001', 0, 'DONE', '2022-06-29 13:14:45'),
(9, 'test 2', '2022-06-26', '2022-06-30', 'EMP-0001', 0, 'DONE', '2022-06-29 13:14:45'),
(10, 'test 3', '2022-06-26', '2022-06-30', 'EMP-0001', 0, 'DONE', '2022-06-29 13:17:28'),
(11, 'Test 4', '2022-06-01', '2022-06-29', 'EMP-0001', 0, 'DONE', '2022-06-29 13:18:13'),
(12, 'test 5', '2022-06-01', '2022-06-30', 'EMP-0001', 0, 'ON GOING', '2022-07-02 07:26:10'),
(13, 'test 6', '2022-06-01', '2022-06-30', 'EMP-0001', 0, 'DONE', '2022-07-03 01:16:39');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `mname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `birthday` date NOT NULL,
  `grade_level` varchar(10) NOT NULL,
  `section` text DEFAULT NULL,
  `lrn` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `place_of_birth` text DEFAULT NULL,
  `prev_school` text NOT NULL,
  `fathers_name` text NOT NULL,
  `mothers_name` text NOT NULL,
  `name_of_guardian` text NOT NULL,
  `relationship_student` text NOT NULL,
  `contact_number` text NOT NULL,
  `image_2x2` text DEFAULT NULL,
  `birth_certificate` text DEFAULT NULL,
  `report_grade` text DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `registered_at` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` varchar(100) NOT NULL DEFAULT 'PENDING',
  `approved_by` text NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `fname`, `mname`, `lname`, `gender`, `birthday`, `grade_level`, `section`, `lrn`, `address`, `place_of_birth`, `prev_school`, `fathers_name`, `mothers_name`, `name_of_guardian`, `relationship_student`, `contact_number`, `image_2x2`, `birth_certificate`, `report_grade`, `email`, `registered_at`, `created_at`, `updated_at`, `status`, `approved_by`, `is_deleted`) VALUES
(1, 'Test1', '', 'Test1', 'Male', '2012-02-02', 'Kinder 1', NULL, '', 'Malolos Bulacan', 'Address', 'School', 'Father', 'Mother', 'Guardian', 'Student', '09123456789', NULL, NULL, NULL, '', '127.0.0.1', '2022-02-04 00:08:15', '2022-02-13 00:39:13', 'APPROVED', 'admin', 0),
(2, 'Test 2', '', 'Test 2', 'Female', '2022-02-02', 'Grade 1', NULL, '', 'Address', 'dfg', 'dfg', 'dfg', 'dfg', 'dfg', 'dfg', '123123123', NULL, NULL, NULL, '', '', '2022-02-04 00:22:08', '2022-02-13 00:39:05', 'APPROVED', 'admin', 0),
(3, 'Test 3', '', 'Test 3', 'Male', '2022-02-09', 'Kinder 1', NULL, 'asd', 'asd', 'asd', 'asd', 'asd', 'asd', 'asd', 'asd', 'asd', NULL, NULL, NULL, '', 'asd', '2022-02-06 06:21:16', '2022-02-13 00:38:57', 'APPROVED', 'admin', 0),
(4, 'Test 4', '', 'Test 4', 'Female', '2022-02-02', 'Grade 1', NULL, '', 'Address', 'dfg', 'dfg', 'dfg', 'dfg', 'dfg', 'dfg', '123123123', NULL, NULL, NULL, '', '', '2022-02-04 00:22:08', '2022-02-13 00:38:47', 'APPROVED', 'admin', 0),
(5, 'Mira', 'Abel Morales', 'Duncan', 'Female', '1975-06-08', 'Grade 3', NULL, '', 'Contreras', '', 'Odonnell', 'Vazquez', 'Horn', 'Fleming', 'Blankenship', 'Cantu', NULL, NULL, NULL, 'joxufuk@mailinator.com', '::1', '2022-02-13 09:02:58', '2022-02-13 01:02:58', 'PENDING', '', 0),
(6, 'Kiayada', 'Jocelyn Navarro', 'Yang', 'Male', '1971-06-01', 'Grade 5', NULL, '', 'Bond', '', 'Clemons', 'Sampson', 'Dorsey', 'Harper', 'Oneill', 'Hull', NULL, NULL, NULL, 'cibawymyn@mailinator.com', '::1', '2022-02-13 09:03:47', '2022-02-13 01:03:47', 'PENDING', '', 0),
(7, 'Aimee', 'Carter Burch', 'Atkins', 'Female', '1986-06-29', 'Grade 5', NULL, '', 'Harrison', '', 'Barnes', 'Wilkinson', 'Harper', 'Walls', 'Sherman', 'Mejia', NULL, NULL, NULL, 'miceh@mailinator.com', '::1', '2022-02-13 09:03:52', '2022-04-25 06:09:18', 'REJECTED', '', 0),
(8, 'Xander', 'Chloe Dickson', 'Hunt', 'Male', '2000-06-15', 'Grade 4', NULL, '', 'Barr', '', 'Townsend', 'Rhodes', 'Bradford', 'Greer', 'Baldwin', '', 'enrollment/eternals-k7-1920x1080.jpg', 'enrollment/532d0341186be12412c6a4362cc08710.jpg', 'enrollment/doctor-strange-in-the-multiverse-of-madness-4k-2020-pw.jpg', 'fokacib@mailinator.com', '::1', '2022-02-13 09:03:55', '2022-03-27 23:17:44', 'APPROVED', 'admin', 0),
(9, 'Alana', 'Judah Christensen', 'Conway', 'Female', '1981-09-23', 'Kinder 1', 'Matino', '', 'Lee', '', 'Burt', 'Burch', 'Franks', 'Ball', 'Ochoa', 'Reilly', NULL, NULL, NULL, 'datusopomy@mailinator.com', '::1', '2022-02-13 09:03:58', '2022-05-09 02:31:52', 'APPROVED', 'admin', 0),
(10, 'Inez', 'Lavinia Hodges', 'Silva', 'Female', '1997-06-17', 'Grade 4', 'makisig', '23232323', 'White', 'malolos', 'Mckee', 'Herman', 'Kirkland', 'Mcneil', 'Peck', 'Battle', NULL, NULL, NULL, 'zurocit@mailinator.com', '::1', '2022-05-14 16:43:48', '2022-05-14 08:43:48', 'APPROVED', 'admin', 0),
(11, 'Drake', 'Leigh Ray', 'Simon', 'Male', '1979-10-06', 'Grade 5', NULL, '', 'Burke', '', 'Mosley', 'Kinney', 'Coffey', 'Mitchell', 'Fitzgerald', 'Morales', NULL, NULL, NULL, 'xaza@mailinator.com', '::1', '2022-02-13 09:08:57', '2022-02-13 01:11:05', 'APPROVED', 'admin', 0),
(12, 'Martha', 'Tamekah Hyde', 'Christensen', 'Male', '1988-07-22', 'Grade 4', NULL, '', 'Maldonado', '', 'Barnes', 'Santiago', 'Shelton', 'Middleton', 'Spence', 'Ellison', NULL, NULL, NULL, 'debemifa@mailinator.com', '::1', '2022-02-13 09:09:15', '2022-02-13 01:10:59', 'APPROVED', 'admin', 0),
(13, 'Rigel', 'Ivory Strong', 'Jarvis', 'Male', '2013-04-20', 'Grade 1', 'Masipag', '111111', 'Melendez', '', 'Mcpherson', 'Murphy', 'Cantu', 'Cunningham', 'Poole', '09107102498', 'enrollment/eternals-k7-1920x1080.jpg', 'enrollment/dp.jpg', NULL, 'zumuxogacu@mailinator.com', '::1', '2022-05-15 10:30:30', '2022-07-02 07:07:02', 'APPROVED', 'admin', 0),
(14, 'Cally', 'Guy Valencia', 'Wilder', 'Male', '1989-11-07', 'Grade 2', NULL, '', 'Wagner', NULL, 'Davenport', 'Hayden', 'Roth', 'Myers', 'Mccarthy', 'George', NULL, NULL, NULL, 'mrmartincarlito@gmail.com', '::1', '2022-03-23 07:27:24', '2022-03-28 23:06:19', 'APPROVED', 'EMP-0001', 0),
(15, 'Phoebe Richards', 'Cecilia Wooten', 'Emily Watkins', 'Female', '1983-04-17', 'Cillum rep', NULL, '', 'Quia molestiae ut ci', NULL, 'Eius nihil deserunt ', 'Jorden Brock', 'Dorian Figueroa', 'Wyatt Spence', 'Exercitation eu dolo', '826', NULL, NULL, NULL, 'byxef@mailinator.com', '::1', '2022-05-14 15:44:38', '2022-05-14 07:55:18', 'REJECTED', '', 0),
(16, 'Eagan Burks', 'Leah Wood', 'Jack Mcconnell', 'Female', '1971-06-17', 'A quis nis', NULL, '', 'A iusto omnis doloru', NULL, 'Laboriosam magni si', 'Veda Oconnor', 'Abbot Cohen', 'Regan Beck', 'Odio necessitatibus ', '101', NULL, NULL, NULL, 'zyju@mailinator.com', '::1', '2022-05-14 15:45:32', '2022-05-14 07:55:01', 'REJECTED', '', 0),
(17, 'Yasir Coffey', 'Kyle Hoover', 'Laura Houston', 'Female', '1983-01-08', 'Kinder 2', 'Fugit numquam volup', '', 'In ut fuga Mollit s', NULL, 'Corporis rerum ratio', 'Charissa Morin', 'Colorado Rosa', 'Colorado Mason', 'At maxime eos iste i', '494', NULL, NULL, NULL, 'benagi@mailinator.com', '::1', '2022-05-14 15:48:44', '2022-05-14 07:48:44', 'PENDING', '', 0),
(18, 'Marcia Ball', 'Ronan Ware', 'Emma Shepard', 'Female', '1990-02-21', 'Grade 6', 'Aliquam nisi quas qu', '', 'Ipsum neque ea sunt', 'Nulla aute proident', 'Deserunt aspernatur ', 'Noah Moran', 'Brittany Lambert', 'Burke Wiggins', 'In quia et veniam d', '89', NULL, NULL, NULL, 'fibexyfyki@mailinator.com', '::1', '2022-05-14 15:51:08', '2022-05-14 07:51:08', 'PENDING', '', 0),
(19, 'Wendy James', 'Dale Singleton', 'Buckminster Wilkinson', 'Female', '1974-03-16', 'Grade 2', 'Explicabo Quia est ', '', 'Tenetur quaerat culp', 'Minim sed et obcaeca', 'Ut suscipit quidem e', 'Adam Sampson', 'Kelsey Bates', 'Jameson Sandoval', 'Numquam reiciendis a', '611', NULL, NULL, NULL, 'febifahik@mailinator.com', '::1', '2022-05-14 15:52:39', '2022-05-14 07:52:39', 'PENDING', '', 0),
(20, 'Brenda Barlow', 'Armand Lopez', 'Cameron Joseph', 'Male', '2005-01-18', 'Grade 3', 'Velit est expedita d', '', 'Est alias optio pr', 'Dolores et ipsum es', 'Vel reprehenderit i', 'Joelle Wheeler', 'Damian Justice', 'Remedios Woodard', 'Eum nemo culpa non ', '856', NULL, NULL, NULL, 'mynisomez@mailinator.com', '::1', '2022-05-14 15:53:35', '2022-06-26 05:35:31', 'PENDING', '', 0),
(21, 'Oren Daniel', 'Amal Sloan', 'Buffy Guerra', 'Male', '1970-04-28', 'Grade 6', 'Delectus voluptate ', 'Doloremque velit exc', 'Ex perferendis sunt', 'Quia dolore molestia', 'Repellendus Ipsam a', 'Azalia Holder', 'Keane Barber', 'Kaden Baker', 'Quis soluta vel ipsu', '09173114735', 'enrollment/doctor-strange-in-the-multiverse-of-madness-4k-2020-pw.jpg', 'enrollment/dp.jpg', 'enrollment/532d0341186be12412c6a4362cc08710.jpg', 'mrmartincarlito@gmail.com', '::1', '2022-05-18 16:41:08', '2022-06-26 06:06:54', 'APPROVED', 'EMP-0001', 0),
(22, 'Moana Schmidt', 'Zenaida Waters', 'Elliott Tran', 'Female', '1989-09-04', 'Grade 2', 'Deleniti aliqua Sin', '', 'Lorem nihil quibusda', 'Ut perferendis asper', 'Enim autem dolor non', 'Henry Mercado', 'Ivana Richardson', 'Cameron Walls', 'Do amet voluptate e', '99', NULL, NULL, NULL, 'cigoxur@mailinator.com', '::1', '2022-05-14 16:35:13', '2022-05-14 08:35:13', 'PENDING', '', 0),
(23, 'Madeline Fisher', 'Jada Sawyer', 'Alden Freeman', 'Male', '2008-05-11', 'Kinder 1', 'Provident dolorem q', 'Voluptatem vero ut ', 'Placeat duis minus ', 'Consequatur fugiat c', 'Omnis eu voluptas ni', 'Lucian Lloyd', 'Mannix Ward', 'Clinton Simpson', 'Corporis id distinct', '841', NULL, NULL, NULL, 'pebiqi@mailinator.com', '::1', '2022-05-14 16:45:32', '2022-05-14 08:45:32', 'PENDING', '', 0),
(24, 'Tanek Hensley', 'Nelle Mckinney', 'Rylee Bowers', 'Female', '1983-11-07', 'Grade 3', 'Sed doloribus eius i', 'Autem velit culpa ', 'Dolore quia quas nul', 'Voluptatem assumend', 'Provident sint dol', 'Cade Woodward', 'Amber Gilmore', 'Rogan David', 'Et rerum velit numq', '332', NULL, NULL, NULL, 'zucyqapup@mailinator.com', '::1', '2022-05-14 16:46:05', '2022-05-14 08:46:05', 'PENDING', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `student_grades`
--

CREATE TABLE `student_grades` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `faculty_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `semester_id` int(11) NOT NULL,
  `first` decimal(10,2) DEFAULT 0.00,
  `second` decimal(10,2) DEFAULT 0.00,
  `third` decimal(10,2) DEFAULT 0.00,
  `fourth` decimal(10,2) DEFAULT 0.00,
  `final` decimal(10,2) DEFAULT 0.00,
  `remarks` varchar(200) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `student_grades`
--

INSERT INTO `student_grades` (`id`, `student_id`, `faculty_id`, `subject_id`, `semester_id`, `first`, `second`, `third`, `fourth`, `final`, `remarks`, `date_time`) VALUES
(1, 9, 3, 193, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '', '2022-03-20 22:49:00'),
(2, 13, 3, 193, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '', '2022-03-20 22:49:04'),
(3, 21, 3, 41, 5, '88.00', '89.00', '98.00', '87.00', '90.50', 'Excellent', '2022-06-29 08:00:51'),
(4, 13, 3, 41, 5, NULL, NULL, NULL, NULL, '0.00', '', '2022-04-04 23:27:49'),
(5, 9, 3, 40, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '', '2022-03-20 22:49:27'),
(6, 13, 3, 40, 5, '90.00', '89.00', '92.00', '90.00', '90.25', '', '2022-04-04 23:28:59'),
(7, 13, 3, 22, 5, '98.00', '0.00', '0.00', '0.00', '24.50', '', '2022-07-03 00:44:36'),
(8, 9, 3, 22, 5, '97.00', '88.00', '0.00', '0.00', '24.25', '', '2022-07-03 00:36:58'),
(9, 9, 3, 23, 5, '0.00', '0.00', '0.00', '0.00', '0.00', '', '2022-06-28 01:53:57'),
(10, 13, 3, 23, 5, '89.00', '88.00', '89.00', '90.00', '89.00', '', '2022-06-28 01:54:27'),
(11, 9, 3, 24, 5, '88.00', '98.00', '87.00', '88.00', '89.00', 'Excellent', '2022-06-29 08:15:32'),
(12, 13, 3, 24, 5, '98.00', '97.00', '97.00', '98.00', '97.50', '', '2022-06-28 01:51:13');

-- --------------------------------------------------------

--
-- Table structure for table `student_payments`
--

CREATE TABLE `student_payments` (
  `id` int(11) NOT NULL,
  `payment_history_id` int(11) NOT NULL,
  `payment_description` text NOT NULL,
  `student_number` varchar(50) NOT NULL,
  `billed_date` text DEFAULT NULL,
  `billed_amount` decimal(10,2) NOT NULL,
  `status` varchar(50) NOT NULL,
  `remarks` text NOT NULL,
  `added_by` varchar(50) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_sms` int(11) NOT NULL DEFAULT 0,
  `is_emailed` int(11) NOT NULL DEFAULT 0,
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `student_payments`
--

INSERT INTO `student_payments` (`id`, `payment_history_id`, `payment_description`, `student_number`, `billed_date`, `billed_amount`, `status`, `remarks`, `added_by`, `date_time`, `is_sms`, `is_emailed`, `is_deleted`) VALUES
(75, 29, 'Down Payment', 'E-0012', '26-06-2022', '10350.88', 'APPROVED', '', '', '2022-06-26 05:48:14', 1, 1, 0),
(76, 26, 'INSTALLMENT', 'E-0012', '05-09-2022', '1650.88', 'APPROVED', '', 'EMP-0001', '2022-05-14 08:48:30', 0, 0, 0),
(77, 28, 'INSTALLMENT', 'E-0012', '05-10-2022', '1650.88', 'APPROVED', '', '', '2022-06-26 04:59:19', 1, 1, 0),
(78, 0, 'INSTALLMENT', 'E-0012', '05-11-2022', '1650.88', '', '', '', '2022-05-14 08:00:52', 0, 0, 0),
(108, 30, 'Down Payment', 'E-0012', '26-06-2022', '5822.16', 'APPROVED', '', '', '2022-06-26 05:48:51', 1, 1, 0),
(109, 0, 'INSTALLMENT', 'E-0012', '05-11-2022', '6362.16', '', '', '', '2022-06-26 05:46:07', 0, 0, 0),
(110, 0, 'INSTALLMENT', 'E-0012', '05-02-2023', '6362.16', '', '', '', '2022-06-26 05:46:07', 0, 0, 0),
(111, 0, 'INSTALLMENT', 'E-0012', '05-05-2023', '6362.16', '', '', '', '2022-06-26 05:46:07', 0, 0, 0),
(112, 31, 'Down Payment', 'E-0012', '26-06-2022', '11286.13', 'APPROVED', '', '', '2022-06-26 05:52:10', 1, 1, 0),
(113, 0, 'INSTALLMENT', 'E-0012', '05-12-2022', '6586.14', '', '', '', '2022-06-26 05:50:41', 0, 0, 0),
(114, 0, 'INSTALLMENT', 'E-0012', '05-03-2023', '6586.14', '', '', '', '2022-06-26 05:50:41', 0, 0, 0),
(122, 33, 'Full Payment', 'E-0010', '02-07-2022', '22957.60', 'APPROVED', 'PAID', '', '2022-07-02 07:20:18', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `student_payment_history`
--

CREATE TABLE `student_payment_history` (
  `id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `image_file` text NOT NULL,
  `date_paid` datetime NOT NULL,
  `student_number` varchar(50) NOT NULL,
  `approved_by` varchar(50) NOT NULL,
  `date_approved` date DEFAULT NULL,
  `is_approved` int(11) NOT NULL DEFAULT 0,
  `or_number` varchar(50) DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'PENDING',
  `remarks` text NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `student_payment_history`
--

INSERT INTO `student_payment_history` (`id`, `amount`, `image_file`, `date_paid`, `student_number`, `approved_by`, `date_approved`, `is_approved`, `or_number`, `status`, `remarks`, `is_deleted`) VALUES
(9, '5822.16', '', '2022-03-21 00:00:00', 'E-0010', 'EMP-0001', '2022-03-21', 1, NULL, 'APPROVED', '', 0),
(10, '6362.16', '', '2022-03-28 06:53:02', 'E-0010', '', NULL, 0, NULL, 'PENDING', '', 0),
(11, '6362.16', '', '2022-03-28 06:54:41', 'E-0010', '', NULL, 0, NULL, 'PENDING', '', 0),
(12, '6362.16', '', '2022-03-28 06:55:38', 'E-0010', '', NULL, 0, NULL, 'PENDING', '', 0),
(14, '6362.16', 'uploads/dp.jpg', '2022-03-28 06:57:43', 'E-0010', '', NULL, 0, NULL, 'PENDING', '', 0),
(15, '6362.16', '', '2022-04-26 00:00:00', '', '', NULL, 0, '62672a85abce1', 'PENDING', '', 1),
(16, '6362.16', '', '2022-04-27 00:00:00', 'E-0010', '', NULL, 0, '62672BA0A5644', 'PENDING', '', 0),
(17, '12724.32', '', '2022-04-27 00:00:00', 'E-0010', '', NULL, 0, '62672BF349E8E', 'PENDING', 'test', 0),
(18, '12724.32', '', '2022-05-15 00:00:00', 'E-0010', '', NULL, 0, '62672C2FBC7C6', 'REJECTED', 'rejected', 0),
(19, '12724.32', '', '2022-04-26 00:00:00', 'E-0010', '', NULL, 0, '62672C4947551', 'PENDING', '', 1),
(20, '12724.32', '', '2022-04-26 00:00:00', 'E-0010', '', NULL, 0, '62672CAEC3520', 'PENDING', '', 1),
(21, '6362.16', '', '2022-04-26 00:00:00', 'E-0010', 'EMP-0001', '2022-04-26', 1, '62672DAC31D69', 'APPROVED', 'Paid', 0),
(22, '6362.16', '', '2022-04-26 00:00:00', 'E-0010', 'EMP-0001', '2022-04-26', 1, '62672E5BCBD7A', 'APPROVED', '', 0),
(23, '6362.16', '', '2022-04-26 00:00:00', 'E-0010', 'EMP-0001', '2022-04-26', 1, '62672EB4E1A7E', 'APPROVED', '', 0),
(24, '6362.16', 'uploads/dp.jpg', '2022-04-26 00:00:00', 'E-0010', 'EMP-0001', '2022-04-26', 1, '62672ED934F1D', 'APPROVED', '', 0),
(25, '10350.88', 'uploads/dp.jpg', '2022-05-14 00:00:00', 'E-0012', 'EMP-0001', '2022-05-14', 1, NULL, 'APPROVED', '', 0),
(26, '1650.88', 'uploads/dp.jpg', '2022-05-14 00:00:00', 'E-0012', 'EMP-0001', '2022-05-14', 1, '2223333', 'APPROVED', '', 0),
(27, '6362.16', 'uploads/dp.jpg', '2022-05-15 00:00:00', 'E-0010', '', NULL, 0, '0', 'REJECTED', 'rejected', 0),
(28, '1650.88', 'uploads/doctor-strange-in-the-multiverse-of-madness-4k-2020-pw.jpg', '2022-06-26 00:00:00', 'E-0012', 'EMP-0001', '2022-06-26', 1, '1234', 'APPROVED', '', 0),
(29, '10350.88', 'uploads/dp.jpg', '2022-06-27 00:00:00', 'E-0012', 'EMP-0001', '2022-06-26', 1, '3333', 'APPROVED', '', 0),
(30, '5822.16', 'uploads/dp.jpg', '2022-06-27 00:00:00', 'E-0012', 'EMP-0001', '2022-06-26', 1, '2222', 'APPROVED', '', 0),
(31, '11286.13', 'uploads/dp.jpg', '2022-06-26 00:00:00', 'E-0012', 'EMP-0001', '2022-06-26', 1, '11111', 'APPROVED', '', 0),
(32, '5822.16', 'uploads/dp.jpg', '2022-06-29 00:00:00', 'E-0010', 'EMP-0001', '2022-06-29', 1, '3434344343', 'APPROVED', '', 0),
(33, '22957.60', 'uploads/dp.jpg', '2022-07-02 00:00:00', 'E-0010', 'EMP-0001', '2022-07-02', 1, '676767676767', 'APPROVED', 'PAID', 0);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `grade_level` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `added_by` varchar(50) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `code`, `grade_level`, `description`, `created_at`, `updated_at`, `added_by`, `is_deleted`) VALUES
(22, 'S-0001', 'Kinder 1', 'Math', '2022-02-06 08:57:58', '2022-02-06 00:57:58', 'admin', 0),
(23, 'S-0002', 'Kinder 1', 'English', '2022-02-06 08:58:00', '2022-02-06 00:58:00', 'admin', 0),
(24, 'S-0003', 'Kinder 1', 'Filipino', '2022-02-06 08:58:02', '2022-02-06 00:58:02', 'admin', 0),
(25, 'S-0004', 'Kinder 2', 'Math', '2022-02-06 08:58:13', '2022-02-06 00:58:13', 'admin', 0),
(26, 'S-0005', 'Kinder 2', 'English', '2022-02-06 08:58:14', '2022-02-06 00:58:14', 'admin', 0),
(27, 'S-0006', 'Kinder 2', 'Filipino', '2022-02-06 08:58:16', '2022-02-06 00:58:16', 'admin', 0),
(28, 'S-0007', 'Grade 1', 'Math', '2022-02-06 08:58:20', '2022-02-06 00:58:20', 'admin', 0),
(29, 'S-0008', 'Grade 1', 'English', '2022-02-06 08:58:21', '2022-02-06 00:58:21', 'admin', 0),
(30, 'S-0009', 'Grade 1', 'Filipino', '2022-02-06 08:58:23', '2022-02-06 00:58:23', 'admin', 0),
(31, 'S-0010', 'Grade 2', 'Math', '2022-02-06 08:58:28', '2022-02-06 00:58:28', 'admin', 0),
(32, 'S-0011', 'Grade 2', 'English', '2022-02-06 08:58:30', '2022-02-06 00:58:30', 'admin', 0),
(33, 'S-0012', 'Grade 2', 'Filipino', '2022-02-06 08:58:32', '2022-02-06 00:58:32', 'admin', 0),
(34, 'S-0013', 'Grade 3', 'Math', '2022-02-06 08:58:35', '2022-02-06 00:58:35', 'admin', 0),
(35, 'S-0014', 'Grade 3', 'Englihs', '2022-02-06 08:58:37', '2022-02-06 00:58:37', 'admin', 0),
(36, 'S-0015', 'Grade 3', 'Filipino', '2022-02-06 08:58:40', '2022-02-06 00:58:40', 'admin', 0),
(37, 'S-0016', 'Grade 4', 'Math', '2022-02-06 08:58:44', '2022-02-06 00:58:44', 'admin', 0),
(38, 'S-0017', 'Grade 4', 'English', '2022-02-06 08:58:47', '2022-02-06 00:58:47', 'admin', 0),
(39, 'S-0018', 'Grade 4', 'Filipino', '2022-02-06 08:58:49', '2022-02-06 00:58:49', 'admin', 0),
(40, 'S-0019', 'Grade 5', 'Math', '2022-02-06 08:58:53', '2022-02-06 00:58:53', 'admin', 0),
(41, 'S-0020', 'Grade 5', 'English', '2022-02-06 08:58:55', '2022-02-06 00:58:55', 'admin', 0),
(42, 'S-0021', 'Grade 5', 'Filipino', '2022-02-06 08:58:57', '2022-02-06 00:58:57', 'admin', 0),
(43, 'S-0022', 'Grade 6', 'Math', '2022-02-06 08:59:01', '2022-02-06 00:59:01', 'admin', 0),
(44, 'S-0023', 'Grade 6', 'Englihs', '2022-02-06 08:59:02', '2022-02-06 00:59:02', 'admin', 0),
(45, 'S-0024', 'Grade 6', 'Filipino', '2022-02-06 08:59:04', '2022-02-06 00:59:04', 'admin', 0),
(46, 'S-0025', 'Grade 6', 'test', '2022-06-26 10:24:40', '2022-06-26 02:24:40', 'EMP-0001', 0),
(47, 'S-0026', 'Grade 6', 'Test 22', '2022-06-26 10:25:39', '2022-06-26 02:25:39', 'EMP-0001', 0);

-- --------------------------------------------------------

--
-- Table structure for table `subjects_schedule`
--

CREATE TABLE `subjects_schedule` (
  `id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `grade_level` varchar(50) NOT NULL,
  `school_year` int(11) NOT NULL DEFAULT 0,
  `title` varchar(50) NOT NULL,
  `faculty_id` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `className` varchar(50) NOT NULL,
  `added_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_deleted` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subjects_schedule`
--

INSERT INTO `subjects_schedule` (`id`, `subject_id`, `grade_level`, `school_year`, `title`, `faculty_id`, `start`, `end`, `className`, `added_by`, `created_at`, `updated_at`, `is_deleted`) VALUES
(299, 23, 'Kinder 1', 5, 'English (August Olsen)', 1, '2022-05-10 08:00:00', '2022-05-10 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:32:24', '2022-07-03 01:20:18', 0),
(307, 22, 'Kinder 1', 5, 'Math (August Olsen)', 1, '2022-05-11 08:00:00', '2022-05-11 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:32:38', '2022-07-03 01:20:18', 0),
(308, 22, 'Kinder 1', 5, 'Math (August Olsen)', 1, '2022-05-18 08:00:00', '2022-05-18 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:32:38', '2022-07-03 01:20:18', 0),
(309, 22, 'Kinder 1', 5, 'Math (August Olsen)', 1, '2022-05-25 08:00:00', '2022-05-25 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:32:38', '2022-07-03 01:20:18', 0),
(310, 22, 'Kinder 1', 5, 'Math (August Olsen)', 1, '2022-06-01 08:00:00', '2022-06-01 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:32:38', '2022-07-03 01:20:18', 0),
(311, 22, 'Kinder 1', 5, 'Math (August Olsen)', 1, '2022-06-08 08:00:00', '2022-06-08 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:32:38', '2022-07-03 01:20:18', 0),
(312, 22, 'Kinder 1', 5, 'Math (August Olsen)', 1, '2022-06-15 08:00:00', '2022-06-15 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:32:38', '2022-07-03 01:20:18', 0),
(313, 22, 'Kinder 1', 5, 'Math (August Olsen)', 1, '2022-06-22 08:00:00', '2022-06-22 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:32:38', '2022-07-03 01:20:18', 0),
(314, 22, 'Kinder 1', 5, 'Math (August Olsen)', 1, '2022-06-29 08:00:00', '2022-06-29 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:32:38', '2022-07-03 01:20:18', 0),
(315, 24, 'Kinder 1', 5, 'Filipino (Wang Rosario Hollee Horton)', 3, '2022-05-12 08:00:00', '2022-05-12 10:00:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:36:53', '2022-07-03 01:20:18', 0),
(316, 24, 'Kinder 1', 5, 'Filipino (Wang Rosario Hollee Horton)', 3, '2022-05-19 08:00:00', '2022-05-19 10:00:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:36:53', '2022-07-03 01:20:18', 0),
(317, 24, 'Kinder 1', 5, 'Filipino (Wang Rosario Hollee Horton)', 3, '2022-05-26 08:00:00', '2022-05-26 10:00:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:36:53', '2022-07-03 01:20:18', 0),
(318, 24, 'Kinder 1', 5, 'Filipino (Wang Rosario Hollee Horton)', 3, '2022-06-02 08:00:00', '2022-06-02 10:00:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:36:53', '2022-07-03 01:20:18', 0),
(319, 24, 'Kinder 1', 5, 'Filipino (Wang Rosario Hollee Horton)', 3, '2022-06-09 08:00:00', '2022-06-09 10:00:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:36:53', '2022-07-03 01:20:18', 0),
(320, 24, 'Kinder 1', 5, 'Filipino (Wang Rosario Hollee Horton)', 3, '2022-06-16 08:00:00', '2022-06-16 10:00:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:36:53', '2022-07-03 01:20:18', 0),
(321, 24, 'Kinder 1', 5, 'Filipino (Wang Rosario Hollee Horton)', 3, '2022-06-23 08:00:00', '2022-06-23 10:00:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:36:53', '2022-07-03 01:20:18', 0),
(322, 23, 'Kinder 1', 5, 'English (Shelby Morris)', 2, '2022-05-12 10:30:00', '2022-05-12 11:30:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:37:17', '2022-07-03 01:20:18', 0),
(323, 23, 'Kinder 1', 5, 'English (Shelby Morris)', 2, '2022-05-19 10:30:00', '2022-05-19 11:30:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:37:17', '2022-07-03 01:20:18', 0),
(324, 23, 'Kinder 1', 5, 'English (Shelby Morris)', 2, '2022-05-26 10:30:00', '2022-05-26 11:30:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:37:17', '2022-07-03 01:20:18', 0),
(325, 23, 'Kinder 1', 5, 'English (Shelby Morris)', 2, '2022-06-02 10:30:00', '2022-06-02 11:30:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:37:17', '2022-07-03 01:20:18', 0),
(326, 23, 'Kinder 1', 5, 'English (Shelby Morris)', 2, '2022-06-09 10:30:00', '2022-06-09 11:30:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:37:17', '2022-07-03 01:20:18', 0),
(327, 23, 'Kinder 1', 5, 'English (Shelby Morris)', 2, '2022-06-16 10:30:00', '2022-06-16 11:30:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:37:17', '2022-07-03 01:20:18', 0),
(328, 23, 'Kinder 1', 5, 'English (Shelby Morris)', 2, '2022-06-23 10:30:00', '2022-06-23 11:30:00', 'bg-primary', 'EMP-0001', '2022-05-09 09:37:17', '2022-07-03 01:20:18', 0),
(329, 23, 'Kinder 1', 5, 'English (August Olsen)', 1, '2022-05-14 08:00:00', '2022-05-14 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-14 08:15:50', '2022-07-03 01:20:18', 0),
(330, 23, 'Kinder 1', 5, 'English (August Olsen)', 1, '2022-05-21 08:00:00', '2022-05-21 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-14 08:15:50', '2022-07-03 01:20:18', 0),
(331, 23, 'Kinder 1', 5, 'English (August Olsen)', 1, '2022-05-28 08:00:00', '2022-05-28 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-14 08:15:50', '2022-07-03 01:20:18', 0),
(332, 23, 'Kinder 1', 5, 'English (August Olsen)', 1, '2022-06-04 08:00:00', '2022-06-04 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-14 08:15:50', '2022-07-03 01:20:18', 0),
(333, 23, 'Kinder 1', 5, 'English (August Olsen)', 1, '2022-06-11 08:00:00', '2022-06-11 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-14 08:15:50', '2022-07-03 01:20:18', 0),
(334, 23, 'Kinder 1', 5, 'English (August Olsen)', 1, '2022-06-18 08:00:00', '2022-06-18 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-14 08:15:50', '2022-07-03 01:20:18', 0),
(335, 23, 'Kinder 1', 5, 'English (August Olsen)', 1, '2022-06-25 08:00:00', '2022-06-25 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-14 08:15:50', '2022-07-03 01:20:18', 0),
(336, 22, 'Kinder 1', 5, 'Math (August Olsen)', 1, '2022-05-14 09:30:00', '2022-05-14 10:45:00', 'bg-primary', 'EMP-0001', '2022-05-14 08:22:46', '2022-07-03 01:20:18', 0),
(337, 22, 'Kinder 1', 5, 'Math (August Olsen)', 1, '2022-05-21 09:30:00', '2022-05-21 10:45:00', 'bg-primary', 'EMP-0001', '2022-05-14 08:22:46', '2022-07-03 01:20:18', 0),
(338, 22, 'Kinder 1', 5, 'Math (August Olsen)', 1, '2022-05-28 09:30:00', '2022-05-28 10:45:00', 'bg-primary', 'EMP-0001', '2022-05-14 08:22:46', '2022-07-03 01:20:18', 0),
(339, 22, 'Kinder 1', 5, 'Math (August Olsen)', 1, '2022-06-04 09:30:00', '2022-06-04 10:45:00', 'bg-primary', 'EMP-0001', '2022-05-14 08:22:46', '2022-07-03 01:20:18', 0),
(340, 22, 'Kinder 1', 5, 'Math (August Olsen)', 1, '2022-06-11 09:30:00', '2022-06-11 10:45:00', 'bg-primary', 'EMP-0001', '2022-05-14 08:22:46', '2022-07-03 01:20:18', 0),
(341, 22, 'Kinder 1', 5, 'Math (August Olsen)', 1, '2022-06-18 09:30:00', '2022-06-18 10:45:00', 'bg-primary', 'EMP-0001', '2022-05-14 08:22:46', '2022-07-03 01:20:18', 0),
(342, 22, 'Kinder 1', 5, 'Math (August Olsen)', 1, '2022-06-25 09:30:00', '2022-06-25 10:45:00', 'bg-primary', 'EMP-0001', '2022-05-14 08:22:46', '2022-07-03 01:20:18', 0),
(350, 23, 'Kinder 1', 5, 'English (Shelby Morris)', 2, '2022-05-17 08:00:00', '2022-05-17 09:30:00', 'bg-primary', 'EMP-0001', '2022-05-15 10:02:25', '2022-07-03 01:20:18', 0),
(351, 23, 'Kinder 1', 5, 'English (Shelby Morris)', 2, '2022-05-24 08:00:00', '2022-05-24 09:30:00', 'bg-primary', 'EMP-0001', '2022-05-15 10:02:25', '2022-07-03 01:20:18', 0),
(352, 23, 'Kinder 1', 5, 'English (Shelby Morris)', 2, '2022-05-31 08:00:00', '2022-05-31 09:30:00', 'bg-primary', 'EMP-0001', '2022-05-15 10:02:25', '2022-07-03 01:20:18', 0),
(353, 23, 'Kinder 1', 5, 'English (Shelby Morris)', 2, '2022-06-07 08:00:00', '2022-06-07 09:30:00', 'bg-primary', 'EMP-0001', '2022-05-15 10:02:25', '2022-07-03 01:20:18', 0),
(354, 23, 'Kinder 1', 5, 'English (Shelby Morris)', 2, '2022-06-14 08:00:00', '2022-06-14 09:30:00', 'bg-primary', 'EMP-0001', '2022-05-15 10:02:25', '2022-07-03 01:20:18', 0),
(355, 23, 'Kinder 1', 5, 'English (Shelby Morris)', 2, '2022-06-21 08:00:00', '2022-06-21 09:30:00', 'bg-primary', 'EMP-0001', '2022-05-15 10:02:25', '2022-07-03 01:20:18', 0),
(356, 23, 'Kinder 1', 5, 'English (Shelby Morris)', 2, '2022-06-28 08:00:00', '2022-06-28 09:30:00', 'bg-primary', 'EMP-0001', '2022-05-15 10:02:25', '2022-07-03 01:20:18', 0),
(357, 24, 'Kinder 1', 5, 'Filipino (Shelby Morris)', 2, '2022-05-15 08:00:00', '2022-05-15 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-15 10:02:45', '2022-07-03 01:20:18', 0),
(358, 24, 'Kinder 1', 5, 'Filipino (Shelby Morris)', 2, '2022-05-22 08:00:00', '2022-05-22 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-15 10:02:45', '2022-07-03 01:20:18', 0),
(359, 24, 'Kinder 1', 5, 'Filipino (Shelby Morris)', 2, '2022-05-29 08:00:00', '2022-05-29 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-15 10:02:45', '2022-07-03 01:20:18', 0),
(360, 24, 'Kinder 1', 5, 'Filipino (Shelby Morris)', 2, '2022-06-05 08:00:00', '2022-06-05 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-15 10:02:45', '2022-07-03 01:20:18', 0),
(361, 24, 'Kinder 1', 5, 'Filipino (Shelby Morris)', 2, '2022-06-12 08:00:00', '2022-06-12 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-15 10:02:45', '2022-07-03 01:20:18', 0),
(362, 24, 'Kinder 1', 5, 'Filipino (Shelby Morris)', 2, '2022-06-19 08:00:00', '2022-06-19 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-15 10:02:45', '2022-07-03 01:20:18', 0),
(363, 24, 'Kinder 1', 5, 'Filipino (Shelby Morris)', 2, '2022-06-26 08:00:00', '2022-06-26 09:00:00', 'bg-primary', 'EMP-0001', '2022-05-15 10:02:45', '2022-07-03 01:20:18', 0),
(364, 35, 'Grade 3', 5, 'Englihs (Shelby Morris)', 2, '2022-05-18 08:00:00', '2022-05-18 09:30:00', 'bg-primary', 'EMP-0001', '2022-05-18 17:15:45', '2022-07-03 01:20:18', 0),
(365, 35, 'Grade 3', 5, 'Englihs (Shelby Morris)', 2, '2022-05-25 08:00:00', '2022-05-25 09:30:00', 'bg-primary', 'EMP-0001', '2022-05-18 17:15:45', '2022-07-03 01:20:18', 0),
(366, 35, 'Grade 3', 5, 'Englihs (Shelby Morris)', 2, '2022-06-01 08:00:00', '2022-06-01 09:30:00', 'bg-primary', 'EMP-0001', '2022-05-18 17:15:45', '2022-07-03 01:20:18', 0),
(367, 35, 'Grade 3', 5, 'Englihs (Shelby Morris)', 2, '2022-06-08 08:00:00', '2022-06-08 09:30:00', 'bg-primary', 'EMP-0001', '2022-05-18 17:15:45', '2022-07-03 01:20:18', 0),
(368, 35, 'Grade 3', 5, 'Englihs (Shelby Morris)', 2, '2022-06-15 08:00:00', '2022-06-15 09:30:00', 'bg-primary', 'EMP-0001', '2022-05-18 17:15:45', '2022-07-03 01:20:18', 0),
(369, 35, 'Grade 3', 5, 'Englihs (Shelby Morris)', 2, '2022-06-22 08:00:00', '2022-06-22 09:30:00', 'bg-primary', 'EMP-0001', '2022-05-18 17:15:45', '2022-07-03 01:20:18', 0),
(370, 35, 'Grade 3', 5, 'Englihs (Shelby Morris)', 2, '2022-06-29 08:00:00', '2022-06-29 09:30:00', 'bg-primary', 'EMP-0001', '2022-05-18 17:15:45', '2022-07-03 01:20:18', 0),
(372, 23, 'Kinder 1', 5, 'English (August Olsen)', 1, '2022-06-27 08:00:00', '2022-06-27 09:00:00', 'bg-primary', 'EMP-0001', '2022-06-26 10:26:47', '2022-07-03 01:20:18', 0),
(373, 23, 'Kinder 1', 5, 'English (August Olsen)', 1, '2022-06-27 10:00:00', '2022-06-27 11:00:00', 'bg-primary', 'EMP-0001', '2022-06-26 10:28:53', '2022-07-03 01:20:18', 0),
(376, 22, 'Kinder 1', 5, 'Math (August Olsen)', 1, '2022-06-27 09:00:00', '2022-06-27 09:30:00', 'bg-primary', 'EMP-0001', '2022-06-26 10:46:54', '2022-07-03 01:20:18', 0),
(378, 24, 'Kinder 1', 5, 'Filipino (August Olsen)', 1, '2022-06-27 09:30:00', '2022-06-27 10:00:00', 'bg-primary', 'EMP-0001', '2022-06-26 10:50:09', '2022-07-03 01:20:18', 0),
(379, 22, 'Kinder 1', 5, 'Math (August Olsen)', 1, '2022-07-03 08:00:00', '2022-07-03 08:45:00', 'bg-primary', 'EMP-0001', '2022-07-03 08:49:14', '2022-07-03 01:20:18', 0),
(380, 23, 'Kinder 1', 5, 'English (August Olsen)', 1, '2022-07-03 09:00:00', '2022-07-03 10:00:00', 'bg-primary', 'EMP-0001', '2022-07-03 09:23:21', '2022-07-03 01:23:21', 0),
(381, 27, 'Kinder 2', 7, 'Filipino (Shelby Morris)', 2, '2022-07-03 08:00:00', '2022-07-03 08:45:00', 'bg-primary', 'EMP-0001', '2022-07-03 09:27:42', '2022-07-03 01:27:42', 0),
(382, 27, 'Kinder 2', 7, 'Filipino (Shelby Morris)', 2, '2022-07-10 08:00:00', '2022-07-10 08:45:00', 'bg-primary', 'EMP-0001', '2022-07-03 09:27:42', '2022-07-03 01:27:42', 0),
(383, 27, 'Kinder 2', 7, 'Filipino (Shelby Morris)', 2, '2022-07-17 08:00:00', '2022-07-17 08:45:00', 'bg-primary', 'EMP-0001', '2022-07-03 09:27:42', '2022-07-03 01:27:42', 0),
(384, 27, 'Kinder 2', 7, 'Filipino (Shelby Morris)', 2, '2022-07-24 08:00:00', '2022-07-24 08:45:00', 'bg-primary', 'EMP-0001', '2022-07-03 09:27:42', '2022-07-03 01:27:42', 0),
(385, 27, 'Kinder 2', 7, 'Filipino (Shelby Morris)', 2, '2022-07-31 08:00:00', '2022-07-31 08:45:00', 'bg-primary', 'EMP-0001', '2022-07-03 09:27:42', '2022-07-03 01:27:42', 0),
(386, 27, 'Kinder 2', 7, 'Filipino (Shelby Morris)', 2, '2022-08-07 08:00:00', '2022-08-07 08:45:00', 'bg-primary', 'EMP-0001', '2022-07-03 09:27:42', '2022-07-03 01:27:42', 0),
(387, 27, 'Kinder 2', 7, 'Filipino (Shelby Morris)', 2, '2022-08-14 08:00:00', '2022-08-14 08:45:00', 'bg-primary', 'EMP-0001', '2022-07-03 09:27:42', '2022-07-03 01:27:42', 0),
(388, 27, 'Kinder 2', 7, 'Filipino (Shelby Morris)', 2, '2022-08-21 08:00:00', '2022-08-21 08:45:00', 'bg-primary', 'EMP-0001', '2022-07-03 09:27:42', '2022-07-03 01:27:42', 0),
(389, 27, 'Kinder 2', 7, 'Filipino (Shelby Morris)', 2, '2022-08-28 08:00:00', '2022-08-28 08:45:00', 'bg-primary', 'EMP-0001', '2022-07-03 09:27:42', '2022-07-03 01:27:42', 0),
(390, 27, 'Kinder 2', 7, 'Filipino (Shelby Morris)', 2, '2022-09-04 08:00:00', '2022-09-04 08:45:00', 'bg-primary', 'EMP-0001', '2022-07-03 09:27:42', '2022-07-03 01:27:42', 0),
(391, 27, 'Kinder 2', 7, 'Filipino (Shelby Morris)', 2, '2022-09-11 08:00:00', '2022-09-11 08:45:00', 'bg-primary', 'EMP-0001', '2022-07-03 09:27:42', '2022-07-03 01:27:42', 0),
(392, 27, 'Kinder 2', 7, 'Filipino (Shelby Morris)', 2, '2022-09-18 08:00:00', '2022-09-18 08:45:00', 'bg-primary', 'EMP-0001', '2022-07-03 09:27:42', '2022-07-03 01:27:42', 0),
(393, 27, 'Kinder 2', 7, 'Filipino (Shelby Morris)', 2, '2022-09-25 08:00:00', '2022-09-25 08:45:00', 'bg-primary', 'EMP-0001', '2022-07-03 09:27:42', '2022-07-03 01:27:42', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_levels`
--
ALTER TABLE `access_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enrolled_students`
--
ALTER TABLE `enrolled_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculties`
--
ALTER TABLE `faculties`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `emp_id` (`emp_id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_grades`
--
ALTER TABLE `student_grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_payments`
--
ALTER TABLE `student_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_payment_history`
--
ALTER TABLE `student_payment_history`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `or` (`or_number`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects_schedule`
--
ALTER TABLE `subjects_schedule`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_levels`
--
ALTER TABLE `access_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `enrolled_students`
--
ALTER TABLE `enrolled_students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `faculties`
--
ALTER TABLE `faculties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=684;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `semester`
--
ALTER TABLE `semester`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `student_grades`
--
ALTER TABLE `student_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `student_payments`
--
ALTER TABLE `student_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `student_payment_history`
--
ALTER TABLE `student_payment_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `subjects_schedule`
--
ALTER TABLE `subjects_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=394;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
