<?php 
	require_once("./api/config.php");

    $requested_page = (@array_keys($_GET)[0]) ? @array_keys($_GET)[0] : "dashboard"; 

    $page_name = str_replace('_',' ',$requested_page);
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" sizes="16x16" href="plugins/images/lchs.png" alt="LOGO">
	<title><?php echo ucwords($page_name);?></title>
	<?php include('./includes/load_on_page_start.php') ?>
	<?php include('./includes/pages/'.$requested_page.'/css_dependencies.php') ?>
</head>

<body class="fix-header">
	<?php include('./includes/page_preloader.php') ?>
	<!-- ============================================================== -->
	<!-- Wrapper -->
	<!-- ============================================================== -->
	<div id="wrapper">
		<?php include('./includes/navigation/topbar.php') ?>
		<?php include('./includes/navigation/navdrawer.php') ?>
		<!-- ============================================================== -->
		<!-- Page Content -->
		<!-- ============================================================== -->
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row bg-title">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h4 class="page-title">
							<?php echo $page_name ?>
						</h4>
					</div>
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
						<button class="right-side-toggle waves-effect waves-light bg-theme btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="white-box">
							<?php
								$content = './includes/pages/'.$requested_page.'/content.html';

								if(!file_exists($content)){
									echo "<script>window.location.href = './?page_not_exist'</script>";
								}

								require($content);
							?>
						</div>
					</div>
				</div>
		
				<footer class="bg-theme footer text-center" style="color:whitesmoke"><b> <?php echo COMPANY_NAME?> </b></footer>
			</div>
			<!-- ============================================================== -->
			<!-- End Page Content -->
			<!-- ============================================================== -->
			<?php include('./includes/navigation/rightsidebar.php') ?>
		</div>
		<!-- ============================================================== -->
		<!-- End Wrapper -->
		<!-- ============================================================== -->
		<?php include('./includes/load_on_page_end.php') ?>
		<?php include('./includes/pages/'.$requested_page.'/script_dependencies.php') ?>
		<?php echo '<script src="./includes/pages/'.$requested_page.'/script.js"></script>' ?>
	
</body>

</html>