<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav slimscrollsidebar">
		<div class="sidebar-head">
			<h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span>
				<span class="hide-menu">Navigation</span></h3>
		</div>
		<ul class="nav" id="side-menu">
            <?php 
                include("navdrawer_links.php");
                echo $links_HTML; 
            ?>
		</ul>
	</div>
</div>
<!-- Left navbar-header end -->