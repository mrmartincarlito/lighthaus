<?php
    $links = array( 
        array( 
            "name" => "Dashboard", 
            "data-icon" => "&#xe028;", 
            "link" => "./?dashboard", 
            "linea-type" => "linea-basic",
            "child_items" => array(
            ) 
        ),
        array( 
            "name" => "Approvals", 
            "data-icon" => "&#xe001;", 
            "link" => "javascript:void(0)", 
            "linea-type" => "linea-basic",
            "child_items" => array(
                array( 
                    "name" => "Student Registration", 
                    "data-icon" => "&#xe00a;", 
                    "linea-type" => "linea-basic",
                    "link" => "./?student_registration", 
                ),
                array( 
                    "name" => "Evaluation", 
                    "data-icon" => "&#xe006;", 
                    "linea-type" => "linea-basic",
                    "link" => "./?evaluation", 
                ),
                array( 
                    "name" => "Billing Payments", 
                    "data-icon" => "t",
                    "linea-type" => "linea-basic", 
                    "link" => "./?payments_approval", 
                ),
            ),
        ),
        array( 
            "name" => "School Year Settings", 
            "data-icon" => "d", 
            "link" => "javascript:void(0)", 
            "linea-type" => "linea-basic",
            "child_items" => array(
                array( 
                    "name" => "Enrolled Students", 
                    "data-icon" => "Y",
                    "linea-type" => "linea-basic", 
                    "link" => "./?students", 
                ),
                array( 
                    "name" => "Faculty", 
                    "data-icon" => "&#xe017;",
                    "linea-type" => "linea-basic", 
                    "link" => "./?faculty", 
                ),
                array( 
                    "name" => "Announcements", 
                    "data-icon" => "K",
                    "linea-type" => "linea-basic", 
                    "link" => "./?announcements", 
                ),
                array( 
                    "name" => "School Year Opening", 
                    "data-icon" => "L",
                    "linea-type" => "linea-basic", 
                    "link" => "./?school_year", 
                ),
                array( 
                    "name" => "Build Payments", 
                    "data-icon" => "s",
                    "linea-type" => "linea-basic", 
                    "link" => "./?build_payments", 
                ),
                array( 
                    "name" => "Subjects", 
                    "data-icon" => "&#xe016;",
                    "linea-type" => "linea-basic", 
                    "link" => "./?subjects", 
                ),
                array( 
                    "name" => "Subject Schedule", 
                    "data-icon" => "b",
                    "linea-type" => "linea-basic", 
                    "link" => "./?subject_schedule", 
                ),
            ) 
        ),
        array( 
            "name" => "Settings", 
            "data-icon" => "&#xe005;", 
            "link" => "javascript:void(0)", 
            "linea-type" => "linea-basic",
            "child_items" => array(
                array( 
                    "name" => "Accounts", 
                    "data-icon" => "{", 
                    "linea-type" => "linea-basic",
                    "link" => "./?user_accounts", 
                ), 
                array( 
                    "name" => "Access Levels", 
                    "data-icon" => "9", 
                    "linea-type" => "linea-basic",
                    "link" => "./?access_levels", 
                ), 
                array( 
                    "name" => "Audit Logs", 
                    "data-icon" => "#", 
                    "linea-type" => "linea-basic",
                    "link" => "./?logs", 
                ), 
            )
        ),
        array( 
            "name" => "Faculty", 
            "data-icon" => "S", 
            "link" => "javascript:void(0)", 
            "linea-type" => "linea-basic",
            "child_items" => array(
                array( 
                    "name" => "My Class", 
                    "data-icon" => "H", 
                    "linea-type" => "linea-basic",
                    "link" => "./?my_class", 
                ), 

                array( 
                    "name" => "Input of Grades", 
                    "data-icon" => "&#xe01b;", 
                    "linea-type" => "linea-basic",
                    "link" => "./?grading", 
                ), 

                array( 
                    "name" => "Final Grades", 
                    "data-icon" => "D", 
                    "linea-type" => "linea-basic",
                    "link" => "./?view_grades", 
                ), 
            ) 
        ),
        array( 
            "name" => "Reports", 
            "data-icon" => "m", 
            "link" => "./?reports", 
            "linea-type" => "linea-basic",
            "child_items" => array(
            ) 
        ),
    ); 

    //do not meddle with the code below

    $links_HTML = "";
    foreach ($links as $link) {
        $isactive = '';
        if($page_name == strtolower($link['name'])){
            $isactive = 'active';
        }

        $child_links = '';
        $arrow_extra = '';
        if(count($link['child_items']) > 0){
            $child_links = '<ul class="nav nav-second-level">';
            $arrow_extra = '<span class="fa arrow"></span>';
            foreach ($link['child_items'] as $child_item) {
                $child_links .= '
                <li>
                    <a href="'.$child_item['link'].'">
                        <i data-icon="'.$child_item['data-icon'].'" class="linea-icon '.$child_item["linea-type"].' fa-fw"></i><span class="hide-menu">'.$child_item['name'].'</span>
                    </a>
                </li>
                ';
            }
            $child_links .= '</ul>';
        }

        $links_HTML .= '
            <li>
                <a href="'.$link['link'].'" class="waves-effect '.$isactive.'">
                    <i data-icon="'.$link['data-icon'].'" class="linea-icon '.$link["linea-type"].' fa-fw"></i>
                    <span class="hide-menu">'.$link['name'].'</span>
                    '.$arrow_extra.'
                </a> 
                    '.$child_links.'
            </li>
        ';
    }
?>
