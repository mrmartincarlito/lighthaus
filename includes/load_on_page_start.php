<?php
  //in order
  $csses = [
    "bootstrap/dist/css/bootstrap.min.css", //bootstrap core
    "plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css", //menu
    "plugins/bower_components/pnotify/pnotify.custom.min.css", //toast css
    "css/animate.css", //animate
    "css/style.css", //custom css
  ];

  foreach($csses as $css){
    echo '<link href="'.$css.'" rel="stylesheet">
    ';
  }

?>

    <link href="css/colors/megna.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->