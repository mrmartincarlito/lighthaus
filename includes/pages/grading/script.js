const GRADE_API = './api/grades.php'

function subjectHandledLookUp(grade_level) {
	$.ajax({
		url: GRADE_API + '?getSubjectHandledByGrade=' + grade_level,
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			var newVal = '<option value="" selected disabled>Select Subject Handled</option>'
	
			newVal += json.map(value => {
				return `<option value="${value.subject_id}">${value.title}</option>`
			})
	
			$(`#subject_handled`).html(newVal)
		})
		.fail(errorThrown => {
			console.log('Get Error: ', errorThrown)
			return false
		})
}
$('#datatable').DataTable()
var myTable;
var studentInTable = []
function filter() {

	if ($("#subject_handled").val() == null) {
		alert("Choose Subject Handled")
		return;
	}

	var requestData = {
		grade_level : $("#grade_level").val(),
		subject_handled : $("#subject_handled").val()
	}

	$.blockUI({
		baseZ: 2000
	})

	$('#datatable').DataTable().destroy()
	myTable = $('#datatable').DataTable({
		processing: true,
		ajax: {
			url: GRADE_API + '?filterStudentGrades=' + JSON.stringify(requestData),
			dataSrc: function (json) {
				var return_data = new Array()
				for (var i = 0; i < json.length; i++) {

					$("#subject").html(json[i].subject.description)	
					
					studentInTable.push(json[i].grade.id)

					return_data.push({
						id: json[i].enrolledInfo.student_number,
						fname: json[i].studentInfo.lname + ', ' + json[i].studentInfo.fname + ' ' + json[i].studentInfo.mname ,
						first : `<input readonly id="first_`+json[i].grade.id+`" class="first form-control" type='text' value='` +json[i].grade.first+ `'>`,
						second : `<input readonly id="second_`+json[i].grade.id+`" class="second form-control" type='text' value='` +json[i].grade.second+ `'>`,
						third : `<input readonly id="third_`+json[i].grade.id+`" class="third form-control" type='text' value='` +json[i].grade.third+ `'>`,
						fourth : `<input readonly id="fourth_`+json[i].grade.id+`" class="fourth form-control" type='text' value='` +json[i].grade.fourth+ `'>`,
						final : `<input readonly class="form-control" type='text' value='` +json[i].grade.final+ `'>`,
						remarks : `<input id="remarks_`+json[i].grade.id+`" class="form-control" type='text' value='` +json[i].grade.remarks+ `'>`
					})
				}
				return return_data
			},

			complete: function () {
				$.unblockUI()
				var grading = $("#grading").val()

				$("." + grading).prop("readonly", false)
			}
		},
		columns: [
			{ data: 'id' },
			{ data: 'fname' },
			{ data: 'first' },
			{ data: 'second' },
			{ data: 'third' },
			{ data: 'fourth' },
			{ data: 'final' },
			{ data : 'remarks'}
		],
		order: [[0, 'desc'], [1, 'asc'], [3, 'asc']]
	})


}

function saveChanges() {
	if(!confirm("Are you sure you want to proceed ?")){
		return
	}

	$.blockUI({
		baseZ: 2000
	})

	var studentWithGrades = []

	for (var i=0; i<studentInTable.length; i++) {
		studentWithGrades.push({
			gradeId : studentInTable[i],
			first : $("#first_" + studentInTable[i]).val(),
			second : $("#second_" + studentInTable[i]).val(),
			third : $("#third_" + studentInTable[i]).val(),
			fourth : $("#fourth_" + studentInTable[i]).val(),
			remarks : $("#remarks_" + studentInTable[i]).val()
		})
	}

	$.ajax({
		url: GRADE_API,
		type: 'post',
		data: 'saveGrade=' + JSON.stringify(studentWithGrades),
		processData: false
	})
		.done(data => {
			$.unblockUI()
			console.log('Save Changes Response: ', data)

			responseJSON = $.parseJSON(data)

			new PNotify(responseJSON)

			if (responseJSON.type == 'success') {
				myTable.ajax.reload(null, false)
			}
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
}