
const EVALUATION_API = './api/evaluation.php'

var myTable = null;
$.fn.dataTable.ext.search.push(
	function (settings, data, dataIndex) {
		var startDate = Date.parse($('#start-date').val(), 10);
		var endDate = Date.parse($('#end-date').val(), 10);
		var columnDate = Date.parse(data[4]) || 0; // use data for the age column
		if ((isNaN(startDate) && isNaN(endDate)) ||
			(isNaN(startDate) && columnDate <= endDate) ||
			(startDate <= columnDate && isNaN(endDate)) ||
			(startDate <= columnDate && columnDate <= endDate)) {
			return true;
		}
		return false;
	}
);

$('.date-range-filter').change(function () {
	myTable.draw();
});

myTable = $('#datatable').DataTable({
	processing: true,
	serverSide: true,
	order: [
		[0, 'desc']
	],
	buttons: [
		'copy', 'excel',
		{

			extend: 'pdf',
			orientation: 'portrait',
			filename: 'Registration',
			paging: true,
			customize: function (doc) {
				doc.content.splice(0, 1);
				var now = new Date();
				var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
				doc.pageMargins = [20, 60, 20, 30];
				doc.defaultStyle.fontSize = 8;
				doc.styles.tableHeader.fontSize = 8;
				doc['header'] = (function () {
					return {
						columns: [{
							alignment: 'left',
							text: 'Student Registration',
							fontSize: 20,
							margin: [20, 20]
						}]
					}
				});
				doc['footer'] = (function () {
					return {
						columns: [{
							alignment: 'left',
							text: 'Created on: ' + jsDate.toString(),
							margin: [10, 10]
						}]
					}
				})
			}
		}
	],
	dom: 'lBfrtip',
	"language": {
		"lengthMenu": 'Display <select>' +
			'<option value="10">10</option>' +
			'<option value="50">50</option>' +
			'<option value="70">70</option>' +
			'<option value="80">80</option>' +
			'<option value="100">100</option>' +
			'<option value="-1">All</option>' +
			'</select> records'
	},
	ajax: {
		url: EVALUATION_API + '?get&status=PENDING',
		complete: function () {
			//TODO Insert code here
		}
	}
})
function viewStudent(id) {
	$(`input[name*="modifyId"]`).val(id)
	$.blockUI()

	$.ajax({
		url: EVALUATION_API + '?getDetails=' + id,
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)

			$.unblockUI()

			$.each(json.enrolledInfo, function (i, item) {
				$("#" + i).html(item)
			});
			
			$.each(json.students, function (i, item) {
				
				if (i == "image_2x2") {
					$("#" + i).attr("src", "./api/" +item)
					$("#image_2x2_download").attr("src", "./api/" +item)
				} 

				else if (i == "birth_certificate" || i == "report_grade") {
					$("#" + i).attr("src", "./api/" +item)
				}
				
				else if ( i == "grade_level") {
					$("#grade_level").html(item)
				}	

				else {
					$("#" + i).html(item)
				}

				
			});



			$(`input[name*="modifyId"]`).val(id)

			$('#formModal').modal('show')
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Account Details Get Error', errorThrown)
			return false
		})
}

//delete button
$('#deleteButton').on('click', function () {
	if (!confirm('Are you sure you want to reject the evaluation of this student?')) {
		return false
	}

	$.blockUI({
		baseZ: 2000
	})

	$(`input[name*="formAction"]`).val('delete')

	var data = $('form').serializeArray()
	var params = postParams('', data)

	$.ajax({
		url: EVALUATION_API,
		type: 'post',
		data: 'data=' + params,
		processData: false
	})
		.done(data => {
			$.unblockUI()
		
			responseJSON = $.parseJSON(data)
			new PNotify(responseJSON)
			if (responseJSON.type == 'success') {
				myTable.ajax.reload(null, false)
				$('#formModal').modal('hide')
			}
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Delete Post Error: ', errorThrown)
			return false
		})

	return false
})

$('form').on('submit', function (e) {
	if (!confirm('Are you sure you want to the approve the enrollment of this student?')) {
		return false
	}

	e.preventDefault()

	$.blockUI({
		baseZ: 2000
	})

	var data = $('form').serializeArray()
	var params = postParams('', data)

	$.ajax({
		url: EVALUATION_API,
		type: 'post',
		data: 'data=' + params,
		processData: false
	})
		.done(data => {
			$.unblockUI()
			console.log(data)
			responseJSON = $.parseJSON(data)

			new PNotify(responseJSON)

			if (responseJSON.type == 'success') {
				myTable.ajax.reload(null, false)
				$('#formModal').modal('hide')
			}
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
})


function filter(){
	var filter = {
		grade_level : $("#grade_level_filter").val(),
		gender : $("#gender_filter").val(),
		status : $("#status_filter").val()
	}

	myTable.ajax.url(EVALUATION_API + '?get&filter=' + JSON.stringify(filter)).load()
	
}