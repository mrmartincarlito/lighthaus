
const PROFILE_API = './api/profile.php'
const PROFILE_IMAGE = './api/upload_image.profile.php'

window.onload = init;

function init(){
	viewProfile()
}

function viewProfile() {

	$.blockUI({
		baseZ: 2000
	})

	$.ajax({
		url: PROFILE_API,
		type: 'post',
		data: 'viewProfile',
		processData: false
	})
		.done(data => {
			$.unblockUI()
			
			responseJSON = $.parseJSON(data)
			$.each(responseJSON, function (i, item) {
				if(i != "image_file"){ //avoid image lookup
					$("#" + i).val(item)
					$("#" + i).html(item)
				}
			});

			if (responseJSON.image_file != "") {
				document.getElementById("profile_pic").src = "./api/"+responseJSON.image_file
				document.getElementById("profile_pic_href").href = "./api/"+responseJSON.image_file
			}

		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
}

$('#settingsSubmit').on('submit', function (e) {
	e.preventDefault()

	$.blockUI({
		baseZ: 2000
	})

	var data = $('#settingsSubmit').serializeArray()

	var params = postParams('', data)

	$.ajax({
		url: PROFILE_API,
		type: 'post',
		data: 'settingChanges=' + params,
	})
		.done(data => {
			$.unblockUI()
			console.log(data)
			responseJSON = $.parseJSON(data)
			new PNotify(responseJSON)

			viewProfile()

		})
		.fail(errorThrown => {
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
})


function editProfile() {
	$('input[name*="modifyId"]').val()
	$('input[name*="formAction"]').val('profile')
	$('#formModal').modal('show')
}

function saveImage() {

	if (!confirm('Are you sure you want to change your profile photo?')) {
		return false
	}

	var formData = new FormData();
	formData.append('image_file', $('#image_file')[0].files[0]);

	$.ajax({
		url: PROFILE_IMAGE,
		type: 'post',
		data: formData,
		cache: false,
		contentType: false,
		processData: false
	})
	.done(data => {
		$.unblockUI()
		responseJSON = $.parseJSON(data)

		new PNotify(responseJSON)
		$('#formModal').modal('hide')

		viewProfile()
	})
	.fail(errorThrown => {
		console.log('Save Changes Post Error: ', errorThrown)
		return false
	})

}


function showPassword(id = "floatingPassword") {
	var x = document.getElementById(id);
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
