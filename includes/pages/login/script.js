const FORGOT_API = './api/forgot-password.php';

$(document).ready(() => {
	$('form').submit(function(e) {
		var data = $(this).serializeArray()
		var params = postParams('login', data)
		$.blockUI()

		$.ajax({
			url: './api/auth.php',
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				console.log('Login Response: ', data)
				$.unblockUI()
				responseJSON = $.parseJSON(data)
				new PNotify(responseJSON)
				if (responseJSON.type == 'success') {
					window.location.href = './?'
				}
			})
			.fail(errorThrown => {
				console.log('Login POST Response: ', errorThrown)
				$.unblockUI()
				console.log(errorThrown)
				return false
			})

		e.preventDefault()
	})
})

function showPassword() {
	var x = document.getElementById("password");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}

function forgotPassword() {
	let username = $("#username").val();

	if (username == "") {
		alert ("Please enter your username");
		return;
	}

	$.ajax({
		url: FORGOT_API,
		type: 'post',
		data: 'forgot=1&username=' + username,
		processData: false
	})
		.done(data => {
			console.log(data)
			new PNotify({
				type : 'success',
				text : 'Forgot Password was sent to the registered email of user ' + username,
				title : 'Successful'
			})
			
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
	
}
