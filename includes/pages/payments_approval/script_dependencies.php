<?php
  //in order
  $scripts = [
    "plugins/bower_components/datatables/jquery.dataTables.min.js", //jquery datatable js
    "plugins/bower_components/custom-select/custom-select.min.js",
    "plugins/bower_components/bootstrap-select/bootstrap-select.min.js",
    "plugins/bower_components/multiselect/js/jquery.multi-select.js",
    "https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js",
    "https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js",
    "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js",
    "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js",
    "https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js",
    "https://cdn.datatables.net/buttons/1.7.1/js/buttons.colVis.min.js",
  ];

  foreach($scripts as $script){
    echo '<script src="'.$script.'"></script>
    ';
  }
?>