
  const API = './api/student_payment.php'
  const IMAGE = './api/upload_image.student_payment.php'

  var myTable = null;
	$.fn.dataTable.ext.search.push(
		function (settings, data, dataIndex) {
			var startDate = Date.parse($('#start-date').val(), 10);
			var endDate = Date.parse($('#end-date').val(), 10);
			var columnDate = Date.parse(data[4]) || 0; // use data for the age column
			if ((isNaN(startDate) && isNaN(endDate)) ||
				(isNaN(startDate) && columnDate <= endDate) ||
				(startDate <= columnDate && isNaN(endDate)) ||
				(startDate <= columnDate && columnDate <= endDate)) {
				return true;
			}
			return false;
		}
	);

	$('.date-range-filter').change(function () {
		myTable.draw();
	});

	myTable = $('#datatable').DataTable({
		processing: true,
		serverSide: true,
		rowReorder: {
			selector: 'td:nth-child(3)'
		},
		responsive: true,
		order: [
			[0, 'desc']
		],
		buttons: [
			{
				extend: 'excel',
				text: 'Export to Excel'
			},
			{

				extend: 'pdf',
				text: 'Export to Pdf',
				orientation: 'portrait',
				filename: 'Product Information',
				paging: true,
				customize: function (doc) {
					doc.content.splice(0, 1);
					var now = new Date();
					var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
					doc.pageMargins = [20, 60, 20, 30];
					doc.defaultStyle.fontSize = 8;
					doc.styles.tableHeader.fontSize = 8;
					doc['header'] = (function () {
						return {
							columns: [{
								alignment: 'left',
								text: 'Product Information',
								fontSize: 20,
								margin: [20, 20]
							}]
						}
					});
					doc['footer'] = (function () {
						return {
							columns: [{
								alignment: 'left',
								text: 'Created on: ' + jsDate.toString(),
								margin: [10, 10]
							}]
						}
					})
				}
			}
		],
		dom: 'lBfrtip',
		"language": {
			"lengthMenu": 'Display <select>' +
				'<option value="10">10</option>' +
				'<option value="50">50</option>' +
				'<option value="70">70</option>' +
				'<option value="80">80</option>' +
				'<option value="100">100</option>' +
				'<option value="-1">All</option>' +
				'</select> records'
		},
		ajax: {
			url: API + '?get&status=PENDING',
			complete: function () {

				$('#addModal').on('click', function () {
					$('form').trigger('reset')
					$('input[type=checkbox]').prop('checked', false)
					$(`input[name*="modifyId"]`).val('')
					$(`input[name*="formAction"]`).val('add')
					$('#formModal').modal('show')
				})
			}
		}
	})

  $('form').on('submit', function(e) {
	  e.preventDefault()

	  $.blockUI({
		  baseZ: 2000
	  })

	  var data = $('form').serializeArray()
	  var params = postParams('', data)
	  var paymentSelected = $("#balances").val();

	  $.ajax({
		  url: API,
		  type: 'post',
		  data: 'data=' + params + '&selectedPayment=' + JSON.stringify(paymentSelected),
		  processData: false
	  })
		  .done(data => {
			  $.unblockUI()
			  console.log('Save Changes Response: ', data)

			  responseJSON = $.parseJSON(data)

			  new PNotify(responseJSON)

			  if (responseJSON.type == 'success') {
				  saveImage()
				  myTable.ajax.reload(null, false)
				  $('#formModal').modal('hide')
			  }
		  })
		  .fail(errorThrown => {
			  $.unblockUI()
			  console.log('Save Changes Post Error: ', errorThrown)
			  return false
		  })
  })

  //delete button
  $('#deleteButton').on('click', function() {
	  if (!confirm('Are you sure you want to remove?')) {
		  return false
	  }

	  $.blockUI({
		  baseZ: 2000
	  })

	  $(`input[name*="formAction"]`).val('delete')

	  var data = $('form').serializeArray()
	  var params = postParams('', data)

	  $.ajax({
		  url: API,
		  type: 'post',
		  data: 'data=' + params ,
		  processData: false
	  })
		  .done(data => {
			  $.unblockUI()
			  console.log('Delete Response: ', data)

			  responseJSON = $.parseJSON(data)
			  new PNotify(responseJSON)
			  if (responseJSON.type == 'success') {
				  myTable.ajax.reload(null, false)
				  $('#formModal').modal('hide')
			  }
		  })
		  .fail(errorThrown => {
			  $.unblockUI()
			  console.log('Delete Post Error: ', errorThrown)
			  return false
		  })

	  return false
  })

function saveImage() {
	var formData = new FormData();
	formData.append('image_file', $('#image_file')[0].files[0]);

	$.ajax({
		url: IMAGE,
		type: 'post',
		data: formData,
		cache: false,
		contentType: false,
		processData: false
	})
	.done(data => {
		console.log(data)
		response = JSON.parse(data)
		myTable.ajax.reload(null, false)
	})
	.fail(errorThrown => {
		console.log('Save Changes Post Error: ', errorThrown)
		return false
	})

}

function modify(data){
	$(`input[name*="modifyId"]`).val(data)
	$(`input[name*="formAction"]`).val('edit')

	$.ajax({
		url: API + '?getDetails=' + data,
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)

			$.unblockUI()
			populateForm($('form'), json)
			$('#formModal').modal('show')
			studentNumberLookUp()
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Account Details Get Error', errorThrown)
			return false
		})
}

function viewImage(data){
	$('#imageModal').modal('show')

	$.ajax({
		url: API + '?getDetails=' + data,
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)

			document.getElementById("receipt-image").src = (json.image_file != "" || json.image_file != "NULL") 
															? "./api/"+json.image_file :
															"plugins/images/no-item.png"
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Account Details Get Error', errorThrown)
			return false
		})
}

function studentNumberLookUp() {
	var studentNumber = $("#student_number").val()

	$.ajax({
		url: API + '?getStudent=' + studentNumber,
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)

			if (json.student.fullName.toString().trim() == "") {
				alert ("Student Number is invalid")
				return
			}
			$("#student_name").val(json.student.fullName)

			var newVal = ''

			newVal += json.payments.map(value => {
				$("#installment_amount").html(value.billed_amount);
				return `<option value="${value.id}">${value.payment_description} - ${value.billed_amount}</option>`
			})

			$(`#balances`).html(newVal)

			$('#balances').multiSelect();
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Account Details Get Error', errorThrown)
			return false
		})

}


function viewAll(){
	myTable.ajax.url(API + '?get').load()
}

function viewApproved(){
	myTable.ajax.url(API + '?get&status=APPROVED').load()
}

function viewPending(){
	myTable.ajax.url(API + '?get&status=PENDING').load()
}

function viewRejected(){
	myTable.ajax.url(API + '?get&status=REJECTED').load()
}

function selectItemInstallments() {
	var values = $("#balances").val();

	if (values == null) {
		$("#total_amount").val("0");
	}

	else {
		$("#total_amount").val(values.length * parseFloat($("#installment_amount").html()))
	}
}


