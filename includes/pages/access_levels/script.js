$(document).ready(function() {
	var myTable = $('#datatable').DataTable({
		processing: true,
		ajax: {
			url: './api/access_levels.php?get',
			dataSrc: function(json) {
				$('#accesslevel_checkboxes').html(
					Object.keys(json[0]).map(value => {
						if (value.includes('access')) {
							let access_text = value
								.substring(value.indexOf('_') + 1)
								.replace('_', ' ')
								.toUpperCase()

							return `
								<div class="checkbox checkbox-success checkbox-circle">
									<input id="${value}" type="checkbox" value="1" name="${value}">
									<label for="${value}"> ${access_text} </label>
								</div>
							`
						}
					})
				)

				var return_data = new Array()
				for (var i = 0; i < json.length; i++) {
					return_data.push({
						id: json[i].id,
						description: json[i].description
					})
				}
				return return_data
			},

			complete: function() {
				$('#datatable tbody').on('dblclick', 'tr', function() {
					$('form').trigger('reset')

					var data = $('#datatable')
						.DataTable()
						.row(this)
						.data().id
					$(`input[name*="modifyId"]`).val(data)
					$(`input[name*="formAction"]`).val('edit')

					$.blockUI()

					$.ajax({
						url: './api/access_levels.php?getDetails=' + data,
						processData: false
					})
						.done(data => {
							var json = $.parseJSON(data)

							$.unblockUI()
							populateForm($('form'), json)
							$('#formModal').modal('show')
						})
						.fail(errorThrown => {
							$.unblockUI()
							console.log('Account Details Get Error', errorThrown)
							return false
						})
				})

				$('#addModal').on('click', function() {
					$('form').trigger('reset')
					$('input[type=checkbox]').prop('checked', false)
					$(`input[name*="modifyId"]`).val('')
					$(`input[name*="formAction"]`).val('add')
					$('#formModal').modal('show')
				})
			}
		},
		columns: [{ data: 'id' }, { data: 'description' }],
		order: [[0, 'desc']]
	})

	$('form').on('submit', function(e) {
		e.preventDefault()

		$.blockUI({
			baseZ: 2000
		})

		var data = $(this).serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: './api/access_levels.php',
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Save Changes Response: ', data)

				responseJSON = $.parseJSON(data)

				new PNotify(responseJSON)

				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Save Changes Post Error: ', errorThrown)
				return false
			})
	})

	//delete button
	$('#deleteButton').on('click', function() {
		if (!confirm('Are you sure you want to remove this user?')) {
			return false
		}

		$.blockUI({
			baseZ: 2000
		})

		$(`input[name*="formAction"]`).val('delete')

		var data = $('form').serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: './api/access_levels.php',
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Delete Response: ', data)

				responseJSON = $.parseJSON(data)
				new PNotify(responseJSON)
				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Delete Post Error: ', errorThrown)
				return false
			})

		return false
	})
})
