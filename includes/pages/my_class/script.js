const MY_CLASS_API = './api/my_class.php'

var myTable = null;
$.fn.dataTable.ext.search.push(
	function (settings, data, dataIndex) {
		var startDate = Date.parse($('#start-date').val(), 10);
		var endDate = Date.parse($('#end-date').val(), 10);
		var columnDate = Date.parse(data[4]) || 0; // use data for the age column
		if ((isNaN(startDate) && isNaN(endDate)) ||
			(isNaN(startDate) && columnDate <= endDate) ||
			(startDate <= columnDate && isNaN(endDate)) ||
			(startDate <= columnDate && columnDate <= endDate)) {
			return true;
		}
		return false;
	}
);

$('.date-range-filter').change(function () {
	myTable.draw();
});

myTable = $('#datatable').DataTable({
	processing: true,
	serverSide: true,
	order: [
		[0, 'desc']
	],
	buttons: [
		'copy', 'excel',
		{

			extend: 'pdf',
			orientation: 'portrait',
			filename: 'Students',
			paging: true,
			customize: function (doc) {
				doc.content.splice(0, 1);
				var now = new Date();
				var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
				doc.pageMargins = [20, 60, 20, 30];
				doc.defaultStyle.fontSize = 8;
				doc.styles.tableHeader.fontSize = 8;
				doc['header'] = (function () {
					return {
						columns: [{
							alignment: 'left',
							text: 'Students',
							fontSize: 20,
							margin: [20, 20]
						}]
					}
				});
				doc['footer'] = (function () {
					return {
						columns: [{
							alignment: 'left',
							text: 'Created on: ' + jsDate.toString(),
							margin: [10, 10]
						}]
					}
				})
			}
		}
	],
	dom: 'lBfrtip',
	"language": {
		"lengthMenu": 'Display <select>' +
			'<option value="10">10</option>' +
			'<option value="50">50</option>' +
			'<option value="70">70</option>' +
			'<option value="80">80</option>' +
			'<option value="100">100</option>' +
			'<option value="-1">All</option>' +
			'</select> records'
	},
	ajax: {
		url: MY_CLASS_API + '?get&grade=' + $(`#grade_level_filter`).val(),
		complete: function () {
			//TODO Insert code here
		}
	}
})

$.ajax({
	url: MY_CLASS_API + '?getAllGradeHandled',
	processData: false
})
	.done(data => {
		var json = $.parseJSON(data)
		var newVal = ''
		var sched = ''

		newVal += json.grade_level.map(value => {
			return `<option>${value.grade_level}</option>`
		})

		sched += json.schedules.map(value => {
			$("#date").html(value.start)

			return `<li class="list-group-item">
			${value.title} (${value.startTime} - ${value.endTime})
			</li>`
		})

		$("#list_schedules").html(sched)
		$(`#grade_level_filter`).html(newVal)
		myTable.ajax.url(MY_CLASS_API + '?get&grade=' + $(`#grade_level_filter`).val()).load()
	})
	.fail(errorThrown => {
		console.log('Get Error: ', errorThrown)
		return false
	})

function changeStudent () {
	myTable.ajax.url(MY_CLASS_API + '?get&grade=' + $(`#grade_level_filter`).val()).load()
}