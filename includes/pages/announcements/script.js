const API = './api/announcements.php'
const IMAGE = './api/upload_image.banner.php'

var myTable;

$(document).ready(function() {
	myTable = $('#datatable').DataTable({
		processing: true,
		ajax: {
			url: API + '?get',
			dataSrc: function(json) {
				var return_data = new Array()
				for (const element of json) {
					return_data.push({
						id: element.id,
						description: element.description,
						image_file: `<img src="./api/${element.image_file}" alt="No Image" width="50%">`,
						created_by: element.added_by,
						date: moment(element.date_time).format('LLL')
					})
				}
				return return_data
			},

			complete: function() {
				$('#datatable tbody').on('dblclick', 'tr', function() {
					$('form').trigger('reset')

					var data = $('#datatable')
						.DataTable()
						.row(this)
						.data().id
					$(`input[name*="modifyId"]`).val(data)
					$(`input[name*="formAction"]`).val('edit')

					$.blockUI()

					$.ajax({
						url: API + '?getDetails=' + data,
						processData: false
					})
						.done(data => {
							var json = $.parseJSON(data)

							$.unblockUI()
							populateForm($('form'), json)
							$('#formModal').modal('show')
						})
						.fail(errorThrown => {
							$.unblockUI()
							console.log('Account Details Get Error', errorThrown)
							return false
						})
				})
				$('#addModal').on('click', function() {
					$('form').trigger('reset')
					$(`input[name*="modifyId"]`).val('')
					$(`input[name*="formAction"]`).val('add')
					$('#formModal').modal('show')
				})
			}
		},
		columns: [
			{ data: 'id' },
			{ data: 'description' },
			{ data: 'image_file' },
			{ data: 'created_by' },
			{ data: 'date' }
		],
		order: [[0, 'desc'], [1, 'asc'], [3, 'asc']]
	})

	$('form').on('submit', function(e) {
		e.preventDefault()

		$.blockUI({
			baseZ: 2000
		})

		var data = $('form').serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Save Changes Response: ', data)

				responseJSON = $.parseJSON(data)

				new PNotify(responseJSON)

				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					saveImage()
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Save Changes Post Error: ', errorThrown)
				return false
			})
	})

	//delete button
	$('#deleteButton').on('click', function() {
		if (!confirm('Are you sure you want to remove this ?')) {
			return false
		}

		$.blockUI({
			baseZ: 2000
		})

		$(`input[name*="formAction"]`).val('delete')

		var data = $('form').serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Delete Response: ', data)

				responseJSON = $.parseJSON(data)
				new PNotify(responseJSON)
				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Delete Post Error: ', errorThrown)
				return false
			})

		return false
	})
})


function saveImage() {
	var formData = new FormData();
	formData.append('image_file', $('#image_file')[0].files[0]);

	$.ajax({
		url: IMAGE,
		type: 'post',
		data: formData,
		cache: false,
		contentType: false,
		processData: false
	})
	.done(data => {
		console.log(data)
		response = JSON.parse(data)
		myTable.ajax.reload(null, false)
	})
	.fail(errorThrown => {
		console.log('Save Changes Post Error: ', errorThrown)
		return false
	})

}