const SUBJECT_SCHEDULE_API = './api/subject_schedule.php'
const FACULTY_API = './api/faculty.php'
const SUBJECT_API = './api/subjects.php'

loadCalendar()

function loadCalendar(filter = "") {
	$('#calendar').fullCalendar('destroy');
	$('#calendar').empty();

	$("#calendar").fullCalendar({
		slotDuration: '00:15:00',
		/* If we want to split day time each 15minutes */
		minTime: '08:00:00',
		maxTime: '19:00:00',
		defaultView: 'agendaWeek',
		handleWindowResize: false,

		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
		events: {
			url: SUBJECT_SCHEDULE_API + '?get&filter=' + filter
		},
		editable: false,
		droppable: false, // this allows things to be dropped onto the calendar !!!
		eventLimit: true, // allow "more" link when too many events
		selectable: true,
		drop: function (date) {
			//$this.onDrop($(this), date);
			console.log("event drop")
		},
		select: function (start, end, allDay) {
			// $("#my-event").modal("show")
			// $(`input[name*="modifyId"]`).val('')
			// $(`input[name*="formAction"]`).val('add')
			// $("#print").attr("disabled", true)
			// $("#start_date").val(moment(start).format("YYYY-MM-DD"))
			test(start, end)
		},
		eventClick: function (calEvent, jsEvent, view) {
			doEventClick(calEvent.id)
		}
	});
}

function doEventClick(id) {
	if (!confirm("Are you sure you want to delete this schedule ?")) {
		return
	}

	$.blockUI({
		baseZ: 2000
	})


	$.ajax({
		url: SUBJECT_SCHEDULE_API + '?deleteOccurence=' + id,
		processData: false
	})
		.done(data => {
			console.log(data)

			responseJSON = $.parseJSON(data)

			new PNotify(responseJSON)

			if (responseJSON.type == 'success') {
				loadCalendar($("#grade_level").val())
			}

			$.unblockUI()
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Roles Get Error: ', errorThrown)
			return false
		})

}

function test(start, end) {
	$("#from_date").val(moment(start).format("YYYY-MM-DD"))
	$("#to_date").val(moment(end).format("YYYY-MM-DD"))

	$("#from_time").val(moment(start).format("HH:mm:ss"))
	$("#to_time").val(moment(end).format("HH:mm:ss"))
}

function selectSubject(grade_level) {
	$.ajax({
		url: SUBJECT_API + '?getSubjectsByGradeLevel=' + grade_level,
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			var newVal = '<option selected disabled>Select Subject</option>'

			newVal += json.map(value => {
				return `<option value="${value.id}">${value.description}</option>`
			})

			$(`#subjects`).html(newVal)
			loadCalendar(grade_level)
		})
		.fail(errorThrown => {
			console.log('Roles Get Error: ', errorThrown)
			return false
		})
}

$('form').on('submit', function (e) {
	e.preventDefault()

	$.blockUI({
		baseZ: 2000
	})

	var data = $('form').serializeArray()
	var params = postParams('', data)

	$.ajax({
		url: SUBJECT_SCHEDULE_API,
		type: 'post',
		data: 'data=' + params,
		processData: false
	})
		.done(data => {
			$.unblockUI()
			console.log('Save Changes Response: ', data)

			responseJSON = $.parseJSON(data)

			new PNotify(responseJSON)

			if (responseJSON.type == 'success') {
				loadCalendar($("#grade_level").val())
			}
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
})

//select faculty
$.ajax({
	url: FACULTY_API + '?get',
	processData: false
})
	.done(data => {
		var json = $.parseJSON(data)
		var newVal = '<option selected disabled>Select Faculty</option>'

		newVal += json.map(value => {
			return `<option value="${value.id}">${value.fname} ${value.mname} ${value.lname}</option>`
		})

		$(`#faculty`).html(newVal)
	})
	.fail(errorThrown => {
		console.log('Roles Get Error: ', errorThrown)
		return false
	})
