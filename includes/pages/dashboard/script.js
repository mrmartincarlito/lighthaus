const API = './api/dashboard.php'

// cards numbers
setInterval(getCards, 10000)
getCards()
function getCards () {
	$.ajax({
		url: API + '?getCards',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
	
			$.each(json, function (i, item) {
				$("#" + i).html(item)
	
			});
		})
		.fail(errorThrown => {
			console.log('Get Error: ', errorThrown)
			return false
	})
}

getStudentStats()
function getStudentStats (){
	$.ajax({
		url: API + '?getStudentStats',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
	
			Morris.Donut({
				element: 'morris-donut-chart',
				data: json,
				resize: true,
				colors: ['#ff7676', '#2cabe3', '#53e69d', '#7bcef3']
			});
		})
		.fail(errorThrown => {
			console.log('Get Error: ', errorThrown)
			return false
	})
}


loadLineChart()
function loadLineChart() {
	//reset line chart
	$("#line-chart").empty();

	var year = $("#year").val()
	var month = $("#month").val()

	$.ajax({
		type: "GET",
		url: API + '?year=' + year + '&month=' + month ,
		data: jQuery.param({ _getLineChartForTransactions: "yes" }),
		processData: false
	})
		.done(response => {
			var json = $.parseJSON(response)

			var data = [
				{ y: '2014', a: 50, b: 90},
				{ y: '2015', a: 65,  b: 75},
				{ y: '2016', a: 50,  b: 50},
				{ y: '2017', a: 75,  b: 60},
				{ y: '2018', a: 80,  b: 65},
				{ y: '2019', a: 90,  b: 70},
				{ y: '2020', a: 100, b: 75},
				{ y: '2021', a: 115, b: 75},
				{ y: '2022', a: 120, b: 85},
				{ y: '2023', a: 145, b: 85},
				{ y: '2024', a: 160, b: 95}
			  ]

			var config = {
				data: json,
				xkey: 'm',
				ykeys: ['a', 'b'],
				labels: ['Total Income', 'Total Transactions'],
				xLabels: "month",
				parseTime: false,
				fillOpacity: 0.6,
				hideHover: 'auto',
				behaveLikeLine: true,
				resize: true,
				pointFillColors: ['#ffffff'],
				pointStrokeColors: ['black', 'blue'],
				lineColors: ['gray', 'red'],
				element: 'line-chart'
			};

			Morris.Line(config);
		})
}