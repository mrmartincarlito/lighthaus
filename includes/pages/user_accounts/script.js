$(document).ready(function() {
	var myTable = $('#datatable').DataTable({
		processing: true,
		ajax: {
			url: './api/accounts.php?get',
			dataSrc: function(json) {
				var return_data = new Array()
				for (var i = 0; i < json.length; i++) {
					return_data.push({
						id: json[i].id,
						complete_name: json[i].complete_name,
						username: json[i].username,
						role: json[i].description,
						date: moment(json[i].date).format('LLL')
					})
				}
				return return_data
			},

			complete: function() {
				$('#datatable tbody').on('dblclick', 'tr', function() {
					$('form').trigger('reset')
					$('input[type=checkbox]').prop('checked', false)

					var data = $('#datatable')
						.DataTable()
						.row(this)
						.data().id
					$(`input[name*="modifyId"]`).val(data)
					$(`input[name*="formAction"]`).val('edit')

					$.blockUI()

					$.ajax({
						url: './api/accounts.php?getDetails=' + data,
						processData: false
					})
						.done(data => {
							var json = $.parseJSON(data)

							$.unblockUI()
							populateForm($('form'), json)
							$('#formModal').modal('show')
						})
						.fail(errorThrown => {
							$.unblockUI()
							console.log('Account Details Get Error', errorThrown)
							return false
						})
				})

				$('#addModal').on('click', function() {
					$('form').trigger('reset')
					$(`input[name*="modifyId"]`).val('')
					$(`input[name*="formAction"]`).val('add')
					$(`input[name*="password"]`).val()
					$(`input[name*="confirm_password"]`).val()
					$('#formModal').modal('show')
				})
			}
		},
		columns: [
			{ data: 'id' },
			{ data: 'complete_name' },
			{ data: 'username' },
			{ data: 'role' },
			{ data: 'date' }
		],
		order: [[0, 'desc'], [1, 'asc'], [3, 'asc']]
	})

	$('form').on('submit', function(e) {
		e.preventDefault()

		$.blockUI({
			baseZ: 2000
		})

		var data = $('form').serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: './api/accounts.php',
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Save Changes Response: ', data)

				responseJSON = $.parseJSON(data)

				new PNotify(responseJSON)

				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Save Changes Post Error: ', errorThrown)
				return false
			})
	})

	//delete button
	$('#deleteButton').on('click', function() {
		if (!confirm('Are you sure you want to remove this user?')) {
			return false
		}

		$.blockUI({
			baseZ: 2000
		})

		$(`input[name*="formAction"]`).val('delete')

		var data = $('form').serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: './api/accounts.php',
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Delete Response: ', data)

				responseJSON = $.parseJSON(data)
				new PNotify(responseJSON)
				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Delete Post Error: ', errorThrown)
				return false
			})

		return false
	})

	//select role combobox
	$.ajax({
		url: './api/access_levels.php?get',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			var newVal = '<option selected disabled>Select role</option>'

			newVal += json.map(value => {
				return `<option value="${value.id}">${value.description}</option>`
			})

			$(`#role`).html(newVal)
		})
		.fail(errorThrown => {
			console.log('Roles Get Error: ', errorThrown)
			return false
		})
})
