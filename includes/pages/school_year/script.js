var myTable = null;

$(document).ready(function() {
	myTable = $('#datatable').DataTable({
		processing: true,
		ajax: {
			url: './api/semester.php?get',
			dataSrc: function(json) {
				var return_data = new Array()
				for (const element of json) {
					var checked = "checked";
					var open = "OPEN";

					if (element.is_opened != "1") {
						checked = "";
						open = "CLOSE";
					}
						
					return_data.push({
						// id: element.id,
						description: element.description,
						start_date: element.start_date,
						end_date: element.end_date,
						//status: open,
						created_by: element.created_by,
						date: moment(element.date_time).format('LLL'),
						action : `<input type="checkbox" ${checked} class="js-switch" data-color="#13dafe" onchange="changeStatus(${element.id})"/> ${open}`
					})
				}
				return return_data
			},

			complete: function() {
				$('#addModal').on('click', function() {
					$('form').trigger('reset')
					$(`input[name*="modifyId"]`).val('')
					$(`input[name*="formAction"]`).val('add')
					$('#formModal').modal('show')
				})

				$('.js-switch').each(function() {
					new Switchery($(this)[0], $(this).data());
				});
			}
		},
		columns: [
			// { data: 'id' },
			{ data: 'description' },
			{ data: 'start_date' },
			{ data: 'end_date' },
			// { data: 'status' },
			{ data: 'created_by' },
			{ data: 'date' },
			{ data: 'action'}
		],
		order: [[0, 'desc'], [1, 'asc'], [3, 'asc']]
	})

	$('form').on('submit', function(e) {
		e.preventDefault()

		$.blockUI({
			baseZ: 2000
		})

		var data = $('form').serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: './api/semester.php',
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Save Changes Response: ', data)

				responseJSON = $.parseJSON(data)

				new PNotify(responseJSON)

				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Save Changes Post Error: ', errorThrown)
				return false
			})
	})
})

function changeStatus(id) {
	

	if(!confirm("Are you sure you want to add semester? This will change the current opened semester")){
		return
	}

	$.ajax({
		url: './api/semester.php',
		type: 'get',
		data: 'changeStatus=' + id,
		processData: false
	})
		.done(data => {
			$.unblockUI()
			responseJSON = $.parseJSON(data)

			new PNotify(responseJSON)

			if (responseJSON.type == 'success') {
				myTable.ajax.reload(null, false)
			}
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Save Changes Post Error: ', errorThrown)
			return false
		})
}
