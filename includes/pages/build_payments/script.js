const PAYMENT_API = './api/payment.php'

$(document).ready(function () {
	var myTable = $('#datatable').DataTable({
		processing: true,
		ajax: {
			url: PAYMENT_API + '?get',
			dataSrc: function (json) {
				var return_data = new Array()
				for (var i = 0; i < json.length; i++) {

					if (json[i].description == "Tuition Fee") {
						computeInterestDisc(json[i].cash)
						$("#tuition_fee").html(formatNumber(json[i].cash))
					} 

					return_data.push({
						id: json[i].id,
						payment_code: json[i].payment_code,
						description: json[i].description,
						schedule_payment: json[i].schedule_payment,
						cash : formatNumber(json[i].cash),
						semestral : formatNumber(json[i].semestral),
						quarterly : formatNumber(json[i].quarterly),
						monthly : formatNumber(json[i].monthly),
						easy_payment : formatNumber(json[i].easy_payment_scheme),
						added_by : json[i].added_by
					})
				}
				return return_data
			},

			complete: function () {

				populateBreakDown();

				$('#datatable tbody').on('dblclick', 'tr', function () {
					$('form').trigger('reset')
					$('input[type=checkbox]').prop('checked', false)

					var data = $('#datatable')
						.DataTable()
						.row(this)
						.data().id
					$(`input[name*="modifyId"]`).val(data)
					$(`input[name*="formAction"]`).val('edit')

					$.blockUI()

					$.ajax({
						url: PAYMENT_API + '?getDetails=' + data,
						processData: false
					})
						.done(data => {
							var json = $.parseJSON(data)

							$.unblockUI()
							populateForm($('form'), json)
							$('#formModal').modal('show')
						})
						.fail(errorThrown => {
							$.unblockUI()
							console.log('Account Details Get Error', errorThrown)
							return false
						})
				})

				$('#addModal').on('click', function () {
					$('form').trigger('reset')
					$(`input[name*="modifyId"]`).val('')
					$(`input[name*="formAction"]`).val('add')
					$('#formModal').modal('show')
				})
			}
		},
		columns: [
			{ data: 'id' },
			{ data: 'payment_code' },
			{ data: 'description' },
			{ data: 'cash' },
			{ data: 'semestral' },
			{ data: 'quarterly' },
			{ data: 'monthly' },
			{ data: 'easy_payment' },
			{ data: 'added_by' }
		],
		order: [[0, 'desc'], [1, 'asc'], [3, 'asc']]
	})

	$('form').on('submit', function (e) {
		e.preventDefault()

		$.blockUI({
			baseZ: 2000
		})

		var data = $('form').serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: PAYMENT_API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Save Changes Response: ', data)

				responseJSON = $.parseJSON(data)

				new PNotify(responseJSON)

				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Save Changes Post Error: ', errorThrown)
				return false
			})
	})

	//delete button
	$('#deleteButton').on('click', function () {
		if (!confirm('Are you sure you want to remove this user?')) {
			return false
		}

		$.blockUI({
			baseZ: 2000
		})

		$(`input[name*="formAction"]`).val('delete')

		var data = $('form').serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: PAYMENT_API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Delete Response: ', data)

				responseJSON = $.parseJSON(data)
				new PNotify(responseJSON)
				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Delete Post Error: ', errorThrown)
				return false
			})

		return false
	})
})
//cash, sem, quart, month, ez
var discInterest = [-5, 5, 8, 10, 10];

function computeInterestDisc(cash) {
	for (var i=0; i<discInterest.length; i++) {
		$("#d_i_" + (i+1)).html(discInterest[i] + " %")
		
		var di = (discInterest[i] / 100) * cash;
		var total = parseFloat(cash) + parseFloat(di);
		$("#compute_" + (i+1)).html( formatNumber(total.toFixed(2)) )
	}
}

function computeNonTuitionFee() {
	$.ajax({
		url: PAYMENT_API + '?getSumOfNonTuitionFee',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)

			var nonTuitionAmount = json[0].sum
			
			for (var i=1; i<=5; i++) {
				var totalAmount = parseFloat(nonTuitionAmount) + parseFloat($("#compute_" + i).html().replace(',',''))
				$("#sum_" + i).html(formatNumber(totalAmount))

				var currentBalance = (totalAmount - parseFloat($("#dp_" + i).html().replace(',',''))).toFixed(2)

				if (currentBalance < 0) {
					currentBalance = 0;
				}

				$("#bal_" + i).html(formatNumber(currentBalance))
			}
			
			computeInstallments()
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Account Details Get Error', errorThrown)
			return false
		})
}

function computeInstallments() {
	$("#inst_1").html("0") //cash

	var sem = (parseFloat($("#bal_2").html().replace(',','')) / 2).toFixed(2)
	$("#inst_2").html(formatNumber(sem) + " (2)")

	var quart = (parseFloat($("#bal_3").html().replace(',','')) / 3).toFixed(2)
	$("#inst_3").html(formatNumber(quart) + " (3)")

	var month = (parseFloat($("#bal_4").html().replace(',','')) / 9).toFixed(2)
	$("#inst_4").html(formatNumber(month) + " (9)")

	var ez = (parseFloat($("#bal_5").html().replace(',','')) / 9).toFixed(2)
	$("#inst_5").html(formatNumber(ez) + " (9)")
}

function populateBreakDown() {
	$.ajax({
		url: PAYMENT_API + '?sumOfPayments',
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)

			for (var i =0; i<json.length; i++) {
				for (const [key, value] of Object.entries(json[i])) {
					$("#" + key).html(formatNumber(value))
				}
			}

			computeNonTuitionFee()

		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Account Details Get Error', errorThrown)
			return false
		})
}

