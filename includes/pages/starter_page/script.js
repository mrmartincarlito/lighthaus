$.ajax({
	url: 'apiurl',
	processData: false
})
	.done(data => {
		var json = $.parseJSON(data)
		var newVal = '<option selected disabled>Select role</option>'

		newVal += json.map(value => {
			return `<option value="${value.id}">${value.description}</option>`
		})

		$(`#role`).html(newVal)
	})
	.fail(errorThrown => {
		console.log('Get Error: ', errorThrown)
		return false
	})
