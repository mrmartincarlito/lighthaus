const FACULTY_API = './api/faculty.php'

$(document).ready(function () {
	var myTable = $('#datatable').DataTable({
		processing: true,
		ajax: {
			url: FACULTY_API + '?get',
			dataSrc: function (json) {
				var return_data = new Array()
				for (var i = 0; i < json.length; i++) {
					return_data.push({
						id: json[i].id,
						emp_id: json[i].emp_id,
						first_name: json[i].fname,
						middle_name: json[i].mname,
						last_name: json[i].lname,
						position: json[i].position,
						created_at: moment(json[i].created_at).format('LLL'),
						added_by : json[i].added_by
					})
				}
				return return_data
			},

			complete: function () {
				$('#datatable tbody').on('dblclick', 'tr', function () {
					$('form').trigger('reset')
					$('input[type=checkbox]').prop('checked', false)

					var data = $('#datatable')
						.DataTable()
						.row(this)
						.data().id
					$(`input[name*="modifyId"]`).val(data)
					$(`input[name*="formAction"]`).val('edit')

					$.blockUI()

					$.ajax({
						url: FACULTY_API + '?getDetails=' + data,
						processData: false
					})
						.done(data => {
							var json = $.parseJSON(data)

							$.unblockUI()
							populateForm($('form'), json)
							$('#formModal').modal('show')
						})
						.fail(errorThrown => {
							$.unblockUI()
							console.log('Account Details Get Error', errorThrown)
							return false
						})
				})

				$('#addModal').on('click', function () {
					$('form').trigger('reset')
					$(`input[name*="modifyId"]`).val('')
					$(`input[name*="formAction"]`).val('add')
					$('#formModal').modal('show')
				})
			}
		},
		columns: [
			{ data: 'id' },
			{ data: 'emp_id' },
			{ data: 'first_name' },
			{ data: 'middle_name' },
			{ data: 'last_name' },
			{ data: 'position' },
			{ data: 'created_at' },
			{ data: 'added_by' }
		],
		order: [[0, 'desc'], [1, 'asc'], [3, 'asc']]
	})

	$('form').on('submit', function (e) {
		e.preventDefault()

		$.blockUI({
			baseZ: 2000
		})

		var data = $('form').serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: FACULTY_API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Save Changes Response: ', data)

				responseJSON = $.parseJSON(data)

				new PNotify(responseJSON)

				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Save Changes Post Error: ', errorThrown)
				return false
			})
	})

	//delete button
	$('#deleteButton').on('click', function () {
		if (!confirm('Are you sure you want to remove this user?')) {
			return false
		}

		$.blockUI({
			baseZ: 2000
		})

		$(`input[name*="formAction"]`).val('delete')

		var data = $('form').serializeArray()
		var params = postParams('', data)

		$.ajax({
			url: FACULTY_API,
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				$.unblockUI()
				console.log('Delete Response: ', data)

				responseJSON = $.parseJSON(data)
				new PNotify(responseJSON)
				if (responseJSON.type == 'success') {
					myTable.ajax.reload(null, false)
					$('#formModal').modal('hide')
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				console.log('Delete Post Error: ', errorThrown)
				return false
			})

		return false
	})
})


