const REPORTS_API = './api/reports.php'


var selectedReport = ""


function enrolledStudents(){
	$("#no_content").css("display", "none")
	$("#daterange").css("display", "none")
	$("#enrolled_students").css("display", "")
	$("#report_of_grades").css("display", "none")
	selectedReport = "enrolled_students"
}

function incomeReport() {
	$("#no_content").css("display", "none")
	$("#daterange").css("display", "")
	$("#enrolled_students").css("display", "none")
	$("#report_of_grades").css("display", "none")
	selectedReport = "income_report"
}

function reportOfGrades() {
	$("#no_content").css("display", "none")
	$("#daterange").css("display", "none")
	$("#enrolled_students").css("display", "none")
	$("#report_of_grades").css("display", "")
	selectedReport = "report_of_grades"
}

function reportOfGradesPerGradeLevel() {
	$("#no_content").css("display", "none")
	$("#daterange").css("display", "none")
	$("#enrolled_students").css("display", "")
	$("#report_of_grades").css("display", "none")
	selectedReport = "report_of_grades_grade_level"
}

function preview(){
	var data = {
		"report" : selectedReport,
		"data" : {
			"dateFrom" : $("#date_from").val(),
			"dateTo" : $("#date_to").val(),
			"grade_level_filter" : $("#grade_level_filter").val(),
			"gender_filter" : $("#gender_filter").val(),
			"section" : $("#section").val(),
			"student_number" : $("#student_number").val()
		}
	}

	$.ajax({
		url: REPORTS_API + '?' + selectedReport + '=' + JSON.stringify(data),
		processData: false
	})
		.done(data => {
			var json = JSON.parse(data)

			$("#caption").html(json.caption)

			//get thead
			var th = "<tr>"
			$.each(json.columns, function (i, item) {
				th = th + "<th style='text-align:center'>"+item+"</th>"
			});
			th = th + "</tr>"
			$("#thead").html(th)

			var tr = ""

			if (json.rows.length == 0) {
				tr = tr + "<tr><td style='text-align:center' colspan='"+json.columns.length+"'>No Record Found</td></tr>"
			}

			//get tbody enrolled_students
			if(selectedReport == "enrolled_students"){
				$.each(json.rows, function (i, item) {
					tr = tr + "<tr>"+
						"<td style='text-align:center'>"+item.id+"</td>"+
						"<td style='text-align:center'>"+item.lname+"</td>"+
						"<td style='text-align:center'>"+item.fname+"</td>"+
						"<td style='text-align:center'>"+item.mname+"</td>"+
						"<td style='text-align:center'>"+item.grade_level+ "/" + item.section + "</td>"+
						"<td style='text-align:center'>"+item.age+"</td>"+
						"<td style='text-align:center'>"+item.address+"</td>"+
					"</tr>"
				});
			}

			//get tbody enrolled_students
			if(selectedReport == "report_of_grades"){
				$.each(json.rows, function (i, item) {
					tr = tr + "<tr>"+
						"<td style='text-align:center'>"+item.faculty_name+"</td>"+
						"<td style='text-align:center'>"+item.description+"</td>"+
						"<td style='text-align:center'>"+item.first+"</td>"+
						"<td style='text-align:center'>"+item.second+"</td>"+
						"<td style='text-align:center'>"+item.third+ "</td>"+
						"<td style='text-align:center'>"+item.fourth+"</td>"+
						"<td style='text-align:center'>"+item.final+"</td>"+
						"<td style='text-align:center'>"+item.remarks+"</td>"+
					"</tr>"
				});
			}

			//get tbody enrolled_students
			if(selectedReport == "report_of_grades_grade_level"){
				$.each(json.rows, function (i, item) {
					tr = tr + "<tr>"+
						"<td style='text-align:center'>"+item.student_name+"</td>"+
						"<td style='text-align:center'>"+item.faculty_name+"</td>"+
						"<td style='text-align:center'>"+item.description+"</td>"+
						"<td style='text-align:center'>"+item.first+"</td>"+
						"<td style='text-align:center'>"+item.second+"</td>"+
						"<td style='text-align:center'>"+item.third+ "</td>"+
						"<td style='text-align:center'>"+item.fourth+"</td>"+
						"<td style='text-align:center'>"+item.final+"</td>"+
						"<td style='text-align:center'>"+item.remarks+"</td>"+
					"</tr>"
				});
			}

			if (selectedReport == "income_report") {
				$.each(json.rows, function (i, item) {
					tr = tr + "<tr>"+
						"<td style='text-align:center'>"+item.student_number+"</td>"+
						"<td style='text-align:center'>"+item.or_number+"</td>"+
						"<td style='text-align:center'>"+item.approved_by+"</td>"+
						"<td style='text-align:center'>"+item.date_paid+"</td>"+
						"<td style='text-align:center'>"+item.remarks+"</td>"+
						"<td style='text-align:right'>"+item.amount+ "</td>"
					"</tr>"
				});
			}

			$("#tbody").html(tr)

			var others = "<table class='table table-bordered'>"
			//other data on report
			$.each(json.others, function (i, item) {
				var value = item.value
				
				if(item.is_numeric == "true"){
					value = formatNumber(item.value)
				}
				others = others + "<tr><td style='text-align:center'>"+item.name+"</td><td style='text-align:right'><b>"+  value +"</b></td></tr>"
			})

			others = others + "</table>"

			$("#others").html(others)

		})
		.fail(errorThrown => {
			console.log('Get Error: ', errorThrown)
			return false
		})
}

function printReport(printpage){
	var headstr = "<html><head><title></title></head><body>";
	var footstr = "</body>";
	var newstr = document.all.item(printpage).innerHTML;
	var oldstr = document.body.innerHTML;
	document.body.innerHTML = headstr + newstr + footstr;
	window.print();
	document.body.innerHTML = oldstr;
	return false;
}
