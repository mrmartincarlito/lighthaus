const GRADES_API = './api/grades.php'
view();

function view() {
	$.blockUI()

	$.ajax({
		url: GRADES_API + '?viewStudentGrade&grade=' + $("#grade_level").val() + '&student_number=' + $("#student_number").val(),
		processData: false
	})
		.done(data => {

			$.unblockUI()

			let json = JSON.parse(data);

			var tr = "";
			var NO_RECORD = `<span class="label alert-danger">NO RECORD</span>`;

			for (const studentData of json) {
				let kinder1 = NO_RECORD;
				if (studentData.Kinder_1.detail != null) {
					kinder1 = `<span class="label alert-success">${studentData.Kinder_1.detail.status}</span>`;
				}

				let kinder2 = NO_RECORD;
				if (studentData.Kinder_2.detail != null) {
					kinder2 = `<span class="label alert-success">${studentData.Kinder_2.detail.status}</span>`;
				}

				let grade1 = NO_RECORD;
				if (studentData.Grade_1.detail != null) {
					grade1 = `<span class="label alert-success">${studentData.Grade_1.detail.status}</span>`;
				}

				let grade2 = NO_RECORD;
				if (studentData.Grade_2.detail != null) {
					grade2 = `<span class="label alert-success">${studentData.Grade_2.detail.status}</span>`;
				}

				let grade3 = NO_RECORD;
				if (studentData.Grade_3.detail != null) {
					grade3 = `<span class="label alert-success">${studentData.Grade_3.detail.status}</span>`;
				}

				let grade4 = NO_RECORD;
				if (studentData.Grade_4.detail != null) {
					grade4 = `<span class="label alert-success">${studentData.Grade_4.detail.status}</span>`;
				}

				let grade5 = NO_RECORD;
				if (studentData.Grade_5.detail != null) {
					grade5 = `<span class="label alert-success">${studentData.Grade_5.detail.status}</span>`;
				}

				let grade6 = NO_RECORD;
				if (studentData.Grade_6.detail != null) {
					grade6 = `<span class="label alert-success">${studentData.Grade_6.detail.status}</span>`;
				}

				if ( studentData.enroll_info == null ) {
					break;
				}

				tr = tr + `
				<tr>
					<td>${studentData.enroll_info.student_number}</td>
					<td>${studentData.student.fname} ${studentData.student.mname} ${studentData.student.lname}</td>
					<td>${kinder1}</td>
					<td>${kinder2}</td>
					<td>${grade1}</td>
					<td>${grade2}</td>
					<td>${grade3}</td>
					<td>${grade4}</td>
					<td>${grade5}</td>
					<td>${grade6}</td>
					<td>${studentData.balance[0].total_balance ?? 'N/A'}</td>
					<td><button class="btn btn-primary" onclick="viewSN('${studentData.enroll_info.student_number}')">VIEW</button></td>
				</tr>
				`
			}

			if (tr == "") {
				tr = `<tr>
					<td colspan="12">NO RECORD FOUND</td>
				</tr>`
			}

			$("#content").html(tr)
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Get Error: ', errorThrown)
			return false
		})
}

function viewSN(studentNumber) {
	$("#formModal").modal('show')

	$.ajax({
		url: GRADES_API + '?viewStudentGrade&student_number=' + studentNumber,
		processData: false
	})
		.done(data => {
			var json = $.parseJSON(data)
			
			if (json.length > 0) {
				let student = json[0];

				let kinder1 = getPreviewTable(student.Kinder_1, "Kinder 1")
				let kinder2 = getPreviewTable(student.Kinder_2, "Kinder 2")
				let grade1 = getPreviewTable(student.Grade_1, "Grade 1")
				let grade2 = getPreviewTable(student.Grade_2, "Grade 2")
				let grade3 = getPreviewTable(student.Grade_3, "Grade 3")
				let grade4 = getPreviewTable(student.Grade_4, "Grade 4")
				let grade5 = getPreviewTable(student.Grade_5, "Grade 5")
				let grade6 = getPreviewTable(student.Grade_6, "Grade 6")

				$("#previewGradesAll").html(
					kinder1 + 
					kinder2 +
					grade1 +
					grade2 +
					grade3 +
					grade4 +
					grade5 +
					grade6 
				)

				$("#student_name").html(student.enroll_info.student_number + " - " + student.student.fname + " " + student.student.lname)
			}
		})
		.fail(errorThrown => {
			console.log('Get Error: ', errorThrown)
			return false
		})
}

function getPreviewTable(data, grade) {

	var tr = "";

	var sy = "";

	if (data.detail == null) {
		tr = "<tr><td colspan='7'>NO RECORD FOUND</td></tr>";
		sy = "No Record within this school year";
	}

	if (data.grade != null) {
		for (const grade of data.grade) {
			tr = tr + `
				<tr>
					<td>${grade.description}</td>
					<td>${grade.first ?? "N/A"}</td>
					<td>${grade.second ?? "N/A"}</td>
					<td>${grade.third ?? "N/A"}</td>
					<td>${grade.fourth ?? "N/A"}</td>
					<td>${grade.final ?? "N/A"}</td>
					<td>${grade.remarks ?? "N/A"}</td>
				</tr>
			`
			sy = grade.school_year
		}
	}

	var table = `
	<div class="">
		<center>
			<h4>${grade}</h4>
			<h5>${sy}</h5>
		</center>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
				<th>Subject</th>
				<th>1st</th>
				<th>2nd</th>
				<th>3rd</th>
				<th>4th</th>
				<th>Final Grade</th>
				<th>Remarks</th>
				</tr>
			</thead>
			<tbody>
				${tr}
			</tbody>
		</table>
  	</div>
	<br />
	<hr />`;

  return table;
}
